package anlys;

import java.util.Map;

public interface Recordable {
	public boolean record(Map<String, Object> record);
	
}
