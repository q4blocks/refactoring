package anlys.metrics;

import java.util.Set;

import org.apache.commons.math3.stat.descriptive.moment.Mean;

import com.google.common.collect.Sets;

import ast.Program;
import ast.Script;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class CyclomaticComplexityAnalyzer {

	public double avgComplexity(Program program) {
		double[] complexityValues = program.coll_scripts().stream().map(s -> complexityOf(s)).mapToDouble(v -> v)
				.filter(v -> v > 1.0)
				.toArray();

		return new Mean().evaluate(complexityValues);
	}

	/**
	 * count number of scripts with CC exceeding the value of 10
	 * https://resources.sei.cmu.edu/asset_files/Handbook/1997_002_001_16523.pdf
	 * 
	 * @param program
	 * @return
	 */
	public long complexScriptCount(Program program) {
		return program.coll_scripts().stream().map(CyclomaticComplexityAnalyzer::complexityOf)
				.filter(v -> v >= 10.0).count();
	}

	/**
	 * calculate cyclomatic complexity for a script
	 * 
	 * @param s
	 * @return
	 */
	public static double complexityOf(Script s) {
		Set<String> conditionalOpcodes = Sets.newHashSet("control_if", "control_if_else", "control_wait_until",
				"control_repeat_until", "operator_and", "operator_or");

		long decisionPoints = s.blockColl().stream().filter(b -> conditionalOpcodes.contains(b.getOpcode())).count();

		return (double) decisionPoints + 1;
	}

}
