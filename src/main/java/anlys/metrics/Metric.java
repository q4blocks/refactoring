package anlys.metrics;

public enum Metric implements MetricKey {

	// quality metrics
	// duplicate expressions
	expr_clone_size, expr_clone_group_size,
	// background data for duplicated seq
	seq_clone_size, seq_clone_group_size,
	// applicability metrics
	num_smells, num_failed_preconds, num_refactorables,
	// safety metrics (client)
	num_failed_invariants,

	// changed
	num_blocks_changed, locs_changed,

	// extract procedure

	// responsiveness
	analysis_time,

	// coverage
	num_executable_stmt;

	public enum Entity implements MetricKey {
		num_vars, procedure_dens, num_parameters, num_literals, num_local_var, num_global_var, num_create_clone_of
	}

	public enum Size implements MetricKey {
		// general size metrics
		num_blocks, num_scripts, num_scriptables, locs
	}

	public enum Quality implements MetricKey {
		long_script_dens, complex_script_dens, ihf
	}

}