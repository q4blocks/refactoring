package anlys.metrics;

import anlys.AnalysisResult;
import anlys.Analyzer;
import ast.Program;

public abstract class MetricAnalyzer extends Analyzer {
	protected Program program;

	public MetricAnalyzer(Program program) {
		this.program = program;
	}

	protected AnalysisResult result = new AnalysisResult();

	public abstract AnalysisResult analyze();
}
