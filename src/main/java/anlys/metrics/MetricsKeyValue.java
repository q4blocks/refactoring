package anlys.metrics;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import lombok.Getter;
import lombok.Setter;

@Getter
public class MetricsKeyValue {
	private Map<MetricKey, Object> report;

	public MetricsKeyValue() {
		report = new HashMap<>();
	}

	public void increment(MetricKey m) {
		if (!report.containsKey(m)) {
			report.put(m, 0);
		}
		report.put(m, ((Integer) report.get(m)) + 1);
	}

	public void put(MetricKey m, Object val) {
		report.put(m, val);
	}

	public Object get(MetricKey m) {
		return report.get(m);
	}

	public Optional<Object> getOpt(MetricKey m) {
		if (report.containsKey(m)) {
			return Optional.of(report.get(m));
		}
		return Optional.empty();
	}

	public Map<String, Object> asMap() {
		return report.entrySet().stream()
				.collect(Collectors.toMap(entry -> entry.getKey().name(), entry -> entry.getValue()));
	}

	@Override
	public String toString() {
		return "MetricsKeyValue [" + (report != null ? "report=" + report : "") + "]";
	}

}
