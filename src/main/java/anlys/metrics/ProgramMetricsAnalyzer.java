package anlys.metrics;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import anlys.ScratchOpcodes;
import ast.Literal;
import ast.Program;
import ast.ScratchBlock;
import ast.Script;
import ast.Scriptable;
import ast.Sprite;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Getter
public class ProgramMetricsAnalyzer {
	private static final int LONG_SCRIPT_THRESHOLD = 11;
	MetricsKeyValue projectMetricsInfo = new MetricsKeyValue();
	@Setter
	String ignorePrefix;

	public ProgramMetricsAnalyzer computeMetrics(Program program) {
		// size
		projectMetricsInfo.put(Metric.Size.num_blocks, numBlocks(program));
		projectMetricsInfo.put(Metric.Size.locs, numLineOfCode(program));
		projectMetricsInfo.put(Metric.Size.num_scripts, numScripts(program));
		projectMetricsInfo.put(Metric.Size.num_scriptables, numScriptables(program));

		// related code smells
		projectMetricsInfo.put(Metric.Quality.long_script_dens, longScriptDensity(program));
		projectMetricsInfo.put(Metric.Quality.complex_script_dens, complexScriptDensity(program));

		// others

		// extract procedure
		projectMetricsInfo.put(Metric.Entity.procedure_dens, procedureDensity(program));

		// extract constant
		projectMetricsInfo.put(Metric.Entity.num_vars, numVars(program));
		projectMetricsInfo.put(Metric.Entity.num_literals, numLiterals(program));

		// reduce var scope
		projectMetricsInfo.put(Metric.Quality.ihf, infoHidingFactor(program));
		projectMetricsInfo.put(Metric.Entity.num_local_var, numLocalVars(program));
		projectMetricsInfo.put(Metric.Entity.num_global_var, numLocalVars(program));

		// extract parent sprite
		projectMetricsInfo.put(Metric.Entity.num_create_clone_of, numCreateCloneOf(program));

		return this;
	}

	public static double infoHidingFactor(Program program) {
		return 0;
	}

	/**
	 * density of complex scripts CC > 10 (per hundred line of code)
	 * 
	 * @param program
	 * @return
	 */
	public static double complexScriptDensity(Program program) {
		double n100Loc = (double) numLineOfCode(program) / 100;
		double numScriptProc = program.coll_scripts().size() + program.coll_procdecl().size();
		double count = new CyclomaticComplexityAnalyzer().complexScriptCount(program);
		log.trace("complex script density {}/{} = {}", count, n100Loc, count / numScriptProc);
		return count / numScriptProc;
	}

	/**
	 * density of long scripts (per hundred line of code)
	 * 
	 * @param program
	 * @return
	 */
	public static double longScriptDensity(Program program) {
		double n100Loc = (double) numLineOfCode(program) / 100;
		int numLongScript = (int) program.coll_scripts().stream().filter(s -> s.loc().value() > LONG_SCRIPT_THRESHOLD)
				.count();
		return (double) numLongScript / n100Loc;
	}

	public static int numLineOfCode(Program program) {
		return program.loc().value();
	}

	public static int numProcs(Program program) {
		int numProcs = program.getStage().getNumProcDecl();
		for (Sprite sp : program.getStage().getSpriteList()) {
			numProcs += sp.getNumProcDecl();
		}
		return numProcs;
	}

	public static int numCreateCloneOf(Program program) {
		int num = 0;
		try {
			List<Script> startAsCloneScripts = program.coll_scripts().stream()
					.filter(s -> s.getBody().getNumStmt() > 0).filter(s -> s.getBody().getStmt(0).getOpcode()
							.equals(ScratchOpcodes.OPCODE_CONTROL_START_AS_CLONE))
					.collect(Collectors.toList());
			num = startAsCloneScripts.size();
		} catch (NullPointerException e) {
			e.printStackTrace();
		}
		return num;
	}

	public static double procedureDensity(Program program) {
		double n100Loc = (double) numLineOfCode(program) / 100;
		return numProcs(program) / n100Loc;
	}

	public static int numVars(Program program) {
		int numVars = program.getStage().getNumVarDecl();
		for (Sprite sp : program.getStage().getSpriteList()) {
			numVars += sp.getNumVarDecl();
		}
		return numVars;
	}

	public static int numLocalVars(Program program) {
		int numVars = 0;
		for (Sprite sp : program.getStage().getSpriteList()) {
			numVars += sp.getNumVarDecl();
		}
		return numVars;
	}

	public static int numGlobalVars(Program program) {
		return program.getStage().getNumVarDecl();
	}

	public static int numLiterals(Program program) {
		int count = 0;
		for (Scriptable s : program.getStageAndSprites()) {
			List<Literal> literals = s.literalColl().stream()
					.filter(l -> l.getAttrs().get("type").contains("math") || l.getAttrs().get("type").equals("text"))
					.collect(Collectors.toList());
			count += literals.size();
		}

		return count;
	}

	public static int numScriptables(Program program) {
		return program.getStage().getNumSprite() + 1;
	}

	public static int numScripts(Program program) {
		return (int) program.coll_scripts().stream().filter(s -> s.getBody().getNumStmt() > 0).count();
	}

	public static int numBlocks(Script script) {
		HashSet<ScratchBlock> blockColl = script.blockColl();
		List<ScratchBlock> blocks = filterDragableBlocks(blockColl);
		return blocks.size();
	}

	public static List<ScratchBlock> filterDragableBlocks(Set<ScratchBlock> blockColl) {
		return blockColl.stream()
				// count only variable block that can be dragged and dropped
				.filter(b -> b.isDraggable())
				// possibly VarAccess that represents dropdown option but not really a block
				.filter(b -> !b.getOpcode().equals(""))
				.collect(Collectors.toList());
	}

	public static int numBlocks(Program program) {
		return filterDragableBlocks(program.blockColl()).size();
	}

}
