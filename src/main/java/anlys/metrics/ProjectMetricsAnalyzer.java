package anlys.metrics;

import com.google.common.collect.ImmutableMap;

import ast.Program;

public class ProjectMetricsAnalyzer {

	public ImmutableMap<String, Object> computeProjectMetrics(Program program) {
		ImmutableMap<String, Object> metrics = ImmutableMap.<String, Object>builder()
				.put("total_blocks", 50)
				.put("failed_precond", 4)
				.put("num_var", 5)
				.build();
		return metrics;
	}

}
