package anlys.refact;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.RandomStringUtils;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import anlys.refact.precond.ConstantExprChecker;
import anlys.refact.precond.FailedPrecondException;
import anlys.refact.precond.PrecondChecker;
import anlys.refact.precond.SameReachingDef;
import anlys.refact.transform.ASTBuilder;
import anlys.refact.transform.ArgBuildSpec;
import anlys.refact.transform.TransformException;
import anlys.refact.transform.Transformer;
import anlys.refact.transform.TransformerHelper;
import anlys.refact.transform.action.TransformRes;
import ast.AssignStmt;
import ast.Expr;
import ast.ExprStmt;
import ast.OperatorExpr;
import ast.Program;
import ast.Stmt;
import ast.VarAccess;
import ast.VarDecl;
import lombok.extern.slf4j.Slf4j;
import sb3.parser.ParsingUtil;
import util.CreatedEntityCounter;

@Slf4j
public class ExtractConst extends RefactAnalyzer<ExtractConstReq> {
	@Override
	protected PrecondChecker getPrecondChecker() {
		return new PrecondChecker() {
			@Override
			public void check() throws FailedPrecondException {
				Set<PrecondFailureType> types = Sets.newHashSet();
				StringBuilder failedMsgBuilder = new StringBuilder();
				request.getExprColl().forEach(expr -> {
					try {
						new ConstantExprChecker(expr).check();
					} catch (FailedPrecondException e) {
						failedMsgBuilder.append(e.getMessage());
						types.addAll(e.getTypes());
					}
				});
				log.info("TODO: precondition checking: same reaching definition if not literal");

				if (!types.isEmpty()) {
					throw new FailedPrecondException(failedMsgBuilder.toString(), types);
				}
			}
		};
	}

	@Override
	protected Transformer getTransformer() {
		return new Transformer() {
			@Override
			public void computeActions() throws TransformException {
				TransformerHelper transHelper = new TransformerHelper();

				if (request.getExprColl().isEmpty()) {
					return;
				}
				Expr targetExpr = request.getExprColl().iterator().next();
				log.info("target: {}", targetExpr.toSbString());

				// declare new var
				String varName = "var" + CreatedEntityCounter.getCount(VarAccess.class.getName());
				String varId = targetExpr.sprite().getName() + varName;
				TransformRes<VarDecl> newVar = transHelper.declareVar(targetExpr.sprite(), varName, varId)
						.record(actions);
				VarDecl varDecl = newVar.get();

				ASTBuilder astBuilder = new ASTBuilder("blk", "var");

				// create assignment statement
				TransformRes<AssignStmt> assignStmtCreateRes = astBuilder
						.createAssignment(varDecl, (Expr) targetExpr.deepCopy().updateId("?"), "0").record(actions);// id?

				AssignStmt assignStmt = assignStmtCreateRes.get();

				Stmt stmtTarget = targetExpr.enclosingStmt();

				// find target: insert after Green Flag event top block of script with x,y
				// topmost & (leftmost)
				stmtTarget = findTargetStmt(request.getExprColl());
				if (stmtTarget == null) {
					// perhaps need to create a new green flag script
					throw new TransformException(
							"no existing scripts that can be used to initialize the contant variable");
				}

				transHelper.insertAfter(assignStmt, stmtTarget).record(actions);

				List<String> varReadIds = Lists.newArrayList();
				Iterator<Expr> exprItr = request.getExprColl().iterator();

				// iterate each recurring constant expr
				// replace duplicated expressions
				while (exprItr.hasNext()) {
					Expr recurringExpr = exprItr.next();
					// create var read reference to the newly created variable
					// don't record yet; decide whether to use the newly created var directly or in
					// a boolean expr
					TransformRes<VarAccess> varReadCreate = astBuilder.createVarReadAcc(varDecl.getID(),
							varDecl.getName());
					varReadIds.add(varReadCreate.get().getBlockId());
					if (targetExpr.isBooleanOp()) {
						// variable = "true" expression for substitution because Scratch does not have
						// special Boolean type variable
						List<ArgBuildSpec> argSpecs = Lists.newArrayList(new ArgBuildSpec(varReadCreate.get(), null),
								new ArgBuildSpec(null, "true"));

						TransformRes<OperatorExpr> varIsTrueCreate = astBuilder
								.createExpr(astBuilder.createExpr("operator_equals", null, argSpecs)).record(actions);

						transHelper.replaceExpr(recurringExpr, varIsTrueCreate.get()).record(actions);
					} else {
						// var read expr for substitution
						varReadCreate.record(actions);
						transHelper.replaceExpr(recurringExpr, varReadCreate.get()).record(actions);
					}
				}
			}

			private Stmt findTargetStmt(List<Expr> exprColl) {
				Stmt target = null;
				for (Expr expr : exprColl) {
					Stmt topStmt = expr.script().getBody().getStmt(0);
					if (topStmt instanceof ExprStmt) {
						if (topStmt.getOpcode().equals("event_whenflagclicked")
//								&& expr.script().getBody().getNumStmt() > 1
								) {
							target = topStmt; 
//									expr.script().getBody().getStmt(1);
							break;
						}
					}
				}
				return target;
			}
		};

	}

}
