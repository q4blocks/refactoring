package anlys.refact;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import anlys.smell.DuplicateConstant;
import ast.Expr;
import ast.Program;
import lombok.Getter;
import lombok.Setter;

public class ExtractConstReq extends RefactRequest<DuplicateConstant> {
	@Getter
	@Setter
	List<Expr> exprColl;
	
	public ExtractConstReq(String smellId, DuplicateConstant smell, Program program) {
		super(smellId, smell, program);
		this.exprColl = smell.getElements().stream().map(el -> el.getExpr()).collect(Collectors.toList());
	}

	public static ExtractConstReq from(DuplicateConstant instance, Program program) {
		ExtractConstReq req = new ExtractConstReq(instance.getSmellId(), instance, program);
		return req;
	}
}
