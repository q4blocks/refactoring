package anlys.refact;

import java.text.ParseException;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ListMultimap;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.google.common.collect.Table;

import anlys.ScratchOpcodes;
import anlys.refact.precond.FailedPrecondException;
import anlys.refact.precond.PrecondChecker;
import anlys.refact.transform.ASTBuilder;
import anlys.refact.transform.ParamSpec;
import anlys.refact.transform.Snippet;
import anlys.refact.transform.TransformException;
import anlys.refact.transform.Transformer;
import anlys.refact.transform.TransformerHelper;
import anlys.refact.transform.action.Create;
import anlys.refact.transform.action.TransformRes;
import anlys.smell.SpriteCloneElement;
import ast.BlockSeq;
import ast.Expr;
import ast.ExprStmt;
import ast.Script;
import ast.Sprite;
import ast.Stmt;
import clone.analyzer.SerializationConfig;
import clone.analyzer.SpriteHashTable;
import clone.analyzer.SpriteHash;
import clone.data.StmtSeq;
import lombok.extern.slf4j.Slf4j;
import sb3.parser.ScriptConvert;
import sb3.parser.ScriptEl;
import sb3.parser.xml.Sb3XmlParser;
import util.CreatedEntityCounter;
import util.UtilFunc;

@Slf4j
public class ExtractParentSprite extends RefactAnalyzer<ExtractParentSpriteReq> {

	@Override
	protected PrecondChecker getPrecondChecker() {
		return new PrecondChecker() {

			@Override
			public void check() throws FailedPrecondException {
				Set<PrecondFailureType> types = Sets.newHashSet();
				List<Sprite> sprites = request.getSmell().getSprites().stream().map(SpriteCloneElement::getSprite)
						.collect(Collectors.toList());

				// not already have whenIStartAsClone
				List<Script> startAsCloneScripts = Lists.newArrayList(sprites.get(0).getScriptList()).stream()
						.filter(s -> s.getBody().getNumStmt() > 0)
						.filter(s -> s.getBody().getStmt(0).getOpcode()
								.equals(ScratchOpcodes.OPCODE_CONTROL_START_AS_CLONE))
						.collect(Collectors.toList());
				if (!startAsCloneScripts.isEmpty()) {
					types.add(PrecondFailureType.precond_contains_start_as_clone_script);
				}

				SpriteHashTable table = SpriteHashTable.of(sprites);
				Map<Integer, Table<Fragment, ParamSpec, Expr>> scriptHashToParamTable = computeScriptHashToParamTable(
						table);

				for (Sprite s : sprites) {
					if (s.getNumCostume() > 1) {
						types.add(PrecondFailureType.precond_multicostume);
						break;
					}

				}

				for (Table<Fragment, ParamSpec, Expr> parameterTable : scriptHashToParamTable.values()) {
					if (!parameterTable.isEmpty()) {
						log.debug("invalid variation exists: {}", parameterTable);
						types.add(PrecondFailureType.precond_invalid_variation_in_script);
						break;
					}
				}

				if (!types.isEmpty()) {
					throw new FailedPrecondException("failed precond", types);
				}

			}

			/*
			 * currently support no variation (e.g. identifier(var), literals)
			 * future: support private variable with same name
			 */
			private Map<Integer, Table<Fragment, ParamSpec, Expr>> computeScriptHashToParamTable(
					SpriteHashTable table) {

				Collection<SpriteHash> spriteHashes = table.get().values();
				// group same script together
				ListMultimap<Integer, StmtSeq> matchedScriptSeq = ArrayListMultimap.create();
				for (SpriteHash h : spriteHashes) {
					h.getStmtSeqSet().stream().forEach(seq -> matchedScriptSeq.put(seq.hash(), seq));
				}

				Map<Integer, Table<Fragment, ParamSpec, Expr>> scriptHashToParamTable = Maps.newHashMap();

				SerializationConfig includeAllInputAndFieldConfig = new SerializationConfig.Builder().skipInput(false)
						.skipField(false).build();

				for (Integer hashKey : matchedScriptSeq.keys()) {
					List<Fragment> matchedScriptfragments = matchedScriptSeq.get(hashKey).stream()
							.map(seq -> new Fragment(seq.get(), includeAllInputAndFieldConfig))
							.collect(Collectors.toList());
					ParameterAnalyzer analyzer = new ParameterAnalyzer();
					Table<Fragment, ParamSpec, Expr> parameterTable = analyzer.analyze(matchedScriptfragments)
							.getParameterTable();
					if (!parameterTable.isEmpty()) {
						scriptHashToParamTable.put(hashKey, parameterTable);
					}
				}
				return scriptHashToParamTable;
			}

		};
	}

	@Override
	protected Transformer getTransformer() {
		return new Transformer() {

			@Override
			public void computeActions() throws TransformException {
				// create a new sprite parent
				String spriteParentName = "parent";
				Sprite parentSprite = new TransformerHelper().createNewSprite(spriteParentName, program).record(actions)
						.get();

				// copy the only costume in each sprite duplicate to the created parent sprite
				// rename each costume to the name of the orginal duplicate sprite
				for (SpriteCloneElement spriteDuplicateEl : request.getSmell().getSprites()) {
					Sprite duplicateSprite = spriteDuplicateEl.getSprite();
					new TransformerHelper().copyCostume(parentSprite,
							duplicateSprite, duplicateSprite.getCostume(0).getName()).record(actions);
				}
				// create clone generator script in parent sprite
				createCloneGenScript(request.getSmell().getSprites(), parentSprite)
						.record(actions);

				// TODO: Create action has target field (nulll default to current Scratch
				// workspace's edittingTarget

				// create common clone initializer script in parent sprite
				Sprite replica = request.getSmell().getSprites().get(0).getSprite(); // pick one as the replica
				createDefaultWhenStartAsCloneScript(replica, parentSprite).record(actions);

				for (Script s : replica.getScriptList()) {
					// create clone's initializer scripts from sprite's initializer scripts
					if (s.getBody().getStmt(0).getOpcode().equals("event_whenflagclicked")) {
						createCloneInitializerScripts(s, parentSprite).record(actions);
					} else if (s.getBody().getStmt(0).getOpcode().equals("event_whenthisspriteclicked")) {
						// conversion to `when start as clone` entry script with forever loop
						createWhenClickedWorkaround(s, parentSprite).record(actions);
					} else {
						// simply make a copy of scripts
						replicateScript(s, parentSprite).record(actions);
					}

				}

				// remove duplicate sprites
				for (SpriteCloneElement spriteDuplicateEl : request.getSmell().getSprites()) {
					Sprite duplicateSprite = spriteDuplicateEl.getSprite();
					new TransformerHelper().removeSprite(duplicateSprite).record(actions);
				}

				// metadata
				result.setMetadata("sprites", request.getSmell().getSprites().stream().map(s -> s.getSpriteName())
						.collect(Collectors.toList()));

			}

			private TransformRes<Script> replicateScript(Script s, Sprite parentSprite) {
				Script copy = s.treeCopy().initCopy();
				parentSprite.addScript(copy);

				ScriptEl scriptEl = new ScriptConvert(copy).toScriptEl();
				String xml = "<xml>"+scriptEl.toXml().mkString()+"</xml>";
				return new TransformRes<>(new Create(xml, parentSprite.getName()), copy);

			}

			private TransformRes<Script> createWhenClickedWorkaround(Script script, Sprite parentSprite) {
				String scriptStr = "<block type='control_start_as_clone' id='Y^OJV(wr:O{C8LXT[pWp'><next><block type='control_forever' id=']ygLb@k;A3~n_,.]KNES'><statement name='SUBSTACK'><block type='control_if' id='uSu-3[21l=I(,Q2K)Pf6'><value name='CONDITION'><block type='operator_and' id='=7D[5E2~Hlpf:vN6Fskb'><value name='OPERAND1'><block type='sensing_mousedown' id='U6f5;]MGCYghE?f+(v0V'></block></value><value name='OPERAND2'><block type='sensing_touchingobject' id=';6).a6gK}9Y1@`ldsa[P'><value name='TOUCHINGOBJECTMENU'><shadow type='sensing_touchingobjectmenu' id='t0D3Sn4f#H;g$J+[2krw'><field name='TOUCHINGOBJECTMENU'>_mouse_</field></shadow></value></block></value></block></value><statement name='SUBSTACK'><block type='control_wait_until' id='_wait_until_not_clicked_'><value name='CONDITION'><block type='operator_not' id='N#r5^4W;K-i+YxxzwZ)w'><value name='OPERAND'><block type='sensing_mousedown' id='/!?68c+:!krjk^?rU_bk'></block></value></block></value></block></statement></block></statement></block></next></block>";
				Script whenStartAsCloneScript = new Script(new Sb3XmlParser().parseBlockSeq(scriptStr));
				Snippet snippet = new Snippet(whenStartAsCloneScript.getBody());

				ExprStmt targetToInsertAbove = (ExprStmt) snippet.get().lookupBlock("_wait_until_not_clicked_").get();

				Iterator<Stmt> stmtItr = script.getBody().getStmtList().iterator();
				stmtItr.next(); // skip whenclicked
				while (stmtItr.hasNext()) {
					Stmt stmt = stmtItr.next().treeCopy().updateId();
					new TransformerHelper().insertBefore(stmt.initCopy(), targetToInsertAbove);
				}
				// replace the place holder
				parentSprite.addScript(whenStartAsCloneScript);

				ScriptEl scriptEl = new ScriptConvert(whenStartAsCloneScript).toScriptEl();
				String xml = "<xml>"+scriptEl.toXml().mkString()+"</xml>";
				return new TransformRes<>(new Create(xml, parentSprite.getName()), whenStartAsCloneScript);
			}

			private TransformRes<Script> createCloneInitializerScripts(Script script, Sprite parentSprite) {
				Script whenStartAsCloneScript = new Script(new BlockSeq());

				// add when start as clone
				ExprStmt startcloneStmt = new ASTBuilder("", "")
						.createStmt(ScratchOpcodes.OPCODE_CONTROL_START_AS_CLONE,
								CreatedEntityCounter.getCountedName("blk"),
								Lists.newArrayList())
						.updateId();
				whenStartAsCloneScript.getBody().addStmt(startcloneStmt);

				// add original statement after when green flag block
				// TODO: needs check finding green flag script
				Script greenFlagScript = script;
				Iterator<Stmt> stmtItr = greenFlagScript.getBody().getStmtList().iterator();
				stmtItr.next(); // skip green flag
				while (stmtItr.hasNext()) {
					Stmt stmt = stmtItr.next().treeCopy().updateId();
					whenStartAsCloneScript.getBody().addStmt(stmt.initCopy());
				}

				parentSprite.addScript(whenStartAsCloneScript);

				ScriptEl scriptEl = new ScriptConvert(whenStartAsCloneScript).toScriptEl();
				String xml = "<xml>"+scriptEl.toXml().mkString()+"</xml>";
				return new TransformRes<>(new Create(xml, parentSprite.getName()), whenStartAsCloneScript);

			}

			/**
			 * When Start as Clone > show
			 * 
			 * @param spriteDuplicate
			 * @param parentSprite
			 * @return
			 */
			private TransformRes<Script> createDefaultWhenStartAsCloneScript(Sprite spriteDuplicate,
					Sprite parentSprite) {
				Script whenStartAsCloneScript = new Script(new BlockSeq());

				// add when start as clone
				ExprStmt startcloneStmt = new ASTBuilder("", "")
						.createStmt(ScratchOpcodes.OPCODE_CONTROL_START_AS_CLONE,
								CreatedEntityCounter.getCountedName("blk"),
								Lists.newArrayList())
						.updateId();
				whenStartAsCloneScript.getBody().addStmt(startcloneStmt);

				// show
				ExprStmt stmt1 = new ASTBuilder("", "").createStmt("looks_show",
						CreatedEntityCounter.getCountedName("blk"),
						Lists.newArrayList()).updateId();
				whenStartAsCloneScript.getBody().addStmt(stmt1);

				parentSprite.addScript(whenStartAsCloneScript);

				ScriptEl scriptEl = new ScriptConvert(whenStartAsCloneScript).toScriptEl();
				String xml = "<xml>"+scriptEl.toXml().mkString()+"</xml>";
				return new TransformRes<>(new Create(xml, parentSprite.getName()), whenStartAsCloneScript);
			}

			private TransformRes<Script> createCloneGenScript(List<SpriteCloneElement> spriteCloneList,
					Sprite parentSprite) throws TransformException {
				Script cloneGenScript = new Script(new BlockSeq());
				// when green flag clicked
				ExprStmt gfStmt = new ASTBuilder("", "").createStmt("event_whenflagclicked",
						CreatedEntityCounter.getCountedName("blk"),
						Lists.newArrayList()).updateId();
				cloneGenScript.getBody().addStmt(gfStmt);
				// hide
				ExprStmt hideStmt = new ASTBuilder("", "").createStmt("looks_hide",
						CreatedEntityCounter.getCountedName("blk"),
						Lists.newArrayList()).updateId();
				cloneGenScript.getBody().addStmt(hideStmt);

				for (SpriteCloneElement spriteDuplicateEl : request.getSmell().getSprites()) {
					Sprite spr = spriteDuplicateEl.getSprite();
					// looks_switchcostumeto
					ExprStmt switchCostumeStmt = new ASTBuilder("", "").createFromTemplate("looks_switchcostumeto",
							Lists.newArrayList(spr.getCostume(0).getName()).toArray()).updateId();
					cloneGenScript.getBody().addStmt(switchCostumeStmt);

					// preserving position
					try {
						ExprStmt gotoStmt = new ASTBuilder("", "").createFromTemplate("motion_gotoxy",
								Lists.newArrayList(
										UtilFunc.formatNumString(spr.getAttrs().get("x"), 0),
										UtilFunc.formatNumString(spr.getAttrs().get("y"), 0)).toArray())
								.updateId();
						cloneGenScript.getBody().addStmt(gotoStmt);
					} catch (ParseException e) {
						throw new TransformException(
								"Unable to preserve sprite's location: unable to parse x, y coordinate");
					}

					// preserving size if duplicate sprite's size is not the default size (100%)
					if (!spr.getAttrs().get("size").equals("100")) {
						try {
							ExprStmt setSize = new ASTBuilder("", "").createFromTemplate("looks_setsizeto",
									Lists.newArrayList(
											UtilFunc.formatNumString(spr.getAttrs().get("size"), 0))
											.toArray())
									.updateId();
							cloneGenScript.getBody().addStmt(setSize);
						} catch (ParseException e) {
							throw new TransformException(
									"Unable to preserve sprite's size: failing to parse sprite's size attribute");
						}
					}

					// create clone of myself
					ExprStmt createcloneStmt = new ASTBuilder("", "").createFromTemplate("control_create_clone_of",
							Lists.newArrayList("_myself_").toArray()).updateId();
					cloneGenScript.getBody().addStmt(createcloneStmt);
				}

				parentSprite.addScript(cloneGenScript);

				return new TransformRes<>(Create.of(cloneGenScript, parentSprite.getName()), cloneGenScript);
			}

		};

	}

}
