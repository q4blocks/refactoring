package anlys.refact;

import java.util.List;
import java.util.stream.Collectors;

import anlys.smell.DuplicateSprite;
import ast.Program;
import ast.Sprite;
import clone.analyzer.SpriteHashTable;

public class ExtractParentSpriteReq extends RefactRequest<DuplicateSprite> {

	public ExtractParentSpriteReq(String id, DuplicateSprite smell, Program program) {
		super(id, smell, program);
	}

	public static ExtractParentSpriteReq from(DuplicateSprite instance, Program program) {
		ExtractParentSpriteReq req = new ExtractParentSpriteReq(instance.getSmellId(), instance, program);

		return req;
	}

}
