package anlys.refact;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.google.common.collect.Table;
import com.google.common.collect.Table.Cell;

import anlys.refact.precond.BlockInputChecker;
import anlys.refact.precond.ConstantExprChecker;
import anlys.refact.precond.FailedPrecondException;
import anlys.refact.precond.PrecondChecker;
import anlys.refact.transform.ASTBuilder;
import anlys.refact.transform.ArgBuildSpec;
import anlys.refact.transform.ParamSpec;
import anlys.refact.transform.Snippet;
import anlys.refact.transform.TransformException;
import anlys.refact.transform.Transformer;
import anlys.refact.transform.TransformerHelper;
import anlys.refact.transform.action.Create;
import anlys.refact.transform.action.TransformRes;
import ast.Expr;
import ast.ProcAccess;
import ast.ProcDecl;
import ast.Program;
import ast.Scriptable;
import ast.VarAccess;
import lombok.extern.slf4j.Slf4j;
import sb3.parser.ProcDeclConvert;
import sb3.parser.ProcDefEl;
import util.CreatedEntityCounter;

@Slf4j
public class ExtractProcedure extends RefactAnalyzer<ExtractProcedureReq> {
	private Table<Fragment, ParamSpec, Expr> paramTable;

	@Override
	public void initialize() {
		super.initialize();
		paramTable = request.getParamTable();
	}

	@Override
	public PrecondChecker getPrecondChecker() {
		return new PrecondChecker() {
			@Override
			public void check() throws FailedPrecondException {
				Set<PrecondFailureType> types = Sets.newHashSet();
				StringBuilder failedMsgBuilder = new StringBuilder();
				for (Cell<Fragment, ParamSpec, Expr> c : paramTable.cellSet()) {
					try {
						new ConstantExprChecker(c.getValue()).check();
					} catch (FailedPrecondException e) {
						failedMsgBuilder.append(e.getMessage());
						types.addAll(e.getTypes());
					}
					try {
						new BlockInputChecker(c.getValue()).check();
					} catch (FailedPrecondException e) {
						failedMsgBuilder.append(e.getMessage());
						types.addAll(e.getTypes());
					}

					// TODO precond: no stop command in the segment
				}
				if (!types.isEmpty()) {
					throw new FailedPrecondException(failedMsgBuilder.toString(), types);
				}

			}
		};
	}

	@Override
	public Transformer getTransformer() {
		return new Transformer() {

			@Override
			public void computeActions() throws TransformException {
				ProcDecl procedureDef = null;
				TransformRes<ProcDecl> procedureDefCreateRes = createProcDecl().record(actions);
				procedureDef = procedureDefCreateRes.getNode();

				// create procedure ProcAccess call & replace each clone fragment with call
				for (Fragment fragment : paramTable.rowKeySet()) {
					// create procAcc
					Map<String, ArgBuildSpec> id2ArgSpec = new HashMap<>();
					for (ParamSpec paramSpec : paramTable.row(fragment).keySet()) {
						Expr expr = paramTable.get(fragment, paramSpec);
						id2ArgSpec.put(paramSpec.getId(), ArgBuildSpec.of(expr));
					}
					TransformRes<ProcAccess> procAccRes = new ASTBuilder.ProcAccessBuilder(procedureDef)
							.arguments(id2ArgSpec).build().record(actions);
					new TransformerHelper(program).replaceStmt(fragment.getStmtIds(), procAccRes.getNode())
							.record(actions);
				}

				// Clone seq with NO parameters (paramTable is empty)
				if (paramTable.isEmpty()) {
					for (Fragment seg : request.getFragments()) {
						Map<String, ArgBuildSpec> id2ArgSpec = new HashMap<>();
						TransformRes<ProcAccess> procAccRes = new ASTBuilder.ProcAccessBuilder(procedureDef)
								.arguments(id2ArgSpec).build().record(actions);

						new TransformerHelper(program).replaceStmt(seg.getStmtIds(), procAccRes.getNode())
								.record(actions);
					}
				}

				program = program.deepCopy(); // to free old nodes being replaced

				// metadata
				result.setMetadata("num_params", paramTable.columnKeySet().size());
			}

			private TransformRes<ProcDecl> createProcDecl() throws TransformException {
				Scriptable target = null;
				ProcDecl procDecl = null;

				target = program.getTargetByName(request.getTarget()).orElseThrow(NullPointerException::new);
				Fragment fragment = request.getFragments().iterator().next();
				String initProcCode = "DoSomething" + CreatedEntityCounter.getCount(ProcDecl.class.getSimpleName());
				List<ParamSpec> paramSpecs = Lists.newArrayList();
				for (ParamSpec paramSpec : paramTable.row(fragment).keySet()) {
					paramSpecs.add(paramSpec);
				}

				// make a copy of the first instance to be used in ProcDecl
				Snippet bodySnippet = new Snippet();
				request.getFragments().get(0).getStmts()
						.forEach(stmt -> bodySnippet.addStmt(stmt.treeCopy().initCopy()));

				ProcDecl procDeclTmp = new ASTBuilder.ProcDeclBuilder(initProcCode, initProcCode + "_id")
						.setParamDeclListFromSpecs(paramSpecs).addBody(bodySnippet.getBody().treeCopy().initCopy())
						.build();
				Snippet procDeclSnippet = new Snippet();
				procDeclSnippet.target().addProcDecl(procDeclTmp);

				// use 1st clone part to parameterize proc's body then repl exprs w/ param

				for (ParamSpec paramSpec : paramTable.row(fragment).keySet()) {
					// create paramAcc
					VarAccess paramAcc = new ASTBuilder("blk", "var").createParamAccess(paramSpec);
					// replace target expression with its corresponding parameter access
					Expr targetExpr = paramTable.get(fragment, paramSpec);

					new TransformerHelper(procDeclSnippet.get()).replaceExpr(targetExpr.getId(), paramAcc);

					procDeclSnippet.get().flushAttrAndCollectionCache(); // make sure tree is up to date
					procDeclSnippet.get().flushTreeCache();
				}
				procDecl = procDeclTmp.treeCopy().initCopy().updateId();
				target.addProcDecl(procDecl);

				ProcDefEl procDefEl = new ProcDeclConvert(procDecl).toProcDefEl();
				String xml = "<xml>" + procDefEl.toXml().mkString() + "</xml>";

				return new TransformRes<>(new Create(xml), procDecl);

			}

		};
	}

}
