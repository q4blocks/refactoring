package anlys.refact;

import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;

import com.google.common.collect.Lists;
import com.google.common.collect.Table;

import anlys.refact.transform.ParamSpec;
import anlys.smell.DuplicateCode;
import ast.Expr;
import ast.Program;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class ExtractProcedureReq extends RefactRequest<DuplicateCode> {

	private List<Fragment> fragments;
	private Table<Fragment, ParamSpec, Expr> paramTable;
	private String target;

	public ExtractProcedureReq(String smellId, DuplicateCode smell, Program program) {
		super(smellId, smell, program);
	}

	public static ExtractProcedureReq from(DuplicateCode instance, Program program) throws Exception {
		ExtractProcedureReq req = new ExtractProcedureReq(instance.getSmellId(), instance, program);

		req.setFragments(instance.getFragments());

		// set target from any block in any clone segment
		req.setTarget(instance.getTarget());
		if (req.getTarget() == "null") {
			throw new Exception("Failure constructing refactoring request");
		}
		ParameterAnalyzer analyzer = new ParameterAnalyzer();
		Table<Fragment, ParamSpec, Expr> parameterTable = analyzer.analyze(instance.getFragments()).getParameterTable();
		req.setParamTable(parameterTable);

		return req;
	}

	public static ExtractProcedureReq from(String target, List<List<String>> stmtIdFragmentList, Program program) {
		ExtractProcedureReq req = new ExtractProcedureReq("", null, program);
		List<Fragment> fragments = Lists.newArrayList();
		for(List<String> stmtIds : stmtIdFragmentList) {
			fragments.add(new Fragment(stmtIds, program)); 
		}
		req.setFragments(fragments);
		req.setTarget(target);

		ParameterAnalyzer analyzer = new ParameterAnalyzer();
		Table<Fragment, ParamSpec, Expr> parameterTable = analyzer.analyze(fragments).getParameterTable();
		req.setParamTable(parameterTable);

		return req;
	}

	@Override
	public String toString() {
		return "ExtractProcRequest [" + (id != null ? "id=" + id + ", " : "")
				+ (program != null ? "program=" + program + ", " : "")
				+ (fragments != null ? "segments=" + fragments + ", " : "")
				+ (paramTable != null ? "paramTable=" + paramTable + ", " : "")
				+ (target != null ? "target=" + target : "") + "]";
	}

}
