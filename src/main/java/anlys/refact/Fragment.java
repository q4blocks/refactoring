package anlys.refact;

import java.util.List;
import java.util.stream.Collectors;

import com.fasterxml.jackson.annotation.JsonIgnore;

import ast.ASTNode;
import ast.Expr;
import ast.Program;
import ast.Stmt;
import clone.analyzer.ASTNodeSerializer;
import clone.analyzer.SerializationConfig;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Getter
@Setter

public class Fragment {
	@JsonIgnore
	private List<Stmt> stmts;
	@JsonIgnore
	private List<Expr> orderedInputExprs;
	private List<String> stmtIds;

	public static final SerializationConfig serializeIgnoreSubExprConfig = new SerializationConfig.Builder()
			.skipInput(false)
			.skipChildExpr(true).build();
	@JsonIgnore
	private SerializationConfig config = null;

	public Fragment() {
		this.config = serializeIgnoreSubExprConfig;
	}

	public Fragment(List<Stmt> stmtSeq) {
		this(stmtSeq, serializeIgnoreSubExprConfig); // Default for extract precedure
	}

	public Fragment(List<Stmt> stmtSeq, SerializationConfig customConfig) {
		this.config = customConfig;

		stmts = stmtSeq;
		stmtIds = stmtSeq.stream().map(stmt -> stmt.getId()).collect(Collectors.toList());
		orderedInputExprs = serializeIgnoreSubExpr(stmtSeq).stream().filter(n -> n instanceof Expr).map(n -> (Expr) n)
				.collect(Collectors.toList());
		log.trace("{}", orderedInputExprs);
	}

	public Fragment(List<String> stmtIds, Program program) {
		this.stmtIds = stmtIds;
		//need test
		this.config = serializeIgnoreSubExprConfig;
		bind(program);
	}
	
	public void bind(Program program) {
		stmts = stmtIds.stream().map(id -> program.lookupStmt(id)).collect(Collectors.toList());
		orderedInputExprs = serializeIgnoreSubExpr(stmts).stream().filter(n -> n instanceof Expr).map(n -> (Expr) n)
				.collect(Collectors.toList());
	}

	private List<ASTNode> serializeIgnoreSubExpr(List<Stmt> stmtSeq) {

		return new ASTNodeSerializer(config).serializeASTSeq(stmtSeq);
	}

	@Override
	public String toString() {
		return "\n"
				+ (stmts != null ? stmts.stream().map(stmt -> stmt.toDevString()).collect(Collectors.toList()) : "");
	}

}
