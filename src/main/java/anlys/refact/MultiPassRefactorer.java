package anlys.refact;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import anlys.smell.CodeSmell;
import anlys.smell.SmellAnalyzer;
import anlys.smell.SmellListResult;
import ast.Program;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class MultiPassRefactorer {
	private Program program;
	private String projectId;

	public MultiPassRefactorer(Program program) {
		this.program = program;
	}

	public MultiPassRefactorer(Program program, String projectId) {
		this(program);
		this.projectId = projectId;
	}

	public void postRun() {
		program.flushAttrAndCollectionCache();
		program.flushTreeCache();
	}

	public <T extends CodeSmell, R extends RefactRequest<T>> Map<String, Object> run(SmellAnalyzer<T> smellAnalyzer,
			RefactAnalyzer<R> refactAnalyzer) throws Exception {
		int i = 0;
		int limit = 1;
		Set<String> processedIds = new HashSet<>();
		List<T> processedInstances = new ArrayList<>();
		Map<String, Object> runMetrics = new HashMap<>();
		int successCount = 0;

		refactAnalyzer.setProjectId(projectId);

		SmellListResult<T> results = smellAnalyzer.analyze(program);

		log.debug("{} initially detected {} ", smellAnalyzer.getClass().getSimpleName(), results.size());

		Iterator<T> smellItr = results.getInstances().iterator();
		boolean unchanged = false;

		Program previousCopy = program.deepCopy();

		while (!Thread.currentThread().isInterrupted()) {
			RefactResult refactRes = null;
			try {

				if (!smellItr.hasNext()) {
					// somehow needs to update program AST before smell analysis
					program.flushTreeCache();
					program.flushCache();

					results = smellAnalyzer.analyze(program);
					results.getInstances().removeIf(smell -> processedIds.contains(smell.getSmellId()));
					log.debug("#{}: {} detected {} new smells", i, smellAnalyzer.getClass().getSimpleName(),
							results.size());
					smellItr = results.getInstances().iterator();
					if (results.getInstances().isEmpty()) {
						unchanged = true;
					}
				}

				if (unchanged || i > limit) {
					break;
				}

				T smell = smellItr.next();

				if (processedIds.contains(smell.getSmellId())) {
					continue;
				}

				@SuppressWarnings("unchecked")
				// safe casting :smell of type T converted to R (RefactRequest for smell type T)
				R refactReq = (R) smell.buildRefactRequest(program);

				log.debug("process smell {}", refactReq.getId());

				refactRes = refactAnalyzer.analyze(program, refactReq);
				log.debug("{}", refactRes.toJson());

				processedIds.add(refactReq.getId());
				processedInstances.add(smell);

				previousCopy = program.deepCopy();

				i++;
			} catch (InterruptedException e) {
				log.error("Analysis of {} is interrupted", projectId, e);
				Thread.currentThread().interrupt(); // restore flag
				program = previousCopy; // revert to previous copy
			}
			if (refactRes != null && refactRes.getMetadata("success").equals(true)) {
				successCount++;
			}

			postRun();

		}
		runMetrics.put("changed", successCount);

		return runMetrics;
	}
}
