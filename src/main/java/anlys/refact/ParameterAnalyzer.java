package anlys.refact;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Lists;
import com.google.common.collect.Table;

import anlys.refact.transform.ParamSpec;
import ast.Expr;
import clone.analyzer.ASTNodeHasher;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ParameterAnalyzer {
	Table<Fragment, Integer, Expr> fragRowPosColumnToExprTable = HashBasedTable.create();
	Table<Fragment, ParamSpec, Expr> segmentParamInputExpr = HashBasedTable.create();
	List<List<Expr>> constants = Lists.newArrayList();

	public ParameterAnalyzer analyze(List<Fragment> fragmentList) {
		for (Fragment s : fragmentList) {
			for (int i = 0; i < s.getOrderedInputExprs().size(); i++) {
				fragRowPosColumnToExprTable.put(s, i, s.getOrderedInputExprs().get(i));
			}
		}

		List<Integer> constantExprInputs = Lists.newArrayList();

		for (Integer positionColumn : fragRowPosColumnToExprTable.columnKeySet()) {
			// compute a set of hashes
			Set<Integer> hashesOfInputExpr = fragRowPosColumnToExprTable.column(positionColumn).values().stream()
					.map(expr -> new ASTNodeHasher(true).hashOfExpr(expr))
					.collect(Collectors.toSet());

			// a resulting set of only one hash means inputExprs at the current pos is
			// constant and need not be made parameter
			if (hashesOfInputExpr.size() <= 1) {
				constantExprInputs.add(positionColumn);
			}
		}

		// dropping inputPos corresponds to constant expr. across different segments
		for (Integer inputPos : constantExprInputs) {
			constants.add(Lists.newArrayList(fragRowPosColumnToExprTable.column(inputPos).values()));
			fragRowPosColumnToExprTable.column(inputPos).clear();
		}

		// prepare output table
		for (Fragment s : fragRowPosColumnToExprTable.rowKeySet()) {
			int n = 0;
			for (Integer inputPosColumn : fragRowPosColumnToExprTable.columnKeySet()) {
				String name = "param" + n++;
				Map<Fragment, Expr> tmp = fragRowPosColumnToExprTable.column(inputPosColumn);
				Expr inputExpr = fragRowPosColumnToExprTable.get(s, inputPosColumn);

				if (inputExpr == null) {
					log.error("TODO: fix :{}", inputExpr);
				}
				segmentParamInputExpr.put(s, new ParamSpec(name, name + "_ID", inputExpr.isBooleanOp()), inputExpr);
			}
		}

		return this;
	}

	/**
	 * a table with column represent parameter spec; row represents a duplicate
	 * segment for a given segment, a cell represents an input expression to be
	 * substituted by its corresponding parameter
	 * 
	 */

	public Table<Fragment, ParamSpec, Expr> getParameterTable() {
		return segmentParamInputExpr;
	}

	public List<List<Expr>> getConstantExprInputs() {
		return constants;
	}

}
