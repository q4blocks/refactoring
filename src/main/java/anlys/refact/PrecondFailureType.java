package anlys.refact;

import anlys.metrics.MetricKey;

public enum PrecondFailureType implements MetricKey {
	/*
	 * e.g. Call(time) but time variable varies in the original code fragment where
	 * it's being accessed (especially when there is a block construct that result
	 * in control flow switch possibly causing certain variable to change).
	 */
	precond_non_constant_non_localvar, precond_non_constant_operator, precond_non_constant_attribute,
	/*
	 * e.g. when I receive <event input>
	 */
	precond_non_parameterizable_block_input,

	/*
	 * when segment will change its orginal control flow e.g. when the segment
	 * contains stop block
	 */
	precond_invalid_segment,

	/*
	 * non literal (when extract loop with loop data)
	 */
	precond_non_literal_loop_data, 
	
	/*
	 * extract parent sprite
	 */
	precond_multiscript, precond_multicostume, precond_non_greenflag_entry, precond_invalid_variation_in_script, precond_contains_start_as_clone_script
}
