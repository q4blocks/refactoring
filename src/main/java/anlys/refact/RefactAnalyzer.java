package anlys.refact;

import anlys.Recordable;
import anlys.refact.precond.FailedPrecondException;
import anlys.refact.precond.PrecondChecker;
import anlys.refact.transform.TransformException;
import anlys.refact.transform.Transformer;
import anlys.refact.transform.action.ActionList;
import anlys.smell.CodeSmell;
import ast.Program;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public abstract class RefactAnalyzer<T extends RefactRequest<?>> {

	protected Program program;
	@Getter
	protected RefactResult result;
	protected PrecondChecker checker;
	protected Transformer transformer;

	@Setter
	protected String projectId;

	protected T request;

	protected Recordable requestRecorder;
	@Getter
	protected Recordable refactRecorder;

	public RefactAnalyzer() {
		this.checker = getPrecondChecker();
		this.transformer = getTransformer();
	}

	public void setSmellMetricsRecorder(Recordable recorder) {
		this.requestRecorder = recorder;
	}

	public void setRefactRecorder(Recordable recorder) {
		this.refactRecorder = recorder;
	}

	private void recordSmellMetrics(CodeSmell smell) {
		if (this.requestRecorder != null) {
			if (projectId != null) {
				smell.getMetadata().put("_id", projectId + "_" + smell.getSmellId());
				smell.getMetadata().put("projectId", projectId);
			}
			this.requestRecorder.record(smell.getMetadata());
		}
	}

	/**
	 * initialize() get called first in analyze() before precondition check
	 * method override should call super.initialize() to properly setup the analyzer
	 */

	public void initialize() {
		this.result = new RefactResult(request.getId());
		result.setMetadata("_id", request.getId());
		recordSmellMetrics(request.getSmell());
	}

	public RefactResult analyze(Program program, T request) {
		this.program = program;
		this.request = request;

		return analyze();
	}

	private RefactResult analyze() {
		initialize();
		boolean isSuccess = true;

		try {
			preconditionChecks();
		} catch (FailedPrecondException e) {
			for (PrecondFailureType failureType : e.getTypes()) {
				result.setMetadata(failureType, true);
			}
			isSuccess = false;
			log.error("Failed Preconditions: {}", e.getTypes());
		}

		if (isSuccess) {
			try {
				computeTransformation();
			} catch (TransformException transformError) {
				isSuccess = false;
				log.error("Failed Transform: {}", transformError.getMessage());
			}
		}

		result.setMetadata("success", isSuccess);
		recordRefactMetrics(result);
		return result;
	}

	private void recordRefactMetrics(RefactResult result) {
		if (this.refactRecorder != null) {
			if (projectId != null) {
				result.getMetadata().put("_id", projectId + "_" + result.getSmellId());
				result.getMetadata().put("projectId", projectId);
			}
			this.refactRecorder.record(result.getMetadata());
		}
	}

	private void preconditionChecks() throws FailedPrecondException {
		checker.check();
	}

	private void computeTransformation() throws TransformException {
		try {
			transformer.clearActions();
			transformer.computeActions();
			result.setActions(transformer.getActions());
		} catch (Exception e) {
			log.error("TODO: revert program to previous one before processing this request");
			result.setActions(new ActionList());
			throw e;
		}
	}

	protected abstract PrecondChecker getPrecondChecker();

	protected abstract Transformer getTransformer();

}
