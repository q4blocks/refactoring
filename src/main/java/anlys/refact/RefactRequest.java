package anlys.refact;

import anlys.smell.CodeSmell;
import ast.Program;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public abstract class RefactRequest<T extends CodeSmell> {
	protected final String id;
	protected final Program program;
	protected final T smell;

	public RefactRequest(String id, T smell, Program program) {
		this.id = id;
		this.smell = smell;
		this.program = program;
	}
}
