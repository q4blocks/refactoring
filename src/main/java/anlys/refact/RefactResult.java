package anlys.refact;

import java.io.IOException;

import com.fasterxml.jackson.annotation.JsonUnwrapped;
import com.fasterxml.jackson.databind.ObjectMapper;

import anlys.AnalysisResult;
import anlys.refact.transform.action.ActionList;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@EqualsAndHashCode(callSuper=false)
@NoArgsConstructor
@Getter
@Setter
public class RefactResult extends AnalysisResult {
	private String smellId;
	@JsonUnwrapped
	private ActionList actions;

	/**
	 * 
	 * @param smellId
	 *            unique id that maps the smell to its computed refactoring result
	 */
	public RefactResult(String smellId) {
		this.smellId = smellId;
	}

	public static RefactResult fromJson(String jsonStr) throws Exception {
		ObjectMapper mapper = new ObjectMapper();
		RefactResult instance;
		try {
			instance = mapper.readValue(jsonStr, RefactResult.class);
			return instance;
		} catch (IOException e) {
			throw new Exception("Error RefactResult deserialization", e);
		}
	}
	
	
}
