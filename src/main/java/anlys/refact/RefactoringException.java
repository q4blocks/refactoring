package anlys.refact;

public abstract class RefactoringException extends Exception {
	public RefactoringException() {
		super();
	}
	
	public RefactoringException(String msg) {
		super(msg);
	}
}
