package anlys.refact;

import java.util.HashSet;

import anlys.refact.precond.FailedPrecondException;
import anlys.refact.precond.PrecondChecker;
import anlys.refact.transform.ASTBuilder;
import anlys.refact.transform.TransformException;
import anlys.refact.transform.Transformer;
import anlys.refact.transform.TransformerHelper;
import anlys.refact.transform.action.TransformRes;
import ast.Expr;
import ast.ReachedUse;
import ast.Scriptable;
import ast.Sprite;
import ast.VarAccess;
import ast.VarDecl;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class RescopeVar extends RefactAnalyzer<RescopeVarReq> {

	@Override
	public PrecondChecker getPrecondChecker() {
		return new PrecondChecker() {

			@Override
			public void check() throws FailedPrecondException {
				log.debug("TODO: precondition check for reduce scope var");
			}
		};
	}

	@Override
	public Transformer getTransformer() {
		return new Transformer() {

			@Override
			public void computeActions() throws TransformException {
				VarDecl variable = request.getVariable();
				String savedName = variable.getName();

				Scriptable targetLocalScope = request.getTargetScope();

				// rename var first
				new TransformerHelper(program)
						.renameVar(variable, "_" + savedName, variable.sprite())
						.record(actions);

				// create new local var
				TransformRes<VarDecl> newVar = new TransformerHelper().declareVar(
						targetLocalScope, savedName, "local_" + variable.getID()).record(actions);
				VarDecl varDecl = newVar.get();

				// replace all used
				ASTBuilder astBuilder = new ASTBuilder("blk", "var");
				for (ReachedUse use : variable.allUsesInProgram()) {
					VarAccess oldUse = (VarAccess) use;
					Expr newVariableReadExpr;
					if (oldUse.sprite() == targetLocalScope) {
						newVariableReadExpr = astBuilder.createVarReadAcc(varDecl.getID(),
								varDecl.getName()).record(actions).get();
					} else {
						newVariableReadExpr = astBuilder.createAttrReadOpExpr(varDecl.getName(),
								targetLocalScope.getName()).record(actions).get();// TODO
					}

					new TransformerHelper(program).replaceExpr((VarAccess) use, newVariableReadExpr).record(actions);

				}

				// remove
				new TransformerHelper(program).deleteVar(variable).record(actions);

			}
		};
	}

}
