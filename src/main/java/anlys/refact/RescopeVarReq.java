package anlys.refact;

import anlys.smell.BroadScopeVar;
import ast.Program;
import ast.Scriptable;
import ast.VarDecl;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class RescopeVarReq extends RefactRequest<BroadScopeVar> {
	VarDecl variable;
	Boolean toLocal;
	Scriptable targetScope;

	public RescopeVarReq(String id, BroadScopeVar instance, Program program) {
		super(id, instance, program);
		this.variable = instance.getVar();
		this.targetScope = instance.getDefScope();
	}

	public static RescopeVarReq from(BroadScopeVar instance, Program program) {
		return new RescopeVarReq(instance.getSmellId(), instance, program);
	}

}