package anlys.refact.precond;

import java.util.Set;

import com.google.common.collect.Sets;

import anlys.refact.PrecondFailureType;
import ast.Expr;
import ast.ScratchBlock;
import ast.Stmt;

public class BlockInputChecker implements PrecondChecker {
	private Expr inputExpr;
	Set<String> invalidBlockInputs = Sets.newHashSet(
			"motion_setrotationstyle",
			"event_whenkeypressed",
			"looks_changeeffectby",
			"looks_seteffectto",
			"looks_gotofrontback",
			"looks_goforwardbackwardlayers",
			"looks_costumenumbername",
			"looks_backdropnumbername",
			"sound_changeeffectby",
			"sound_seteffectto",
			"event_whenkeypressed",
			"event_whenbackdropswitchesto",
			"event_whengreaterthan",
			"event_whenbroadcastreceived",
			"control_stop",
			"sensing_touchingobject",
			"sensing_keyoptions",
			"sensing_setdragmode",
			"sensing_of",
			"sensing_current",
			"operator_mathop",
			"data_setvariableto",
			"data_changevariableby",
			"data_showvariable",
			"data_hidevariable");

	public BlockInputChecker(Expr inputExpr) {
		this.inputExpr = inputExpr;
	}

	/**
	 * check that value variation located in block input that accept block input
	 */
	@Override
	public void check() throws FailedPrecondException {
		Stmt parentStmt = inputExpr.parentStmt();
		String opcode = ((ScratchBlock) parentStmt).getOpcode();
		if (invalidBlockInputs.contains(opcode) && inputExpr.inputIndex() == 0) {
			throw new FailedPrecondException("First input slot of block cannot be parameterized",
					Sets.newHashSet(PrecondFailureType.precond_non_parameterizable_block_input));
		}
	}

}

class Pair {
	String left;
	Integer right;

	public Pair(String left, Integer right) {
		this.left = left;
		this.right = right;
	}

	public static Pair of(String opcode, Integer input) {
		return new Pair(opcode, input);
	}

}

/*
 * motion_setrotationstyle, event_whenkeypressed,
 * looks_changeeffectby(fieldName0:EFFECT)
 * looks_seteffectto(fieldName0:EFFECT)
 * looks_gotofrontback(field0)
 * looks_goforwardbackwardlayers field0
 * looks_costumenumbername field0
 * looks_backdropnumbername field0
 * sound_changeeffectby field0
 * sound_seteffectto field0
 * event_whenkeypressed field0
 * event_whenbackdropswitchesto field0
 * event_whengreaterthan field0
 * event_whenbroadcastreceived field0
 * control_stop field0
 * sensing_touchingobject field0
 * sensing_keyoptions field0
 * sensing_setdragmode field0
 * sensing_of field0
 * sensing_current field0
 * operator_mathop field0
 * data_setvariableto, data_changevariableby,data_showvariable,data_hidevariable
 */
