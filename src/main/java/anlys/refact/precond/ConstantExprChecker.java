package anlys.refact.precond;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;

import anlys.refact.PrecondFailureType;
import ast.ASTNode;
import ast.AttrAccess;
import ast.Expr;
import ast.OperatorExpr;
import ast.ReachingDef;
import ast.ScratchBlock;
import ast.Script;
import ast.VarAccess;
import clone.analyzer.ASTNodeSerializer;
import clone.analyzer.SerializationConfig;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ConstantExprChecker implements PrecondChecker {
	private Expr expr;

	public ConstantExprChecker(Expr expr) {
		this.expr = expr;
	}

	/**
	 * whether the given expression is constant
	 * 
	 */
	@Override
	public void check() throws FailedPrecondException {
		if (expr instanceof OperatorExpr) {
			targetExpressionIsConstant((OperatorExpr) expr);
		}
	}

	private void targetExpressionIsConstant(OperatorExpr expr) throws FailedPrecondException {
		OperatorExpr targetExpr = expr;
		Script script = targetExpr.script();
		SerializationConfig config = new SerializationConfig.Builder().skipField(false).skipInput(false).build();
		ASTNodeSerializer exprSerializer = new ASTNodeSerializer(config);
		List<ASTNode> nodes = exprSerializer.serialize(targetExpr);

		Set<ASTNode> blocks = nodes.stream().filter(n -> n instanceof ScratchBlock).collect(Collectors.toSet());
		Set<ASTNode> nonConstantOps = blocks.stream().filter(n -> isNonConstantBlockOpExpr(n))
				.collect(Collectors.toSet());

		Set<ASTNode> attrAccessBlocks = blocks.stream().filter(n -> isAttrAccess(n)).collect(Collectors.toSet());
		Set<ASTNode> nonLocalVars = blocks.stream().filter(n -> isVarAndNotLocallyDefined(n, script))
				.collect(Collectors.toSet());

		Set<PrecondFailureType> types = Sets.newHashSet();
		if (!nonConstantOps.isEmpty()) {
			types.add(PrecondFailureType.precond_non_constant_operator);
		}
		if (!attrAccessBlocks.isEmpty()) {
			types.add(PrecondFailureType.precond_non_constant_attribute);
		}
		if (!nonLocalVars.isEmpty()) {
			types.add(PrecondFailureType.precond_non_constant_non_localvar);
		}
		if (!types.isEmpty()) {
			String msg = "Target expression is not constant: " + targetExpr.toDevString();
			log.debug("Not constant: {}", targetExpr.toDevString());
			throw new FailedPrecondException(msg, types);
		}

	}

	private boolean isVarAndNotLocallyDefined(ASTNode n, Script script) {
		if (n instanceof VarAccess) {
			HashSet<ReachingDef> defs = new HashSet<>();
			try {
				// if variable access is a parameter then it's strictly local (cannot be
				// redefined and can only be used in the procedure body)
				if (((VarAccess) n).getType().startsWith("argument_reporter")) {
					return false;
				}
				defs = ((VarAccess) n).decl().allDefs();
			} catch (Exception e) {
				e.printStackTrace();
			}

			for (ReachingDef def : defs) {
				if (def instanceof VarAccess) {
					boolean notLocallyDefined = ((VarAccess) def).script() != script;
					log.debug("Not locally defined: " + def + ":" + notLocallyDefined);
					if (notLocallyDefined) {
						return notLocallyDefined;
					}
				}
			}
		}
		return false;
	}

	private boolean isNonConstantBlockOpExpr(ASTNode n) {
		// TODO precond: isNonConstantBlockExpr ADD MORE TO LIST
		Set<String> nonConstantValueOps = ImmutableSet.of("operator_random", "looks_costumenumbername",
				"motion_xposition", "motion_yposition", "motion_direction", "looks_costumenumbername",
				"looks_backdropnumbername", "looks_size", "sound_volume", "sensing_touchingobject",
				"sensing_touchingcolor", "sensing_coloristouchingcolor", "sensing_distanceto", "sensing_keypressed",
				"sensing_mousedown", "sensing_mousex", "sensing_mousey", "sensing_loudness", "sensing_timer",
				"sensing_of", "sensing_answer", "sensing_current", "CURRENTMENU", "sensing_dayssince2000");
		boolean isNonConstant = nonConstantValueOps.contains(((ScratchBlock) n).getOpcode());
		return isNonConstant;
	}

	private boolean isAttrAccess(ASTNode n) {
		boolean isAttrAcc = n instanceof AttrAccess;
		return isAttrAcc;
	}

}
