package anlys.refact.precond;

import java.util.HashSet;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import anlys.refact.transform.TransformerHelper;
import ast.AssignStmt;
import ast.Program;
import ast.Stmt;
import ast.VarAccess;

public class DefPreservAnalysis {
	private static final Logger LOGGER = LogManager.getLogger(DefPreservAnalysis.class);
	private final Program program;

	public DefPreservAnalysis(Program program) {
		this.program = program;
	}

	public Stmt determineAssignmentInsertTarget(AssignStmt assignStmt, List<VarAccess> varReadAccs) {
		Stmt insertTarget = null;
		Stmt moveUpStmt = assignStmt;
		
		// update // should be done 
		program.flushTreeCache();
		program.flushCache();
		// if already preserved just return the next stmt
		if (reachedByTargetDef(varReadAccs, assignStmt.getDest())) {
			return (Stmt) assignStmt.getNextSibling();
		}
		
		Stmt moveUpTarget = new TransformerHelper().getMoveUpBeforeTarget(moveUpStmt);
		while (moveUpTarget != null) {
			new TransformerHelper().moveUp(moveUpStmt);
			program.flushTreeCache();
			program.flushCache();
			if (reachedByTargetDef(varReadAccs, assignStmt.getDest())) {
				LOGGER.debug("Definition is preserved!");
				insertTarget = moveUpTarget;
				break;
			}
			// if definition is not preserved
			moveUpTarget = new TransformerHelper().getMoveUpBeforeTarget(moveUpStmt);
		}
		return insertTarget;

	}

	private boolean reachedByTargetDef(List<VarAccess> varReadAccs, VarAccess def) {
		for (VarAccess varRead : varReadAccs) {
			HashSet defs = null;
			try {
				defs = varRead.defs();
				boolean hasDef = defs.contains(def);
				boolean hasExactlyOneDef = defs.size() == 1;
				boolean reachedByTargetDef = hasDef && hasExactlyOneDef;
				if (!reachedByTargetDef) {
					System.out.println("At least one not reached: " + varRead.toDevString());
					return false;
				}
			} catch (Exception e) {
				e.printStackTrace();
				return false;
			}
		}
		return true;
	}

}
