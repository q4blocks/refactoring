package anlys.refact.precond;

import java.util.Set;

import anlys.refact.PrecondFailureType;
import anlys.refact.RefactoringException;
import lombok.Getter;

@SuppressWarnings("serial")
public class FailedPrecondException extends RefactoringException {


	final String msg;

	@Getter
	private final Set<PrecondFailureType> types;

	public FailedPrecondException(String msg, Set<PrecondFailureType> types) {
		super(msg);
		this.msg = msg;
		this.types = types;
	}

	@Override
	public String toString() {
		return "FailedPreconditionException [" + (msg != null ? "msg=" + msg + ", " : "")
				+ (types != null ? "types=" + types : "") + "]";
	}

}
