package anlys.refact.precond;

import anlys.refact.RefactoringException;

public class InvalidRefactParamException extends RefactoringException {

	public InvalidRefactParamException(String msg) {
		super(msg);
	}

}
