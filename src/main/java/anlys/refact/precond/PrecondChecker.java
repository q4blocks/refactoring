package anlys.refact.precond;

public interface PrecondChecker {


	public void check() throws FailedPrecondException;
}
