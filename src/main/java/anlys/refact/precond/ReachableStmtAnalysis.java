package anlys.refact.precond;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

import com.google.common.collect.Sets;

import ast.BlockSeq;
import ast.ExprStmt;
import ast.ProcDecl;
import ast.Program;
import ast.ScratchBlock;
import ast.Script;
import ast.Stmt;

public class ReachableStmtAnalysis {
	private Program program;

	public ReachableStmtAnalysis(Program program) {
		this.program = program;
	}

	public int getNumReachbleStmts() {
		return countStmt(program);
	}
	
	

	private int countStmt(Program program) {
		Set<Stmt> stmts = Sets.newHashSet();

		// go by script first
		// only the reachable one (those starts with event hat blocks)
		Set<Script> eventScripts = program.coll_scripts().stream()
				.filter(script -> script.getBody().getStmt(0).isEventBlock()).collect(Collectors.toSet());
		
		// for receive event script
//		Set<Script> reachableEventScripts = eventScripts.stream().filter(script -> script.reachable().size() > 0)
//				.collect(Collectors.toSet());
		// sum all stmts in the reachable scripts
		eventScripts.stream().forEach(script -> stmts.addAll(script.getBody().stmtColl()));

		// and procedure that are called
		Set<ProcDecl> reachableProcDecls = program.coll_procdecl().stream()
				.filter(procDecl -> procDecl.uses().size() > 0).collect(Collectors.toSet());

		reachableProcDecls.stream().forEach(procDecl -> {
			stmts.addAll(procDecl.getBody().stmtColl());
		});

		Set<Stmt> executableStmts = stmts.stream().filter(stmt -> !(stmt instanceof BlockSeq))
				.collect(Collectors.toSet());

		int procDeclHatBlocks = reachableProcDecls.size(); // hat blocks is executable
		executableStmts.stream().map(stmt->((ScratchBlock)stmt).getOpcode()).collect(Collectors.toList()).forEach(stmt->System.out.println(stmt));
		return executableStmts.size() + procDeclHatBlocks;
		
//		pen_setPenColorToColor
//		event_whenflagclicked
//		event_whenflagclicked
//		control_forever
//		data_setvariableto
//		control_if_else
//		control_wait
//		pen_clear
//		pen_setPenSizeTo
//		pen_clear
//		event_whenflagclicked
//		pen_setPenSizeTo
//		motion_movesteps
//		pen_penDown
//		motion_pointindirection
//		data_setvariableto
//		motion_ifonedgebounce
//		control_forever
//		control_forever
	}

}
