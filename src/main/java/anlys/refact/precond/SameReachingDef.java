package anlys.refact.precond;

import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Table;

import ast.ASTNode;
import ast.Expr;
import ast.OperatorExpr;
import ast.ReachingDef;
import ast.VarAccess;
import ast.VarDecl;
import clone.analyzer.ASTNodeSerializer;
import clone.analyzer.SerializationConfig;

public class SameReachingDef implements PrecondChecker {
	Set<Expr> exprColl;

	public SameReachingDef(Set<Expr> exprColl) {
		this.exprColl = exprColl;
	}

	@Override
	public void check() throws FailedPrecondException {
		// construct a table clone (column: vardecl, row: expr clone, cell: defs)
		Table<OperatorExpr, VarDecl, Set<ReachingDef>> opVarDefs = HashBasedTable.create();

		for (Expr exprBlock : exprColl) {
			SerializationConfig config = new SerializationConfig.Builder().skipField(false).skipInput(false).build();
			ASTNodeSerializer exprSerializer = new ASTNodeSerializer(config);
			List<ASTNode> nodes = exprSerializer.serialize(exprBlock);

			Set<VarAccess> uses = nodes.stream().filter(n -> n instanceof VarAccess).map(n -> (VarAccess) n)
					.filter(v -> v.isSource()).collect(Collectors.toSet());

			for (VarAccess use : uses) {
				if (use.getType().startsWith("argument_reporter")) { // skip parameters; they are always constant
					continue;
				}
				try {
					opVarDefs.put((OperatorExpr) exprBlock, use.decl(), use.defs());
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}

		// check for each var, its defs is the same for all expr clones
		for (VarDecl v : opVarDefs.columnKeySet()) {
			Iterator<OperatorExpr> itr = opVarDefs.column(v).keySet().iterator();
			OperatorExpr cloneExpr = itr.next();
			Set<ReachingDef> reachingDef0 = opVarDefs.get(cloneExpr, v);
			while (itr.hasNext()) {
				cloneExpr = itr.next();
				Set<ReachingDef> reachingDef1 = opVarDefs.get(cloneExpr, v);
				if (!reachingDef0.equals(reachingDef1)) {
					throw new FailedPrecondException("one of the variable has a different definiton set:"
							+ cloneExpr.enclosingStmt().toDevString(),null);
				}
			}
		}

	}

}
