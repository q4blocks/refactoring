package anlys.refact.transform;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.lang3.RandomStringUtils;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import anlys.refact.transform.action.Create;
import anlys.refact.transform.action.TransformRes;
import ast.AssignStmt;
import ast.BlockSeq;
import ast.Expr;
import ast.ExprStmt;
import ast.IfElseStmt;
import ast.IncrementStmt;
import ast.Literal;
import ast.OperatorExpr;
import ast.ParamDecl;
import ast.ProcAccess;
import ast.ProcDecl;
import ast.ScratchBlock;
import ast.SimpleBlock;
import ast.Stmt;
import ast.StrLiteral;
import ast.VarAccess;
import ast.VarDecl;
import lombok.extern.slf4j.Slf4j;
import sb3.data.Default;
import sb3.data.ParamDeclMeta;
import sb3.data.ProcAccMeta;
import sb3.data.ProcDeclMeta;
import sb3.parser.ParsingUtil;
import sb3.parser.ProcDeclConvert;
import sb3.parser.ProcDefEl;
import sb3.parser.xml.Sb3XmlParser;
import util.CreatedEntityCounter;

@Slf4j
public class ASTBuilder {
	private final String BLOCK_ID_PREFIX;
	private final String VAR_ID_PREFIX;

	public ASTBuilder(String blockIdPrefix, String varIdPrefix) {
		BLOCK_ID_PREFIX = blockIdPrefix;
		VAR_ID_PREFIX = varIdPrefix;
	}

	/*
	 * used internally for invariant code check generation
	 */
	public TransformRes<IncrementStmt> createIncrementStmt(VarDecl varDecl, String id) {
		VarAccess varAcc = createVarWriteAccess(varDecl);
		IncrementStmt incrStmt = new IncrementStmt();
		incrStmt.setDest(varAcc);
		incrStmt.setBlockId(id);
		incrStmt.setOpcode("data_changevariableby");
		incrStmt.initDefault(2);
		Default defaultVal = new Default("1");
		defaultVal.setShadowAttr(Maps.newHashMap(ImmutableMap.of("type", "text")));
		defaultVal.setFieldAttr(Maps.newHashMap(ImmutableMap.of("name", "TEXT")));
		incrStmt.setDefault(defaultVal, 1);

		incrStmt.setSource(defaultVal.toExpr());

		return new TransformRes<>(new Create(incrStmt), incrStmt);

	}

	public TransformRes<AssignStmt> createAssignment(VarDecl varDecl, Expr sourceExpr, String defaultValStr) {
		VarAccess varAcc = createVarWriteAccess(varDecl);

		AssignStmt assignStmt = new AssignStmt();
		assignStmt.setDest(varAcc);
		assignStmt.setBlockId(ParsingUtil.getRandomId(BLOCK_ID_PREFIX));
		assignStmt.setOpcode("data_setvariableto");
		assignStmt.initDefault(2);
			
		Default defaultVal = new Default(
				(sourceExpr instanceof Literal) ? ((Literal) sourceExpr).getValue() : defaultValStr); 
		defaultVal.setShadowAttr(Maps.newHashMap(ImmutableMap.of("type", "text")));
		defaultVal.setFieldAttr(Maps.newHashMap(ImmutableMap.of("name", "TEXT")));
		assignStmt.setDefault(defaultVal, 1);

		if (sourceExpr != null) {
			assignStmt.setSource(sourceExpr);
		} else {
			assignStmt.setSource(defaultVal.toExpr());
		}

		return new TransformRes<>(new Create(assignStmt), assignStmt);

	}

	public TransformRes<VarAccess> createVarReadAcc(String varId, String varName) {
		String varBlockId = varId + CreatedEntityCounter.getCount(varId);
		return createVarReadAccess(varId, varName, varBlockId);
	}

	public TransformRes<VarAccess> createVarReadAccess(String varId, String varName, String blockId) {
		VarAccess node = new VarAccess();
		node.setOpcode("data_variable");
		node.setBlockId(blockId);
		node.initDefault(0);
		node.setAttrs(Maps.newHashMap(ImmutableMap.of("type", "data_variable", "id", node.getBlockId())));
		node.setID(varId);
		node.setName(varName);

		Create blockCreate = new Create((ScratchBlock) node);

		return new TransformRes<>(blockCreate, node);
	}

	public TransformRes<OperatorExpr> createExpr(OperatorExpr expr) {
		return new TransformRes<>(new Create(expr), expr);
	}

	public VarAccess createVarWriteAccess(VarDecl varDecl) {
		VarAccess varAcc = new VarAccess();
		varAcc.setID(varDecl.getID());
		varAcc.setName(varDecl.getName());
		return varAcc;
	}

	public VarAccess createParamAccess(String name, String id, String type) {
		VarAccess node = new VarAccess();
		node.setOpcode(type);
		node.setType(type);
		node.setBlockId(VAR_ID_PREFIX + ParsingUtil.getRandomId());
		node.initDefault(0);
		node.setAttrs(Maps.newHashMap(ImmutableMap.of("type", type, "id", node.getBlockId())));
		node.setID(id);
		node.setName(name);
		return node;
	}

	/**
	 * Scratch's representaiton for parameter access does not include the parameter
	 * id. Scratch only uses the parameter name of the parameter access block to map
	 * back to the declared parameter. For example,
	 * <block type='argument_reporter_string_number' id='uniqueId'><field name=
	 * 'VALUE'>param_name</field></block>
	 * 
	 * We store the parameter id in the parameter access node to distinguish between
	 * parameter access blocks in different procedure definition (although it's not
	 * used in the Xml generation)
	 * 
	 * @param paramSpec
	 * @return
	 */
	public VarAccess createParamAccess(ParamSpec paramSpec) {
		return createParamAccess(paramSpec.name, paramSpec.id, paramSpec.type);
	}

	public VarAccess createParamAccess(ParamDecl paramDecl) {
		return createParamAccess(paramDecl.getName(), paramDecl.getID(), paramDecl.getVarType());
	}

	public static class ProcDeclBuilder {
		private String procCode;
		private String id;
		private ProcDeclMeta procDeclMeta;
		private List<ParamDecl> paramDeclList = Lists.newArrayList();
		private BlockSeq body;
		private Map<String, String> attrs = Maps.newHashMap();

		/**
		 * 
		 * @param procCode
		 *            procedure name without parameter symbols (e.g., %s)
		 * @param id
		 *            procedure id
		 */
		public ProcDeclBuilder(String procCode, String id) {
			this.procCode = procCode;
			this.id = id;
			attrs.put("type", "procedures_definition"); // TODO: LATER AUTO CONFIG
			attrs.put("id", id);
		}

		public ProcDeclBuilder addParamDecl(Map<String, String> name2Id) {
			name2Id.entrySet().stream().forEach(entry -> {
				ParamDecl paramDecl = new ParamDecl(entry.getValue());
				paramDecl.setName(entry.getKey());
				paramDecl.setMeta(new ParamDeclMeta.Builder(entry.getValue()).shadowId(ParsingUtil.getRandomId())
						.shadowType("argument_reporter_string_number").fieldName("VALUE").fieldValue("5").build());
				paramDeclList.add(paramDecl);
			});

			return this;
		}

		public ProcDeclBuilder setParamDeclListFromSpecs(List<ParamSpec> paramSpecList) {
			paramSpecList.stream().forEach(entry -> {
				ParamDecl paramDecl = new ParamDecl(entry.id);
				paramDecl.setName(entry.name);
				paramDecl.setMeta(new ParamDeclMeta.Builder(entry.id).shadowId(ParsingUtil.getRandomId())
						.shadowType(entry.type).fieldName("VALUE").fieldValue(entry.name).build());
				paramDeclList.add(paramDecl);
			});

			updateProcCode(paramSpecList);

			return this;
		}

		private void updateProcCode(List<ParamSpec> paramSpecList) {
			for (ParamSpec paramSpec : paramSpecList) {
				procCode += paramSpec.isBoolean() ? " %b" : " %s";
			}
		}

		public ProcDeclBuilder addBody(BlockSeq seq) {
			this.body = seq;
			return this;
		}

		public ProcDecl build() {
			ProcDecl procDecl = new ProcDecl();
			procDecl.setAttrs(attrs);

			procDeclMeta = new ProcDeclMeta.Builder().procCode(procCode)
					.shadowId(RandomStringUtils.randomAlphanumeric(5))
					.argIds(paramDeclList.stream().map(p -> p.getID()).collect(Collectors.toList()))
					.argNames(paramDeclList.stream().map(p -> p.getName()).collect(Collectors.toList()))
					.argDefaults(paramDeclList.stream().map(p -> "").collect(Collectors.toList())).build();
			procDecl.setMeta(procDeclMeta);

			paramDeclList.stream().forEach(param -> procDecl.addParamDecl(param));
			procDecl.setBody(body);
			return procDecl;
		}
	}

	public static class ProcAccessBuilder {
		private ProcDecl procDecl;
		private String id;
		private Map<String, ArgBuildSpec> id2ArgSpec;

		/*
		 * TODO: add CreatedEntityCounter helper to deal with context sensitive
		 * parameters for procedure call
		 */
		public ProcAccessBuilder(ProcDecl procDecl) {
			this(procDecl, (procDecl.getId() + "_Call"
					+ CreatedEntityCounter.getCount(ProcAccess.class.getSimpleName() + procDecl.getId() + "_Call"))
							.replaceAll("\\s", ""));
		}

		public ProcAccessBuilder(ProcDecl procDecl, String id) {
			this.procDecl = procDecl;
			this.id = id;
		}

		public ProcAccessBuilder arguments(Map<String, ArgBuildSpec> id2ArgSpec) {
			this.id2ArgSpec = id2ArgSpec;
			return this;
		}

		public TransformRes<ProcAccess> build() {
			ProcAccess procAcc = new ProcAccess();
			procAcc.setOpcode("procedures_call");// AUTO SET
			procAcc.setMeta(new ProcAccMeta());
			procAcc.setBlockId(id);
			procAcc.getMeta().setProccode(procDecl.getMeta().getProcCode());

			procAcc.initDefault(procDecl.getMeta().getArgIds().size());

			for (int i = 0; i < procDecl.getMeta().getArgIds().size(); i++) {
				String argId = procDecl.getMeta().getArgIds().get(i);
				procAcc.getMeta().getArgumentIds().add(argId);

				Default defaultVal = new Default(id2ArgSpec.get(argId).defaultVal);
				defaultVal.setShadowAttr(Maps.newHashMap(ImmutableMap.of("type", "text", "id", id + "_param_" + i)));
				defaultVal.setFieldAttr(Maps.newHashMap(ImmutableMap.of("name", "TEXT")));

				if (id2ArgSpec.get(argId).expr != null) {
					procAcc.addArgInput(id2ArgSpec.get(argId).expr);
				} else {
					procAcc.addArgInput(defaultVal.toExpr());
				}
				procAcc.setDefault(defaultVal, i);
			}
			Create blockCreate = new Create((ScratchBlock) procAcc);

			return new TransformRes<>(blockCreate, procAcc);
		}
	}

	public ExprStmt createStmt(String opcode, String id, List<ArgBuildSpec> argSpecs) {
		ExprStmt stmt = new ExprStmt();
		createSimpleBlockTypeHelper(opcode, id, argSpecs, stmt);

		return stmt;
	}

	public OperatorExpr createExpr(String opcode, String id, List<ArgBuildSpec> argSpecs) {
		OperatorExpr expr = new OperatorExpr();
		createSimpleBlockTypeHelper(opcode, id, argSpecs, expr);
		return expr;
	}

	/*
	 * <block type='sensing_of' id='_.SAMNV!VB_R^Ji$b(L1' x='185' y='389'><field
	 * name='PROPERTY'>costume name</field><value name='OBJECT'><shadow
	 * type='sensing_of_object_menu' id='da13YtS*U{/4=.z`SHf9'><field
	 * name='OBJECT'>Sprite1</field></shadow></value></block>
	 */

	public TransformRes<Expr> createAttrReadOpExpr(String attrName, String targetName) {
		String xml = String.format(
				"<block type='sensing_of' id='%s' x='0' y='0'>"
						+ "<field name='PROPERTY'>%s</field>"
						+ "<value name='OBJECT'><shadow type='sensing_of_object_menu' id='%s'>"
						+ "<field name='OBJECT'>%s</field></shadow></value></block>",
				CreatedEntityCounter.getCountedName(targetName + "." + attrName), attrName, ParsingUtil.getRandomId(),
				targetName);
		log.debug(xml);
		OperatorExpr expr = (OperatorExpr) new Sb3XmlParser().parseBlock(xml);
		Create blockCreate = new Create((ScratchBlock) expr);

		return new TransformRes<>(blockCreate, expr);
	}

	private void createSimpleBlockTypeHelper(String opcode, String id, List<ArgBuildSpec> argSpecs, SimpleBlock stmt) {
		stmt.setOpcode(opcode);
		stmt.setBlockId(id == null ? ParsingUtil.getRandomId() : id);
		stmt.setAttrs(Maps.newHashMap(ImmutableMap.of("id", stmt.getBlockId(), "type", opcode)));
		stmt.initDefault(argSpecs.size());

		for (int i = 0; i < argSpecs.size(); i++) {
			Default defaultVal = new Default(argSpecs.get(i).defaultVal);
			defaultVal.setShadowAttr(Maps.newHashMap(ImmutableMap.of("type", "text")));
			defaultVal.setFieldAttr(Maps.newHashMap(ImmutableMap.of("name", "TEXT")));

			if (argSpecs.get(i).expr != null) {
				stmt.addArgInput(argSpecs.get(i).expr);
			} else {
				stmt.addArgInput(defaultVal.toExpr());
			}
			stmt.setDefault(defaultVal, i);
		}
	}

	public IfElseStmt createIfStmt(Expr condExpr, BlockSeq then) {
		IfElseStmt stmt = new IfElseStmt();
		stmt.setOpcode("control_if");
		stmt.setBlockId(ParsingUtil.getRandomId());
		stmt.setAttrs(Maps.newHashMap(ImmutableMap.of("id", stmt.getBlockId(), "type", stmt.getOpcode())));
		stmt.initDefault(2);
		stmt.setCond(condExpr);
		stmt.setThen(then);
		return stmt;
	}

	public TransformRes<ProcDecl> createInvariantCheckDef(VarDecl variableDecl, String watchedId) {
		String procCode = "assertEquals";

		// param(var) EQUAL param(expr)
		List<ParamSpec> paramSpecs = Lists.newArrayList(new ParamSpec("var", "varId", false),
				new ParamSpec("expr", "exprRootId", false));
		VarAccess paramAcc0 = createParamAccess(paramSpecs.get(0));
		VarAccess paramAcc1 = createParamAccess(paramSpecs.get(1));
		List<ArgBuildSpec> argSpecs = Lists.newArrayList(new ArgBuildSpec(paramAcc0, ""),
				new ArgBuildSpec(paramAcc1, ""));
		OperatorExpr boolExpr = createExpr("operator_equals", null, argSpecs);

		// NOT (param(var) EQUAL param(expr))
		List<ArgBuildSpec> opNotArgSpecs = Lists.newArrayList(new ArgBuildSpec(boolExpr, ""));
		OperatorExpr operatorNotExpr = createExpr("operator_not", null, opNotArgSpecs);

		// THEN

		TransformRes incrStmtCreateRes = createIncrementStmt(variableDecl, watchedId);
		BlockSeq then = new BlockSeq();
		then.addStmt((Stmt) incrStmtCreateRes.getNode());

		// IF (COND) THEN
		IfElseStmt ifstmt = createIfStmt(operatorNotExpr, then);
		BlockSeq body = new BlockSeq();
		body.addStmt(ifstmt);
		// PROCDEF { IF (COND) THEN }
		ProcDecl procDecl = new ASTBuilder.ProcDeclBuilder(procCode, "assertEqualID")
				.setParamDeclListFromSpecs(paramSpecs).addBody(body).build();
		// return procDecl;

		// procDecl.updateId();
		// set watch id for increment statement
		// ((ScratchBlock) incrStmtCreateRes.getNode()).setBlockId(watchedId);

		ProcDefEl procDefEl = new ProcDeclConvert(procDecl).toProcDefEl();
		String xml = "<xml>" + procDefEl.toXml().mkString() + "</xml>";
		return new TransformRes<>(new Create(xml), procDecl);
	}

	public ExprStmt createFromTemplate(String opcode, Object... args) throws TransformException {
		String template;
		String xml;
		switch (opcode) {
		case "looks_switchcostumeto": {
			template = "<block type='looks_switchcostumeto' id='_block_id_' x='0' y='0'><value name='COSTUME'><shadow type='looks_costume' id='_shadow_id_'><field name='COSTUME'>%s</field></shadow></value></block>";
			xml = String.format(template, args);
			break;
		}
		case "control_create_clone_of": {
			template = "<block type='control_create_clone_of' id='_block_id_' x='0' y='0'><value name='CLONE_OPTION'><shadow type='control_create_clone_of_menu' id='_shadow_id_'><field name='CLONE_OPTION'>%s</field></shadow></value></block>";
			xml = String.format(template, args);
			break;
		}
		case "motion_gotoxy": {
			template = "<block type='motion_gotoxy' id='_block_id_' x='0' y='0'><value name='X'><shadow type='math_number' id='_shadow_id_0_'><field name='NUM'>%s</field></shadow></value><value name='Y'><shadow type='math_number' id='_shadow_id_1_'><field name='NUM'>%s</field></shadow></value></block>";
			xml = String.format(template, args);
			break;
		}
		case "looks_setsizeto": {
			template = "<block type='looks_setsizeto' id='_block_id_' x='0' y='0'><value name='SIZE'><shadow type='math_number' id='_shadow_id_'><field name='NUM'>%s</field></shadow></value></block>";
			xml = String.format(template, args);
			break;
		}
		default:
			throw new TransformException("Unknown opcode:" + opcode);
		}
		ExprStmt stmt = (ExprStmt) new Sb3XmlParser().parseBlock(xml);
		return stmt;
	}

	// OperatorExpr expr = (OperatorExpr) new Sb3XmlParser().parseBlock(xml);
	// Create blockCreate = new Create((ScratchBlock) expr);
	//
	// return new TransformRes<>(blockCreate, expr);

}