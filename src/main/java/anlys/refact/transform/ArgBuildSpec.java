package anlys.refact.transform;

import ast.Expr;
import ast.Literal;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public
class ArgBuildSpec {
	Expr expr;
	String defaultVal;
	
	public static ArgBuildSpec of(Expr expr) {
		String defaultVal;
		if(expr instanceof Literal) {
			defaultVal = ((Literal) expr).getValue();
		}else {
			defaultVal = "0";
		}
		return new ArgBuildSpec(expr, defaultVal);
	}
}