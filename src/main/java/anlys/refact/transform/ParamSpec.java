package anlys.refact.transform;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class ParamSpec {
	String name;
	String id;
	String type;
	boolean isBoolean;

	public ParamSpec(String name, String id, boolean isBoolean) {
		this.name = name;
		this.id = id;
		this.isBoolean = isBoolean;
		this.type = isBoolean ? "argument_reporter_boolean" : "argument_reporter_string_number";
	}
	
//	public static ParamSpec of(String name, String id, boolean isBoolean) {
//		String type = isBoolean ? "argument_reporter_boolean" : "argument_reporter_string_number";
//		return new ParamSpec(name, id, type, isBoolean);
//	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ParamSpec other = (ParamSpec) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (type == null) {
			if (other.type != null)
				return false;
		} else if (!type.equals(other.type))
			return false;
		return true;
	}

}