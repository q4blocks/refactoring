package anlys.refact.transform;

import ast.ASTNode;
import ast.BlockSeq;
import ast.ProcDecl;
import ast.Program;
import ast.Script;
import ast.Sprite;
import ast.Stage;
import ast.Stmt;

public class Snippet {
	private final Program program = new Program();
	private Script script = new Script();
	private ProcDecl astNode;

	public Snippet() {
		this(new BlockSeq());
	}

	public Snippet(BlockSeq seq) {
		program.setStage(new Stage());
		program.getStage().addSprite(new Sprite());
		script.setBody(seq);
		program.getStage().getSprite(0).addScript(script);
	}

	public Snippet(ProcDecl proc) {
		this.astNode = proc;
		program.setStage(new Stage());
		program.getStage().addProcDecl(proc);
	}

	public Program get() {
		return program;
	}
	
	public ASTNode getNode() {
		return astNode;
	}
	
	public Sprite target() {
		return program.getStage().getSprite(0);
	}

	public void addStmt(Stmt stmt) {
		this.script.getBody().addStmt(stmt);
	}

	public BlockSeq getBody() {
		return this.script.getBody();
	}

}
