package anlys.refact.transform;

import anlys.refact.RefactoringException;

public class TransformException extends RefactoringException{
	public TransformException(String msg) {
		super(msg);
	}
}
