package anlys.refact.transform;

import anlys.refact.transform.action.Action;
import anlys.refact.transform.action.ActionList;
import anlys.refact.transform.action.TransformRes;
import ast.ASTNode;
import ast.Program;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
public abstract class Transformer {

	protected ActionList actions = new ActionList();

	public abstract void computeActions() throws TransformException ;

	public ActionList getActions() {
		return actions;
	};
	
	public void clearActions() {
		actions = new ActionList();
	}

	public void recordAction(Action action) {
		actions.addAction(action);
	}
	
	public <T extends ASTNode> void recordAction(TransformRes<T> res) {
		actions.addAction(res.getAction());
	}
}
