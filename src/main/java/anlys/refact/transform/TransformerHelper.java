package anlys.refact.transform;

import java.util.Optional;
import java.util.stream.Collectors;

import anlys.refact.transform.action.Action;
import anlys.refact.transform.action.CostumeCopy;
import anlys.refact.transform.action.Insert;
import anlys.refact.transform.action.Replace;
import anlys.refact.transform.action.ReplaceSeq;
import anlys.refact.transform.action.SpriteCreate;
import anlys.refact.transform.action.SpriteDelete;
import anlys.refact.transform.action.TransformRes;
import anlys.refact.transform.action.VarDeclare;
import anlys.refact.transform.action.VarDelete;
import anlys.refact.transform.action.VarRename;
import ast.ASTNode;
import ast.Costume;
import ast.Expr;
import ast.List;
import ast.Program;
import ast.Scriptable;
import ast.Sprite;
import ast.Stmt;
import ast.VarDecl;

public class TransformerHelper {
	private Program program;
	boolean shouldApply;

	public TransformerHelper() {
		this.shouldApply = true;
	}

	public TransformerHelper(Program program, boolean shouldApply) {
		this.program = program;
		this.shouldApply = shouldApply;
	}

	public TransformerHelper(Program program) {
		this.program = program;
		this.shouldApply = true;
	}

	/**
	 * 
	 * @param toInsert
	 * @param targetBlock
	 * @return
	 */
	public Insert insertBefore(Stmt toInsert, Stmt targetBlock) {
		assert (targetBlock != null);
		assert (targetBlock.parentSeq() != null);
		int pos = targetBlock.parentSeq().getIndexOfChild(targetBlock);
		if (shouldApply) {
			targetBlock.parentSeq().getStmtList().insertChild(toInsert, pos);
		}
		Insert insertAction = new Insert(toInsert, targetBlock);
		return insertAction;
	}

	public Insert insertAfter(Stmt toInsert, Stmt targetBlock) {
		assert (targetBlock != null);
		assert (targetBlock.parentSeq() != null);
		int pos = targetBlock.parentSeq().getIndexOfChild(targetBlock);
		if (shouldApply) {
			targetBlock.parentSeq().getStmtList().insertChild(toInsert, pos + 1);
		}
		Insert insertAction = new Insert(toInsert, targetBlock, false);
		return insertAction;

	}

	public TransformRes<VarDecl> declareVar(Scriptable scope, String varName, String varId) {
		VarDecl newVarDecl = declareVarHelper(scope, varName, varId);
		VarDeclare varDeclare = new VarDeclare(varName, varId, scope == null ? "_stage_" : scope.getName());

		return new TransformRes<>(varDeclare, newVarDecl);
	}

	public TransformRes<?> deleteVar(VarDecl variable) {
		VarDelete varDelete = new VarDelete(variable.getID(), variable.getName(), variable.sprite().getName());
		variable.getParent().removeChild(variable.getParent().getIndexOfChild(variable));
		return new TransformRes<>(varDelete, null);
	}

	public TransformRes<VarDecl> renameVar(VarDecl variable, String newName, Scriptable target) {
		String oldName = variable.getName();
		variable.setName(newName);
		VarRename varRename = new VarRename(oldName, newName, variable.getID(), target.getName());
		return new TransformRes<>(varRename, variable);
	}

	private VarDecl declareVarHelper(Scriptable scope, String varName, String varId) {
		VarDecl newVarDecl = new VarDecl(varId);
		newVarDecl.setName(varName);
		if (scope != null) {
			scope.addVarDecl(newVarDecl);
		}
		return newVarDecl;
	}

	public Action replaceExpr(Expr existing, Expr newExpr) throws TransformException {
		// save targetId before being replace and lost the link to its parent
		// (onlyparent knows its literal or shadow id)
		String targetId = existing.getId();
		existing.getParent().setChild(newExpr, existing.getParent().getIndexOfChild(existing));

		if (targetId == null) {
			throw new TransformException("Target replacement expression has no id");
		}

		return new Replace(targetId, newExpr.getId());
	}

	public Action replaceExpr(String existingExprId, Expr newExpr) throws TransformException {
		Optional<Expr> expr = program.lookupExpr(existingExprId);
		if (expr.isPresent()) {
			return replaceExpr(expr.get(), newExpr);
		} else {
			// actually a dropdown (no blockId)
			// type.equals("broadcast_msg") meaning no need to replace and just demote it to
			// default
			// setExpr(
			// should be DEXS%^XHnDI!~7]/C_h5
			throw new TransformException("Expr to replace ID: " + existingExprId + " not found");
		}
	}

	public Action replaceStmt(java.util.List<String> targetStmtIds, Stmt stmt) throws TransformException {
		java.util.List<Stmt> targetStmts = targetStmtIds.stream().map(id -> program.lookupStmt(id))
				.collect(Collectors.toList());
		replaceStmtHelper(targetStmts, stmt);
		// check if it's gone
		program.flushAttrAndCollectionCache();
		java.util.List<Stmt> targetStmtsAfter = targetStmtIds.stream().map(id -> program.lookupStmt(id))
				.collect(Collectors.toList());

		// assert(targetStmtsAfter.size()==0);

		return new ReplaceSeq(targetStmtIds, stmt.getId());
	}

	public void replaceStmtHelper(java.util.List<Stmt> targetStmts, Stmt stmt) throws TransformException {
		if (targetStmts == null || targetStmts.isEmpty()) {
			return;
		}

		Stmt startBlock = targetStmts.get(0);
		ASTNode parentList = startBlock.getParent();

		int start = parentList.getIndexOfChild(startBlock);
		int end = parentList.getIndexOfChild(targetStmts.get(targetStmts.size() - 1));

		// check if valid connected blocks
		int offset = start;
		for (int i = 0; i < targetStmts.size(); i++) {
			Stmt next = (Stmt) parentList.getChild(i + offset);
			boolean isValid = next.getId().equals(targetStmts.get(i).getId());
			if (!isValid) {
				throw new TransformException("Attempt to replace non-connected statements");
			}
		}

		// remove existing stmt in the targetStmts and replace last one with the stmt
		for (int i = start; i < end; i++) {
			// remove stmt at the start position for end - start times
			// elements moved to the left as the element before them is removed
			// save the last stmt to remove as a placeholder to be replace next
			parentList.removeChild(start);
		}

		// Stmt lastStmtInTarget = targetStmts.get(targetStmts.size() - 1);
		// replace last target statement (now moved to the start position in the
		// parentList) with stmt
		parentList.setChild(stmt, start);
	}

	public void replaceWith(Stmt target, Stmt stmt) {
		String targetID = target.getId();
		List parentList = (List) target.getParent();
		// replace

	}

	public Stmt getMoveUpBeforeTarget(Stmt stmt) {
		Stmt temp = stmt;
		ASTNode parent = stmt.getParent();
		Stmt targetStmt = null;
		if (parent instanceof ast.List && parent.getIndexOfChild(stmt) == 0) {
			// first in the seq; ready to move up another level
			targetStmt = parent.parentSeq().parentStmt();
		} else {
			// has some nodes to move through in the same level
			// maybe more efficient/ by swapping
			targetStmt = (Stmt) parent.getChild(parent.getIndexOfChild(stmt) - 1);
		}

		return targetStmt;
	}

	public void moveUp(Stmt stmt) {
		Stmt temp = stmt;
		Stmt targetStmt = getMoveUpBeforeTarget(temp);
		ASTNode parent = stmt.getParent();
		parent.removeChild(parent.getIndexOfChild(stmt));
		if (targetStmt != null) {
			insertBefore(temp, targetStmt);
		}
	}
	
	/*
	 * sprite
	 */

	public TransformRes<Sprite> createNewSprite(String spriteParentName, Program program) {
		Sprite parentSprite = new Sprite();
		parentSprite.setName(spriteParentName);
		program.getStage().addSprite(parentSprite);
		return new TransformRes<>(new SpriteCreate(spriteParentName), parentSprite);
	}
	
	public TransformRes<Costume> copyCostume(Sprite copyDestination,Sprite copySource, String costumeName) {
		Costume costume = new Costume(costumeName);
		copyDestination.addCostume(costume);
		CostumeCopy action = new CostumeCopy(costumeName, copyDestination.getName(), copySource.getName());
		return new TransformRes<>(action, costume);
	}

	public TransformRes<?> removeSprite(Sprite sprite) {
		SpriteDelete spriteDelete = new SpriteDelete(sprite.getName());
		sprite.getParent().removeChild(sprite.getParent().getIndexOfChild(sprite));
		return new TransformRes<>(spriteDelete, null);
	}
}
