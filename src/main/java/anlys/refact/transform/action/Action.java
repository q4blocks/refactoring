package anlys.refact.transform.action;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonSubTypes.Type;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeInfo.As;

import lombok.Getter;
import lombok.Setter;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = As.PROPERTY, property = "type")
@JsonSubTypes({ @Type(value = Insert.class, name = "InsertBlockAction"),
		@Type(value = Replace.class, name = "ReplaceAction"),
		@Type(value = ReplaceSeq.class, name = "ReplaceSeqAction"),
		@Type(value = VarDeclare.class, name = "VarDeclareAction"),
		@Type(value = VarRename.class, name = "VarRenameAction"),
		@Type(value = VarDelete.class, name = "VarDeleteAction"),
		@Type(value = Create.class, name = "BlockCreateAction"),
		@Type(value = SpriteCreate.class, name = "SpriteCreate"),
		@Type(value = CostumeCopy.class, name = "CostumeCopy"),
		@Type(value = SpriteDelete.class, name = "SpriteDelete")
})
@Getter
@Setter
public abstract class Action {
	public <T extends Action> T record(ActionList actions) {
		actions.addAction(this);
		return (T) this;
	}

}
