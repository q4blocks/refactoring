package anlys.refact.transform.action;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import lombok.EqualsAndHashCode;

@EqualsAndHashCode
@JsonRootName(value = "actions")
public class ActionList {
	@JsonProperty("actions")
	@JacksonXmlProperty(localName = "action")
	@JacksonXmlElementWrapper(useWrapping = false) // prevent actions wrapper tag
	List<Action> actionSequence = new ArrayList<Action>();

	public ActionList addAction(Action action) {
		actionSequence.add(action);
		return this;
	}

	public List<Action> get() {
		return actionSequence;
	}

	public static ActionList empty() {
		return new ActionList();
	}

}