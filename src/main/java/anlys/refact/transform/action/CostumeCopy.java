package anlys.refact.transform.action;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

@ToString
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
@Getter
public class CostumeCopy extends Action {
	String name;
	String copyDestination;
	String copySource;

	public CostumeCopy(String costumeName, String copyDestination, String copySource) {
		this.name = costumeName;
		this.copyDestination = copyDestination;
		this.copySource = copySource;
	}
}