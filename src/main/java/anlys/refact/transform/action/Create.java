package anlys.refact.transform.action;

import com.fasterxml.jackson.annotation.JsonProperty;

import ast.ScratchBlock;
import ast.Script;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import util.UtilFunc;

@ToString
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
@Getter
public class Create extends Action {
	@JsonProperty("block_xml")
	String blockXml;
	String blockId; // first block
	String info;
	String targetName;

	public Create(String xml) {
		blockXml = UtilFunc.replaceDoubleWithSingleQuote(xml);
	}
	/**
	 * 
	 * @param xml must be enclosed with <xml></xml>
	 * @param targetName
	 */
	public Create(String xml, String targetName) {
		this(xml);
		this.targetName = targetName;
	}

	public Create(ScratchBlock block) {
		blockXml = "<xml>" + UtilFunc.xmlFromScratchBlock(block) + "</xml>";
		blockId = block.getBlockId();
		info = block.getOpcode();
	}

	public Create(ScratchBlock block, String targetName) {
		this(block);
		this.targetName = targetName;
	}

	public static Create of(Script cloneGenScript, String targetName) {
		sb3.parser.ScriptEl scriptEl = new sb3.parser.ScriptConvert(cloneGenScript).toScriptEl();
		String xml = "<xml>" +scriptEl.toXml().mkString() + "</xml>";
		return new Create(xml, targetName);
	}
}