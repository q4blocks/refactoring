package anlys.refact.transform.action;

import com.fasterxml.jackson.annotation.JsonProperty;

import ast.ScratchBlock;
import ast.Stmt;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@ToString
@EqualsAndHashCode(callSuper=false)
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Insert extends Action {
	@JsonProperty("inserted_block")
	String insertedBlock;

	@JsonProperty("target_block")
	String targetBlock;

	@JsonProperty("before_target")
	boolean beforeTarget = true;

	public Insert(Stmt insertedBlock, Stmt targetBlock) {
		this.insertedBlock = ((ScratchBlock) insertedBlock).getBlockId();
		this.targetBlock = ((ScratchBlock) targetBlock).getBlockId();
	}

	public Insert(Stmt insertedBlock, Stmt targetBlock, boolean beforeTarget) {
		this.insertedBlock = ((ScratchBlock) insertedBlock).getBlockId();
		this.targetBlock = ((ScratchBlock) targetBlock).getBlockId();
		this.beforeTarget = beforeTarget;
	}

}