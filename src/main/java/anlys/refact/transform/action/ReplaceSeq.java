package anlys.refact.transform.action;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@ToString
@EqualsAndHashCode(callSuper=false)
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class ReplaceSeq extends Action {
	@JsonProperty("target_blocks")
	List<String> targetBlocks;
	@JsonProperty("replace_with")
	String replaceWith;	
}
