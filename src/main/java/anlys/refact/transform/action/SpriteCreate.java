package anlys.refact.transform.action;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

@ToString
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
@Getter
public class SpriteCreate extends Action {
	String spriteName;

	public SpriteCreate(String name) {
		this.spriteName = name;
	}
}
