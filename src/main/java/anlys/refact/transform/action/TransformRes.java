package anlys.refact.transform.action;

import ast.ASTNode;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * TransformationResult for different AST action
 * 
 * @author karn
 *
 * @param <T>
 *            when result contains newly created AST node
 */

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class TransformRes<T extends ASTNode> {
	Action action;
	T node;

	public TransformRes<T> record(ActionList actions) {
		actions.addAction(action);
		return this;
	}

	public T get() {
		return node;
	}
}
