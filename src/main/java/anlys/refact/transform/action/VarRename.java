package anlys.refact.transform.action;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@ToString
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class VarRename extends Action {
	@JsonProperty("oldName")
	String oldName;
	@JsonProperty("newName")
	String newName;
	@JsonProperty("var_id")
	String varId;
	@JsonProperty("target")
	String target;
}
