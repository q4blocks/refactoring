package anlys.refact.transform.strategy;

import anlys.refact.transform.TransformException;
import anlys.refact.transform.action.Action;

public interface ActionTransform {
	public Action apply() throws TransformException;
}
