package anlys.refact.transform.strategy;

import anlys.refact.transform.TransformException;

public interface ActionlessTransform {
	public void apply() throws TransformException;
}
