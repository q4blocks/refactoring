package anlys.refact.transform.strategy;

import anlys.refact.transform.TransformException;
import anlys.refact.transform.action.Action;
import ast.ASTNode;

/**
 * ExprStmt stmt = ...;
 * ExprStmt copy = new CopyStrategy<>(stmt).makeCopy();
 * 
 * @author karn
 *
 * @param <T>
 */
public class CopyStrategy<T extends ASTNode> extends TransformStrategy {
	private T node;
	private T copy;

	public CopyStrategy(T node) {
		super(null);
		setNode(node);
		setTransform(() -> {
			copy = (T) node.treeCopy().initCopy().updateId();
			return null;// CREATE action
		});
	}

	@SuppressWarnings("unchecked")
	public void setNode(T node) {
		this.node = node;
	}

	/**
	 * run without cache flushing because the tree is not modified
	 * 
	 * @return
	 * @throws TransformException
	 */
	@Override
	public Action applyTransform() throws TransformException {
		return this.transform.apply();
	}

	public T makeCopy() throws TransformException {
		applyTransform();
		return copy;
	}

}
