package anlys.refact.transform.strategy;

import anlys.refact.transform.TransformException;
import anlys.refact.transform.action.Action;
import anlys.refact.transform.action.Replace;
import ast.Expr;
import ast.Program;

public class ReplaceExpr<T extends Expr> extends TransformStrategy {
	T target;
	T replacement;

	public ReplaceExpr(Program program) {
		super(program);
		setTransform(() -> {
			Expr existing = target;
			String targetId = existing.getId();
			assert (!targetId.isEmpty());
			Expr newExpr = this.replacement;
			existing.getParent().setChild(newExpr, existing.getParent().getIndexOfChild(existing));
			Action action = new Replace(targetId, newExpr.getId());
			return action;
		});
	}

	public ReplaceExpr<T> replace(T target) {
		this.target = target;
		return this;
	}

	public ReplaceExpr<T> with(T replacement) {
		this.replacement = replacement;
		return this;
	}

}
