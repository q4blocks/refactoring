package anlys.refact.transform.strategy;

import java.util.ArrayList;
import java.util.List;

import anlys.refact.transform.TransformException;
import anlys.refact.transform.action.Action;
import anlys.refact.transform.action.Replace;
import ast.ASTNode;
import ast.Expr;
import ast.Program;
import ast.Stmt;

public class ReplaceStmts<T extends Stmt> extends TransformStrategy {
	List<T> targetStmts = new ArrayList<>();
	T replacement;

	public ReplaceStmts(Program program) {
		super(program);

		setTransform(() -> {
			if (targetStmts == null || targetStmts.isEmpty()) {
				return null;
			}

			Stmt startBlock = targetStmts.get(0);
			ASTNode parentList = startBlock.getParent();

			int start = parentList.getIndexOfChild(startBlock);
			int end = parentList.getIndexOfChild(targetStmts.get(targetStmts.size() - 1));

			// check if valid connected blocks
			int offset = start;
			for (int i = 0; i < targetStmts.size(); i++) {
				Stmt next = (Stmt) parentList.getChild(i + offset);
				boolean isValid = next.getId().equals(targetStmts.get(i).getId());
				if (!isValid) {
					// throw new TransformException("Attempt to replace non-connected statements");
				}
			}

			// remove existing stmt in the targetStmts and replace last one with the stmt
			for (int i = start; i < end; i++) {
				// remove stmt at the start position for end - start times
				// elements moved to the left as the element before them is removed
				// save the last stmt to remove as a placeholder to be replace next
				parentList.removeChild(start);
			}

			parentList.setChild(replacement, start);

			return null;// TODO ACTION
		});

	}

	public ReplaceStmts<T> replace(List<T> targetStmts) {
		this.targetStmts = targetStmts;
		return this;
	}

	public ReplaceStmts<T> with(T replacementStmt) {
		this.replacement = replacementStmt;
		return this;
	}

}
