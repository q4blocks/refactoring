package anlys.refact.transform.strategy;

import anlys.refact.transform.TransformException;
import anlys.refact.transform.action.Action;
import ast.Program;

public class TransformStrategy {
	protected Program program;
	protected ActionTransform transform;
	protected ActionlessTransform actionlessTransform;

	public TransformStrategy(Program program) {
		this.program = program;
	}

	public void setTransform(ActionTransform transform) {
		this.transform = transform;
	}

	public void setTransform(ActionlessTransform transform) {
		this.actionlessTransform = transform;
	}

	public Action apply(ActionTransform transform) throws TransformException {
		setTransform(transform);
		return applyTransform();
	}

	/**
	 * transform & perform tree update without action as return value
	 * 
	 * @param transform
	 * @throws TransformException
	 */
	public void apply(ActionlessTransform transform) throws TransformException {
		setTransform(transform);
		applyActionlessTransform();
	}

	public Action applyTransform() throws TransformException {
		Action action = this.transform.apply();
		program.flushTreeCache();
		return action;
	}

	public void applyActionlessTransform() throws TransformException {
		this.actionlessTransform.apply();
		program.flushTreeCache();
	}

}
