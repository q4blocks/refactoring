package anlys.smell;

import java.io.IOException;
import java.lang.reflect.Type;
import java.nio.charset.StandardCharsets;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Sets;
import com.google.common.collect.Sets.SetView;
import com.google.common.hash.HashFunction;
import com.google.common.hash.Hasher;
import com.google.common.hash.Hashing;
import com.google.gson.JsonElement;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import anlys.refact.RescopeVarReq;
import ast.Program;
import ast.Sprite;
import ast.VarAccess;
import ast.VarDecl;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class BroadScopeVar extends CodeSmell implements JsonSerializer<DuplicateConstant> {
	String varId;

	@Getter
	@Setter
	@JsonIgnore
	VarDecl var;

	@Getter
	@Setter
	@JsonIgnore
	Sprite defScope;

	String defScopeName;

	public BroadScopeVar(VarDecl v, Sprite defScope) {
		this.var = v;
		this.varId = v.getID();
		this.defScope = defScope;
		this.defScopeName = defScope.getName();
		Set<String> usedScope = v.allUsesInProgram().stream().map(u -> (VarAccess) u)
				.map(vacc -> vacc.sprite().getName())
				.collect(Collectors.toSet());

		// metadata
		metadata.put("_id", computeSmellId());
		metadata.put("var_name", v.getName());
		metadata.put("def_target", defScope.getName());
		metadata.put("num_uses", v.allUsesInProgram().size());

		metadata.put("only_use_in_defined_scope",
				usedScope.size() == 1 && usedScope.iterator().next().equals(defScopeName));

		if (!usedScope.isEmpty()) {
			SetView<String> externalUseScope = Sets.difference(usedScope, Sets.newHashSet(defScopeName));
			log.debug("def in :{} , used in : {}", defScope.getName(), usedScope);
			log.debug("external uses: {}",externalUseScope);
			metadata.put("num_uses_outside_defined_scope", externalUseScope.size());
		}


	}

	@Override
	public String computeSmellId() {
		HashFunction hf = Hashing.farmHashFingerprint64();
		Hasher hasher = hf.newHasher();
		hasher.putString(var.getID(), StandardCharsets.UTF_8);
		return hasher.hash().toString();
	}

	@SuppressWarnings("unchecked")
	@Override
	public BroadScopeVar fromJson(String jsonStr, Program program) throws SmellJson2ObjException {
		ObjectMapper mapper = new ObjectMapper();
		BroadScopeVar instance;
		try {
			instance = mapper.readValue(jsonStr, BroadScopeVar.class);
			instance.setVar(program.lookupVarDecl(varId));
			instance.setDefScope((Sprite) program.getTargetByName(defScopeName)
					.orElseThrow(() -> new SmellJson2ObjException("Sprite not found")));
			return instance;
		} catch (IOException e) {
			throw new SmellJson2ObjException("Error BroadScopeVar smell deserialization", e);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public RescopeVarReq buildRefactRequest(Program program) throws Exception {
		return RescopeVarReq.from(this, program);
	}

	@Override
	public JsonElement serialize(DuplicateConstant src, Type typeOfSrc, JsonSerializationContext context) {
		// Never been called
		return null;
	}
}
