package anlys.smell;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import ast.Program;
import ast.Sprite;
import ast.VarAccess;
import ast.VarDecl;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class BroadScopeVarAnalyzer extends SmellAnalyzer<BroadScopeVar> {

	@Override
	public SmellListResult<BroadScopeVar> analyze(Program program) {
		SmellListResult<BroadScopeVar> result = new SmellListResult<>();

		Map<VarDecl, Sprite> smellInstanceMap = detectGlobalVarWithOneTimeLocalDef(program);
		List<BroadScopeVar> res = smellInstanceMap.entrySet().stream()
				.map(entry -> new BroadScopeVar(entry.getKey(), entry.getValue())).collect(Collectors.toList());

		result.setInstances(res);
		return result;
	}

	private Map<VarDecl, Sprite> detectGlobalVarWithOneTimeLocalDef(Program program) {
		Map<VarDecl, Sprite> instances = new HashMap<>();
		// iterate each global var
		for (VarDecl v : program.getStage().getVarDeclList()) {
			if (v.allDefs().size() == 1) {
				VarAccess def = (VarAccess) v.allDefs().iterator().next();
				if (!def.sprite().equals(program.getStage())) {
					log.debug("{} : {}", v.getName(), def.sprite().getName());
					log.debug("#uses: {}", v.allUsesInProgram().size());
					instances.put(v, (Sprite) def.sprite());
				}
			}
		}
		return instances;
	}

}
