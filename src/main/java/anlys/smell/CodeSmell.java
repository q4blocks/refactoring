package anlys.smell;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonSubTypes.Type;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.hash.Hashing;
import com.google.gson.JsonElement;
import com.google.gson.JsonSerializationContext;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

import anlys.refact.RefactRequest;
import ast.Program;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY, getterVisibility = JsonAutoDetect.Visibility.NONE, setterVisibility = JsonAutoDetect.Visibility.NONE)
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type")
@JsonSubTypes({ @Type(value = DuplicateConstant.class, name = "DuplicateConstant"),
		@Type(value = DuplicateCode.class, name = "duplicate_code"),
		@Type(value = DuplicateSprite.class, name = "duplicate_sprite")
})

public abstract class CodeSmell {
	@Getter
	@Setter(AccessLevel.NONE)
	@JsonProperty("metadata")
	protected Map<String, Object> metadata = new HashMap<>();

	String smellId;

	@JsonProperty
	public String getSmellId() {
		if (smellId == null) {
			smellId = computeSmellId();
		}
		return smellId;
	}

	/**
	 * should be unique for each smell instance
	 * use Hashing.farmHashFingerprint64() to compute hash based on the id of code
	 * elements appear in this smell
	 * 
	 * @return
	 */
	public abstract String computeSmellId();

	public String toJson() {
		ObjectMapper mapper = new ObjectMapper();
		try {
			return mapper.writeValueAsString(this);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
			return "{}";
		}
	}
	
	public String toPrettyJson() {
		ObjectMapper mapper = new ObjectMapper();
		try {
			return mapper.writerWithDefaultPrettyPrinter().writeValueAsString(this);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
			return "{}";
		}
	}

	public abstract <T extends CodeSmell> T fromJson(String jsonStr, Program program) throws SmellJson2ObjException;

	public abstract <T extends CodeSmell> RefactRequest<T> buildRefactRequest(Program program) throws Exception;
}
