package anlys.smell;

import java.io.IOException;
import java.lang.reflect.Type;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.hash.HashFunction;
import com.google.common.hash.Hasher;
import com.google.common.hash.Hashing;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import anlys.refact.ExtractProcedureReq;
import anlys.refact.Fragment;
import anlys.refact.RefactRequest;
import ast.Program;
import ast.Stmt;
import clone.result.CloneGroup;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * internally use CloneGroup as internal data
 * 
 * @author karn
 *
 */
@NoArgsConstructor
@Setter
@Getter
@Slf4j
public class DuplicateCode extends CodeSmell implements JsonSerializer<DuplicateCode> {
	private String target;
	private List<Fragment> fragments = new ArrayList<>();

	public DuplicateCode(CloneGroup cloneGroup) {
		target = cloneGroup.getTarget();
		for (int i = 0; i < cloneGroup.size(); i++) {
			List<Stmt> stmtSeq = cloneGroup.getPart(i).get();
			Fragment segment = new Fragment(stmtSeq);
			fragments.add(segment);
		}

		// metrics
		metadata.put("_id", computeSmellId());
		metadata.put("group_size", cloneGroup.size());
//		metadata.put("instance_size", cloneGroup.getPart(0).size());
		// TODO: instance size should be AST size
		metadata.put("instance_size", cloneGroup.getPart(0).numTotalStmt());
//		System.out.println("fragmentsize"+cloneGroup.getPart(0).asNameSeq().size());
	}

	@Override
	public JsonElement serialize(DuplicateCode src, Type typeOfSrc, JsonSerializationContext context) {
		// Only use Json for deserialization
		return null;
	}

	public int size() {
		return fragments.size();
	}

	public Fragment getFragment(int i) {
		return fragments.get(i);
	}

	@SuppressWarnings("unchecked")
	@Override
	public DuplicateCode fromJson(String jsonStr, Program program) throws SmellJson2ObjException {
		ObjectMapper mapper = new ObjectMapper();
		DuplicateCode instance;
		try {
			instance = mapper.readValue(jsonStr, DuplicateCode.class);
			for (Fragment f : instance.getFragments()) {
				f.bind(program);
			}
			return instance;
		} catch (IOException e) {
			throw new SmellJson2ObjException("Error DuplicateCode smell deserialization", e);
		}

	}

	// TODO: https://github.com/google/guava/wiki/HashingExplained
	// http://llimllib.github.io/bloomfilter-tutorial/
	public String computeHash() {
		HashFunction hf = Hashing.farmHashFingerprint64();
		Hasher hasher = hf.newHasher();
		for (Fragment f : this.fragments) {
			for (String blockId : f.getStmtIds()) {
				hasher.putString(blockId, StandardCharsets.UTF_8);
			}

		}

		return hasher.hash().toString();
	}

	@Override
	public String computeSmellId() {
		return computeHash();
	}

	@SuppressWarnings("unchecked")
	@Override
	public ExtractProcedureReq buildRefactRequest(Program program) throws Exception {
		return ExtractProcedureReq.from(this, program);
	}

}
