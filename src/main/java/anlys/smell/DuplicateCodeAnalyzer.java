package anlys.smell;

import ast.Program;
import ast.Sprite;
import clone.analyzer.CloneStmtSeqAnalyzer;
import clone.analyzer.SequenceGenStrategy;
import clone.result.CloneGroup;
import clone.result.CloneSeqResult;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class DuplicateCodeAnalyzer extends SmellAnalyzer<DuplicateCode> {
	@Setter
	int groupSize = 2;
	@Setter
	int minSubseqSize = 2;

	@Override
	public SmellListResult<DuplicateCode> analyze(Program program) {
		SmellListResult<DuplicateCode> results = new SmellListResult<>();
		CloneStmtSeqAnalyzer<Sprite> analyzer = new CloneStmtSeqAnalyzer<>(SequenceGenStrategy.GROUP_BY_SPRITE);
		analyzer.setGroupSize(groupSize);
		analyzer.setMinSubseqSize(minSubseqSize);

		CloneSeqResult<Sprite> cloneGroupResult = analyzer.analyze(program);

		for (CloneGroup group : cloneGroupResult.groups()) {
			DuplicateCode codeDupSmell = new DuplicateCode(group);
			results.add(codeDupSmell);
			log.debug("found in {}, id:{}", codeDupSmell.getTarget(), codeDupSmell.getSmellId());
		}
		return results;
	}

}
