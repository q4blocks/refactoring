package anlys.smell;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.Collectors;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import anlys.metrics.MetricKey;
import anlys.refact.Fragment;
import anlys.refact.ParameterAnalyzer;
import anlys.smell.DuplicateConstant.ExprElement;
import ast.Expr;
import ast.Literal;
import ast.Program;
import ast.Scriptable;
import ast.Sprite;
import ast.Stmt;
import clone.analyzer.CloneExprAnalyzer;
import clone.analyzer.CloneStmtSeqAnalyzer;
import clone.analyzer.SequenceGenStrategy;
import clone.data.CExpr;
import clone.result.CloneExprResult;
import clone.result.CloneExprResult.CloneGroup;
import clone.result.CloneSeqResult;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class DuplicateConstAnalyzer extends SmellAnalyzer<DuplicateConstant> {

	enum Key implements MetricKey {
		// magic-value
		appl_repetition, appl_scope, appl_partOfLargerClone, appl_constantType, appl_exprLength
	}

	@Getter
	@Setter
	int minCloneGroupSize = 5;
	@Getter
	@Setter
	int minLiteralLength = 2;

	public SmellListResult<DuplicateConstant> analyze(Program program) {
		SmellListResult<DuplicateConstant> result = new SmellListResult<>();
		List<DuplicateConstant> res = analyzeByLiteralFrequencyPerTarget(program);
		log.debug("found {} smells", res.size());
		result.setInstances(res);
		return result;
	}

	private List<DuplicateConstant> analyzeByLiteralFrequencyPerTarget(Program program) {
		List<DuplicateConstant> results = Lists.newArrayList();
		for (Scriptable s : program.getStageAndSprites()) {

			Map<String, List<Literal>> groups = s.literalColl().stream()
					.filter(l -> l.getValue().length() >= minLiteralLength)
					.filter(l -> l.getAttrs().get("type").contains("math") || l.getAttrs().get("type").equals("text"))
					.collect(Collectors.groupingBy(Literal::getValue, Collectors.toList()));

			Map<String, List<Literal>> frequentGroups = groups.entrySet().stream()
					.filter(e -> e.getValue().size() >= minCloneGroupSize)
					.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
			Map<String, Integer> frequentGroupSummary = frequentGroups.entrySet().stream()
					.collect(Collectors.toMap(Map.Entry::getKey, e -> e.getValue().size()));

			for (Entry<String, List<Literal>> entry : frequentGroups.entrySet()) {
				List<ExprElement> duplicateElms = entry.getValue().stream()
						.map(DuplicateConstant::createExprElementOf).collect(Collectors.toList());
				results.add(new DuplicateConstant(duplicateElms));
			}

			if (!frequentGroupSummary.isEmpty()) {
				log.debug("{} -> {}", s.getName(), frequentGroupSummary);
			}

		}

		return results;
	}

	Set<DuplicateConstant> analyzeConstantAcrossDuplicateFragment(Program program) {
		CloneStmtSeqAnalyzer<Sprite> analyzer = new CloneStmtSeqAnalyzer<>(SequenceGenStrategy.GROUP_BY_SPRITE);

		CloneSeqResult<Sprite> cloneGroupResult = analyzer.analyze(program);

		for (clone.result.CloneGroup group : cloneGroupResult.groups()) {
			List<Fragment> fragments = Lists.newArrayList();
			for (int i = 0; i < group.size(); i++) {
				List<Stmt> stmtSeq = group.getPart(i).get();
				fragments.add(new Fragment(stmtSeq));
			}
			List<List<Expr>> res = new ParameterAnalyzer().analyze(fragments).getConstantExprInputs();
			res.stream()
					.filter(g -> g.size() > 2)
					.filter(g -> g.get(0) instanceof Literal)
					.filter(g -> g.get(0).toSbString().length() > 2)
					.forEach(g -> log.debug("{}\t{} [x{}]", group.getTarget(), g.get(0).toSbString(),
							g.size()));
		}

		return Sets.newHashSet();
	}

	Set<DuplicateConstant> analyzerHelper(Program program) {
		Set<DuplicateConstant> values = new HashSet<>();
		CloneExprAnalyzer analyzer = new CloneExprAnalyzer();
		Map<Expr, List<CExpr>> res = analyzer.analyze(program.getStage().getSprite(0));
		CloneExprResult result = new CloneExprResult(res);
		for (CloneGroup group : result.groups()) {
			List<ExprElement> elements = group.get().stream()
					.map(cexpr -> cexpr.get()).map(DuplicateConstant::createExprElementOf).collect(Collectors.toList());
			values.add(new DuplicateConstant(elements));
		}
		return values;
	}

}
