package anlys.smell;

import java.io.IOException;
import java.lang.reflect.Type;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Lists;
import com.google.common.hash.HashFunction;
import com.google.common.hash.Hasher;
import com.google.common.hash.Hashing;
import com.google.gson.JsonElement;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import anlys.refact.ExtractConstReq;
import anlys.refact.Fragment;
import ast.Expr;
import ast.Literal;
import ast.Program;
import clone.analyzer.ASTNodeSerializer;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@NoArgsConstructor
@Setter
@Getter
@Slf4j
public class DuplicateConstant extends CodeSmell implements JsonSerializer<DuplicateConstant> {
	@JsonIgnore
	List<ExprElement> elements = Lists.newArrayList();
	private List<String> valueIds = new ArrayList<>();
	
	@Override
	public String toString() {
		return "DuplicateConstant [elements=" + elements + "]";
	}

	public DuplicateConstant(List<ExprElement> elements) {
		this.elements = elements;

		// metrics
		metadata.put("_id", computeSmellId());
		metadata.put("group_size", elements.size());
		if (elements.get(0).getExpr() instanceof Literal) {
			String strValue = ((Literal) elements.get(0).getExpr()).getValue();
			metadata.put("literal_value", strValue);
			metadata.put("instance_size", strValue.length());
		} else {
			metadata.put("instance_size", new ASTNodeSerializer().serialize(elements.get(0).getExpr()).size());
		}
		
		// append valueIds
		for(ExprElement e:elements) {
			valueIds.add(e.blockId);
		}
	}

	@Override
	public String computeSmellId() {
		HashFunction hf = Hashing.farmHashFingerprint64();
		Hasher hasher = hf.newHasher();
		elements.stream().map(e -> e.blockId).sorted().forEach(id -> {
			hasher.putString(id, StandardCharsets.UTF_8);
		});

		return hasher.hash().toString();
	}

	@SuppressWarnings("unchecked")
	@Override
	public ExtractConstReq buildRefactRequest(Program program) throws Exception {
		return ExtractConstReq.from(this, program);
	}

	public static ExprElement createExprElementOf(Expr expr) {
		return new DuplicateConstant().new ExprElement(expr);
	}

	@SuppressWarnings("unchecked")
	@Override
	public DuplicateConstant fromJson(String jsonStr, Program program) throws SmellJson2ObjException {
		ObjectMapper mapper = new ObjectMapper();
		DuplicateConstant instance;
		try {
			instance = mapper.readValue(jsonStr, DuplicateConstant.class);
			for (ExprElement e : instance.getElements()) {
				e.bind(program);
			}

		} catch (IOException e) {
			throw new SmellJson2ObjException("Error Duplicate Constant smell deserialization", e);
		}
		return null;
	}

	@Override
	public JsonElement serialize(DuplicateConstant src, Type typeOfSrc, JsonSerializationContext context) {
		// TODO Auto-generated method stub
		return null;
	}

	@NoArgsConstructor
	@Getter
	@Setter
	public class ExprElement {

		// Recurring constant values:
		// blockId : use id of block for block expression or shadow id for literal value
		@JsonIgnore
		Expr expr;

		String blockId;

		public ExprElement(Expr expr) {
			this.expr = expr;
			this.blockId = expr.getId();
		}

		public void bind(Program program) throws SmellJson2ObjException {

			Optional<Expr> exprOpt = program.lookupExpr(blockId);
			if (exprOpt.isPresent()) {
				this.expr = exprOpt.get();
			} else {
				throw new SmellJson2ObjException("failed to bind expr with blockId: " + blockId);
			}

		}

		@Override
		public int hashCode() {
			return blockId.hashCode();
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			ExprElement other = (ExprElement) obj;
			if (blockId == null) {
				if (other.blockId != null)
					return false;
			} else if (!blockId.equals(other.blockId)) {
				return false;
			}

			return true;
		}

		@Override
		public String toString() {
			return "Element [expr=" + expr.toDevString() + ", blockId=" + blockId + "]";
		}
	}

}