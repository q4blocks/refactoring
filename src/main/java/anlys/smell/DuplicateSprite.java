package anlys.smell;

import java.io.IOException;
import java.lang.reflect.Type;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.hash.HashFunction;
import com.google.common.hash.Hasher;
import com.google.common.hash.Hashing;
import com.google.gson.JsonElement;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import anlys.refact.ExtractParentSpriteReq;
import ast.Program;
import ast.Scriptable;
import ast.Sprite;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@NoArgsConstructor
@Setter
@Getter
@Slf4j
public class DuplicateSprite extends CodeSmell implements JsonSerializer<DuplicateSprite> {
	private List<SpriteCloneElement> sprites = new ArrayList<>();

	public DuplicateSprite(Collection<Sprite> spriteColl) {
		spriteColl.forEach(sprite -> {
			sprites.add(new SpriteCloneElement(sprite));
		});

		// metrics
		metadata.put("_id", computeSmellId());
		metadata.put("group_size", sprites.size());
		metadata.put("instance_size", sprites.get(0).getSprite().getNumScript());
		metadata.put("sprites", sprites.stream().map(SpriteCloneElement::getSpriteName).collect(Collectors.toList()));
	}

	@Override
	public String computeSmellId() {
		HashFunction hf = Hashing.farmHashFingerprint64();
		Hasher hasher = hf.newHasher();
		for (SpriteCloneElement s : sprites) {
			hasher.putString(s.getSpriteName(), StandardCharsets.UTF_8);
		}
		return hasher.hash().toString();
	}

	@SuppressWarnings("unchecked")
	@Override
	public ExtractParentSpriteReq buildRefactRequest(Program program) throws Exception {
		return ExtractParentSpriteReq.from(this, program);
	}

	@Override
	public JsonElement serialize(DuplicateSprite src, Type typeOfSrc, JsonSerializationContext context) {
		// Only use Json for deserialization
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public DuplicateSprite fromJson(String jsonStr, Program program) throws SmellJson2ObjException {
		ObjectMapper mapper = new ObjectMapper();
		DuplicateSprite instance;
		try {
			instance = mapper.readValue(jsonStr, DuplicateSprite.class);
			for (SpriteCloneElement element : instance.getSprites()) {
				Optional<Scriptable> spriteOpt = program.getTargetByName(element.getSpriteName());
				if (spriteOpt.isPresent() && spriteOpt.get() instanceof Sprite) {
					element.setSprite((Sprite) spriteOpt.get());
				}
			}

			return instance;
		} catch (IOException e) {
			throw new SmellJson2ObjException("Error DuplicateSprite smell deserialization", e);
		}

	}

}
