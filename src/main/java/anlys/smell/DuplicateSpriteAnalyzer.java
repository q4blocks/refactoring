package anlys.smell;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import com.google.common.collect.Lists;

import ast.Program;
import ast.Sprite;
import clone.analyzer.SpriteHash;
import clone.analyzer.SpriteHashTable;
import lombok.extern.slf4j.Slf4j;

/*
 * should not consider unreachable e.g. 289416332
 */
@Slf4j
public class DuplicateSpriteAnalyzer extends SmellAnalyzer<DuplicateSprite> {

	@Override
	public SmellListResult<DuplicateSprite> analyze(Program program) {
		SmellListResult<DuplicateSprite> results = new SmellListResult<>();
		Collection<Sprite> sprites = Lists.newArrayList(program.getSpriteList());

		SpriteHashTable table = SpriteHashTable.of(sprites);

		for (Integer hashKey : table.get().keySet()) {
			List<SpriteHash> candidateGroup = table.get().get(hashKey);
			List<Sprite> duplicateSprites = candidateGroup.stream().map(h -> h.getSprite())
					.collect(Collectors.toList());
			results.add(new DuplicateSprite(duplicateSprites));
		}

		return results;
	}

}
