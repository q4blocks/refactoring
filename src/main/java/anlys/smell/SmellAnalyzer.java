package anlys.smell;

import anlys.Analyzer;
import ast.Program;

public abstract class SmellAnalyzer<T extends CodeSmell> extends Analyzer {
	public abstract SmellListResult<T> analyze(Program program);
}