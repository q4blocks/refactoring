package anlys.smell;

@SuppressWarnings("serial")
public class SmellJson2ObjException extends Exception {
	public SmellJson2ObjException(String msg) {
		super(msg);
	}

	public SmellJson2ObjException(String msg, Exception e) {
		super(msg, e);
	}
}
