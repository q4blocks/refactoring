package anlys.smell;

import java.util.List;

import com.google.common.collect.Lists;

import anlys.AnalysisResult;
import anlys.refact.RefactRequest;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@EqualsAndHashCode(callSuper = false)
@Getter
@Setter
public class SmellListResult<T extends CodeSmell> extends AnalysisResult {
	private List<T> instances;

	public SmellListResult() {
		instances = Lists.newArrayList();
	}

	public void add(T smellInstance) {
		instances.add(smellInstance);
	}

	public int size() {
		return instances.size();
	}

	public T get(int i) {
		return instances.get(i);
	}

}
