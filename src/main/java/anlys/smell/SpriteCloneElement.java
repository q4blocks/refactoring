package anlys.smell;

import com.fasterxml.jackson.annotation.JsonIgnore;

import ast.Sprite;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
public class SpriteCloneElement {
	@JsonIgnore
	private Sprite sprite;
	private String spriteName;

	public SpriteCloneElement(Sprite sprite) {
		this.sprite = sprite;
		this.spriteName = sprite.getName();
	}
}