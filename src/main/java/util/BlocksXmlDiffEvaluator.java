package util;

import java.util.List;

import org.w3c.dom.Attr;
import org.w3c.dom.Node;
import org.xmlunit.diff.Comparison;
import org.xmlunit.diff.ComparisonResult;
import org.xmlunit.diff.DifferenceEvaluator;

import com.google.common.collect.Lists;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class BlocksXmlDiffEvaluator implements DifferenceEvaluator {
	private List<String> attributeNames = Lists.newArrayList();

	public BlocksXmlDiffEvaluator(String attributeName) {
	}

	public BlocksXmlDiffEvaluator ignore(String attributeName) {
		attributeNames.add(attributeName);
		return this;
	}

	@Override
	public ComparisonResult evaluate(Comparison comparison, ComparisonResult outcome) {
		if (outcome == ComparisonResult.EQUAL)
			return outcome;
		final Node controlNode = comparison.getControlDetails().getTarget();
		if (controlNode instanceof Attr) {
			Attr attr = (Attr) controlNode;
			if (attributeNames.contains(attr.getName())) {
				return ComparisonResult.SIMILAR;
			}
		}
		return outcome;
	}
}