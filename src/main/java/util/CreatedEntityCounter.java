package util;

import java.util.HashMap;
import java.util.Map;

import lombok.extern.slf4j.Slf4j;

/**
 * A singleton for generating name
 * 
 * @author karn
 *
 */
@Slf4j
public class CreatedEntityCounter {
	private static CreatedEntityCounter instance = null;
	private static Map<String, Integer> counter = new HashMap<>();

	// private constructor restricted to this class itself
	private CreatedEntityCounter() {

	}

	public int get(String entityName) {
		counter.putIfAbsent(entityName, 1);
		int current = counter.get(entityName);
		counter.put(entityName, counter.get(entityName) + 1);
		return current;
	}

	public static CreatedEntityCounter get() {
		if (instance == null) {
			instance = new CreatedEntityCounter();
		}
		return instance;
	}

	public static int getCount(String entityName) {
		return get().get(entityName);
	}
	
	public static String getCountedName(String entityName) {
		return entityName+"_"+getCount(entityName);
	}

	public static void reset() {
		counter = new HashMap<>();
	}

}
