package util;

import java.util.LinkedList;

import util.diff_match_patch.Diff;

public class DiffTextGenerator {
	diff_match_patch dmp = new diff_match_patch();
	private LinkedList<Diff> diffs;
	private ProjectFileWriter projectFileWriter;

	public DiffTextGenerator(ProjectFileWriter projectFileWriter) {
		this.projectFileWriter = projectFileWriter;
	}

	public DiffTextGenerator compare(String before, String after) {
		diffs = dmp.diff_main(before, after, false);
		dmp.diff_cleanupEfficiency(diffs);
		return this;
	}

	public void toPrettyHtml(String projectId) throws Exception {
		if (projectFileWriter == null) {
			throw new Exception("No project file writer given");
		}
		if (!diffs.isEmpty()) {
			String htmlStr = dmp.diff_prettyHtml(diffs);
			projectFileWriter.write(htmlStr, projectId, "html");
		}
	}

}
