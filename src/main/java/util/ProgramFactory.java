package util;

import ast.Program;
import ast.Sprite;
import ast.Stage;

public class ProgramFactory {

	public static Program programOf(Sprite sprite) {
		Program program = new Program();
		program.setStage(new Stage());
		program.getStage().addSprite(sprite);
		return program;
	}

}
