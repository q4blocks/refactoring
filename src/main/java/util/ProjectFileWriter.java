package util;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ProjectFileWriter {
	@Setter
	@Getter
	private String basePath = "./tmp/";

	public ProjectFileWriter(String basePath) {
		this.basePath = basePath;
		initialize();
	}

	public void appendBasePath(String subpathName) {
		this.basePath = basePath + subpathName + "/";
	}

	public ProjectFileWriter() {
		initialize();
	}

	private void initialize() {
		File file = new File(basePath);
		file.mkdirs();
	}

	/**
	 * 
	 * @param data
	 * @param fileName
	 * @param fileType
	 *            without dot(.)
	 * @throws IOException
	 */
	public void write(String data, String fileName, String fileType) throws IOException {
		String filePath = basePath + fileName + "." + fileType;
		File file = new File(filePath);
		FileUtils.writeStringToFile(file, data);
		log.info("write to {}", filePath);
	}
}
