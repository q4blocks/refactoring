package util;

import java.io.File;
import java.io.IOException;
import java.text.NumberFormat;
import java.text.ParseException;

import org.apache.commons.io.FileUtils;

import ast.ProcDecl;
import ast.Program;
import ast.ScratchBlock;
import lombok.extern.slf4j.Slf4j;
import sb3.parser.ProcDeclConvert;
import sb3.parser.ProcDefEl;
import sb3.parser.ScratchBlockAstConvert;
import sb3.parser.ProgramConvert;
import sb3.parser.ProgramEl;

@Slf4j
public class UtilFunc {
	private UtilFunc() {

	}

	public static String xmlFromScratchBlock(ScratchBlock block) {
		String xmlString = new ScratchBlockAstConvert(block).toXml().mkString();
		xmlString = replaceDoubleWithSingleQuote(xmlString);
		return xmlString;
	}

	public static String xmlFromProcDecl(ProcDecl procDecl) {
		ProcDefEl procDefEl = new ProcDeclConvert(procDecl).toProcDefEl();
		return procDefEl.toXml().mkString();
	}
	
	public static String xmlFromProgram(Program program) {
		ProgramEl programEl = new ProgramConvert(program).toProgramEl();
		return programEl.toXml().mkString();
	}

	public static String replaceDoubleWithSingleQuote(String xmlString) {
		xmlString = xmlString.replaceAll("\"", "\'");
		return xmlString;
	}

	public static String readStringFromResource(String fileName) throws IOException {
		File file = new File(UtilFunc.class.getClassLoader().getResource(fileName).getFile());
		String string = FileUtils.readFileToString(file);
		return string;
	}
	
	public static String formatNumString(String str, int decimalPlace) throws ParseException {
		Number valAfter = NumberFormat.getInstance().parse(str);
		double doubleVal = valAfter.doubleValue();
		return String.format("%." + decimalPlace + "f", doubleVal);
	}

}
