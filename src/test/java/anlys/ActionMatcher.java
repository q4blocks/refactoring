package anlys;

import org.hamcrest.Description;
import org.hamcrest.TypeSafeMatcher;
import org.xmlunit.diff.Diff;

import anlys.refact.transform.action.Action;
import anlys.refact.transform.action.Create;
import anlys.refact.transform.action.Insert;
import anlys.refact.transform.action.Replace;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ActionMatcher<T extends Action> extends TypeSafeMatcher<T> {

	private T expected;
	private Diff diff;

	public T getExpected() {
		return expected;
	}

	public ActionMatcher(T expected) {
		this.expected = expected;
	}

	@Override
	public void describeTo(Description description) {
		description.appendText(expected.toString());
	}

	@Override
	protected boolean matchesSafely(T actual) {
		if (actual instanceof Create) {
			this.diff = TestUtil.blockXmlDiff(((Create) expected).getBlockXml(), ((Create) actual).getBlockXml());
			log.error(diff.toString());
			diff.getDifferences().forEach(d->{
				log.error("{}vs{}",d.getComparison().getControlDetails().getTarget().getParentNode().getParentNode().getAttributes().getNamedItem("id").getNodeValue());
			});
			return !diff.hasDifferences();
		} else if (expected instanceof Insert) {
			boolean sameType = actual instanceof Insert;
			boolean sameRelativePos = ((Insert) expected).isBeforeTarget() == ((Insert) actual).isBeforeTarget();
			return sameType && sameRelativePos;
		} else if (expected instanceof Replace) {
			if (actual instanceof Replace) {
				return ((Replace) actual).getReplaceWith().equals(((Replace) expected).getReplaceWith());
			} else {
				return false;
			}

		} else {
			return match(expected, actual);
		}
	}

	private boolean shareSubSeq(String t1, String t2) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	protected void describeMismatchSafely(T actual, Description mismatchDescription) {
		mismatchDescription.appendText("not matched with").appendValue(actual);
		if (actual instanceof Create) {
			mismatchDescription.appendValue(diff);
		}
	}

	protected boolean match(T expected, T actual) {
		// generic check attribute value
		return expected.equals(actual);
	}

	public static <T extends Action> ActionMatcher<T> similarActionTo(T expected) {
		return new ActionMatcher<>(expected);
	}

}
