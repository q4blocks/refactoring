package anlys;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.hamcrest.Description;
import org.hamcrest.TypeSafeMatcher;

import anlys.refact.transform.action.Action;
import anlys.refact.transform.action.ActionList;

public class ActionsMatcher extends TypeSafeMatcher<ActionList> {
	private final List<ActionMatcher<?>> matchers = new ArrayList<>();
	private final List<ActionMatcher<?>> failedMatchers = new ArrayList<>();
	private ActionList expectedActions;
	
	
	public ActionsMatcher(ActionList expected) {
		this.expectedActions = expected;
		for (Action action : expected.get()) {
			matchers.add(new ActionMatcher<Action>(action));
		}
	}

	@Override
	public void describeTo(Description description) {
		description.appendValueList("\n", " " + "and" + "\n", "", matchers);
	}

	@Override
	protected boolean matchesSafely(ActionList actual) {
		boolean matchesAllMatchers = true;
		for (int i = 0; i < matchers.size(); i++) {
			ActionMatcher<?> matcher = matchers.get(i);
			if (!matcher.matches(actual.get().get(i))) {
				failedMatchers.add(matcher);
				matchesAllMatchers = false;
			}
		}

		return matchesAllMatchers;
	}

	@Override
	protected void describeMismatchSafely(ActionList actual, Description mismatchDescription) {
		mismatchDescription.appendText("\n");
		for (Iterator<ActionMatcher<?>> iterator = failedMatchers.iterator(); iterator.hasNext();) {
			final ActionMatcher<?> matcher = iterator.next();
			
			mismatchDescription.appendDescriptionOf(matcher).appendText(" ");
			int mismatchedActionIndex = expectedActions.get().indexOf(matcher.getExpected());
			Action actualAction = actual.get().get(mismatchedActionIndex);
			matcher.describeMismatch(actualAction, mismatchDescription);
			if (iterator.hasNext()) {
				mismatchDescription.appendText("\n");
			}
		}
	}

	public static ActionsMatcher matchesActions(ActionList expected) {
		return new ActionsMatcher(expected);
	}

}
