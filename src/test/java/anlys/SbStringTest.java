package anlys;

import static org.junit.Assert.*;

import org.junit.Test;

import ast.OperatorExpr;
import ast.ScratchBlock;
import lombok.extern.slf4j.Slf4j;
import sb3.parser.xml.Sb3XmlParser;

@Slf4j
public class SbStringTest {

	@Test
	public void test() {
		Sb3XmlParser parser = new Sb3XmlParser();
		String xml1 = "<block type='operator_random' id='G+o{,t^|Wo|C!8;D,Z[j' x='23' y='236'><value name='FROM'><shadow type='math_number' id='YBq#f_Sz}_n3X#CxkD6}'><field name='NUM'>1</field></shadow><block type='operator_add' id='Bg$IFvPa3@xoSL0F[+5V'><value name='NUM1'><shadow type='math_number' id='U%}{r(|5egNx[H)CrcAM'><field name='NUM'>1</field></shadow></value><value name='NUM2'><shadow type='math_number' id=';9IW!mI%#IW@Z*(KObzp'><field name='NUM'>2</field></shadow></value></block></value><value name='TO'><shadow type='math_number' id='K`13_.4E^jD.ePtUCB=U'><field name='NUM'>10</field></shadow><block type='operator_subtract' id='AnLHdDt(8aD0_C[R2;;4'><value name='NUM1'><shadow type='math_number' id='7:lv4{Gggh7$kRVTuI$L'><field name='NUM'></field></shadow><block type='operator_multiply' id='9JqY#CG1#~c-)/ELR1qT'><value name='NUM1'><shadow type='math_number' id='mqC[O]{Kg78R6F9@34H5'><field name='NUM'>3</field></shadow></value><value name='NUM2'><shadow type='math_number' id='[-7NR4Kt5_.Y{ACsTqrf'><field name='NUM'>4</field></shadow></value></block></value><value name='NUM2'><shadow type='math_number' id='uhHI-FJF@Y5G?%K4o(FF'><field name='NUM'></field></shadow><block type='operator_divide' id='@tQ`DImqU8|B;@-w+H%O'><value name='NUM1'><shadow type='math_number' id='qQUUw1VRnPvllHvd|uVw'><field name='NUM'>5</field></shadow></value><value name='NUM2'><shadow type='math_number' id='fPRjl48I?nk@;z|{42?q'><field name='NUM'>6</field></shadow></value></block></value></block></value></block>";
		String xml2 = "<block type='operator_and' id='Wg*YHVVA3E_TTBWs_~t@' x='291' y='337'><value name='OPERAND1'><block type='operator_lt' id='aYV-p6Z}B5}gL/PgioDE'><value name='OPERAND1'><shadow type='text' id='1BypO7UHCSEj9afyGJs)'><field name='TEXT'>1</field></shadow></value><value name='OPERAND2'><shadow type='text' id='oC25/!ptPfj3e7-hKASi'><field name='TEXT'>2</field></shadow></value></block></value><value name='OPERAND2'><block type='operator_or' id='9!,N]%z~5d*-_dh6*yTG'><value name='OPERAND1'><block type='operator_equals' id='Eiqz/fG;3@o)C[V5;!gm'><value name='OPERAND1'><shadow type='text' id='0tS}OC5#}1e4Fp{VpbRu'><field name='TEXT'>3</field></shadow></value><value name='OPERAND2'><shadow type='text' id='uBVL:p~$UM5n8.C9Sxg%'><field name='TEXT'>4</field></shadow></value></block></value><value name='OPERAND2'><block type='operator_not' id='~fC2;rd-3RyH};p$S_iF'><value name='OPERAND'><block type='operator_gt' id='w%9#@K*XsusNARqo:sp='><value name='OPERAND1'><shadow type='text' id='E$7Rd?fWZCFslppjtJ!o'><field name='TEXT'>5</field></shadow></value><value name='OPERAND2'><shadow type='text' id='Vgx~Pz1vy[z%q)O[`y-u'><field name='TEXT'>6</field></shadow></value></block></value></block></value></block></value></block>";
		String xml3 = "<block type='operator_join' id='{hk.R2.?$8}?e0hdzwRL' x='91' y='513'><value name='STRING1'><shadow type='text' id='gB#,a-!3*8_=jZ_)!-3i'><field name='TEXT'>hello</field></shadow></value><value name='STRING2'><shadow type='text' id='^Ib3{(hwx[UeGP(mL|+~'><field name='TEXT'>world</field></shadow><block type='operator_letter_of' id='3)7v11xrtEbh`q,if_kD'><value name='LETTER'><shadow type='math_whole_number' id='JskKd=t?c9]balyJSyU,'><field name='NUM'>1</field></shadow><block type='operator_mod' id='4/Lu{D0Mr+o2|if+/U2['><value name='NUM1'><shadow type='math_number' id='ALJR3e=4l7!{U=FT:?%P'><field name='NUM'>1</field></shadow><block type='operator_mathop' id='mxQ+=;J^q(68zR}UyWh2'><field name='OPERATOR'>abs</field><value name='NUM'><shadow type='math_number' id='r)gVZzW[9NzrU,:nI(B8'><field name='NUM'>3</field></shadow></value></block></value><value name='NUM2'><shadow type='math_number' id='VLhpvWQC2p/5`TQ9Q@aN'><field name='NUM'>2</field></shadow><block type='operator_round' id='(.6M:zsTSidZA}#Efo{.'><value name='NUM'><shadow type='math_number' id='KMz)K-_:?RthcFJ}hhmI'><field name='NUM'>2.3</field></shadow></value></block></value></block></value><value name='STRING'><shadow type='text' id=',YWfc4^AZm0US;B3M:v3'><field name='TEXT'>world</field></shadow><block type='operator_length' id='TxbwGGB5X};j!2HA-:_u'><value name='STRING'><shadow type='text' id='`dAmHf;gEBu@uWSQ#T7Z'><field name='TEXT'>world</field></shadow></value></block></value></block></value></block>";
		String xml4 = "<block type='operator_add' id='yg,*W7S~Jo(9c-GiraO.' x='232' y='397'><value name='NUM1'><shadow type='math_number' id='Kvt=di{ZCX]QO^)!;}Ze'><field name='NUM'>1</field></shadow><block type='motion_xposition' id='sSu]lh[.^_[U+PryxfHC'></block></value><value name='NUM2'><shadow type='math_number' id='dJ)?gn0eMS7=?d?:D=OM'><field name='NUM'></field></shadow><block type='looks_costumenumbername' id='lW=|WJ-w~lEKL/}powx1'><field name='NUMBER_NAME'>number</field></block></value></block>";
		String xml5 = "<block type='sensing_of' id='o!s%bU|0n!N/Q[U}%d#K' x='120' y='545'><field name='PROPERTY'>x position</field><value name='OBJECT'><shadow type='sensing_of_object_menu' id='*8IS)!dse8Q%q({j}pf?'><field name='OBJECT'>Sprite1</field></shadow></value></block>";
		OperatorExpr node = (OperatorExpr) parser.parseBlock(xml5);
		String res = node.toSbString();
		log.info(res);
		
		
	}

}
