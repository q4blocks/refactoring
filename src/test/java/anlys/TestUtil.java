package anlys;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;

import javax.xml.transform.Source;

import org.xmlunit.builder.DiffBuilder;
import org.xmlunit.builder.Input;
import org.xmlunit.diff.Comparison;
import org.xmlunit.diff.ComparisonListener;
import org.xmlunit.diff.ComparisonResult;
import org.xmlunit.diff.ComparisonType;
import org.xmlunit.diff.DOMDifferenceEngine;
import org.xmlunit.diff.Diff;
import org.xmlunit.diff.DifferenceEngine;
import org.xmlunit.input.NormalizedSource;

import lombok.extern.slf4j.Slf4j;
import util.BlocksXmlDiffEvaluator;

@Slf4j
public class TestUtil {
	public static void assertUniqueId(String controlXmlStr, String testXmlStr) {
		Source control = Input.fromString(controlXmlStr).build();
		Source test = Input.fromString(testXmlStr).build();
		DifferenceEngine diff = new DOMDifferenceEngine();
		diff.addDifferenceListener(new ComparisonListener() {
			public void comparisonPerformed(Comparison comparison, ComparisonResult outcome) {
				if (!comparison.getType().equals(ComparisonType.ATTR_VALUE)) {
					return;
				}
				if (comparison.getTestDetails().getTarget().getNodeName().equals("id")) {
					assertNotEquals(comparison.getTestDetails().getValue(), comparison.getControlDetails().getValue());
				}
			}
		});
		diff.compare(control, test);
	}

	public static void assertSimilarBlockXml(String controlXmlStr, String testXmlStr) {
		Diff myDiff = blockXmlDiff(controlXmlStr, testXmlStr);
		assertFalse(myDiff.toString(), myDiff.hasDifferences());
	}

	public static Diff blockXmlDiff(String controlXmlStr, String testXmlStr) {
		return DiffBuilder.compare(controlXmlStr).withTest(testXmlStr)
				.withDifferenceEvaluator(new BlocksXmlDiffEvaluator().ignore("id").ignore("x").ignore("y"))
				.checkForSimilar()
				// .ignoreComments() // [2]
				// .ignoreWhitespace() // [3]
				// .normalizeWhitespace() // [4]
				.ignoreElementContentWhitespace()
				.build();
	}

	public static boolean isBlockXmlSimilar(String controlXmlStr, String testXmlStr) {
		Diff myDiff = blockXmlDiff(controlXmlStr, testXmlStr);
		return !myDiff.hasDifferences();
	}

}
