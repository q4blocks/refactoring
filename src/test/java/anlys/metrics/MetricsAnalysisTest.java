package anlys.metrics;

import static org.hamcrest.Matchers.closeTo;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

import org.junit.Test;

import ast.Program;
import ast.Sprite;
import lombok.extern.slf4j.Slf4j;
import sb3.parser.xml.Sb3XmlParser;
import util.ProgramFactory;

@Slf4j
public class MetricsAnalysisTest {
	private static final double DELTA = 1e-15;

	@Test
	public void testProgramMetricsAnalyzer() {
		String xml = "<program><stage name='Stage'><xml><variables><variable type='' id='`jEk@4|i[#Fk?(8x)AV.-my variable' islocal='false'>my variable</variable></variables><block id='it~Uzrs//R1X:!xg7Gl}' type='event_whenflagclicked' x='277' y='346' ></block></xml></stage><sprite name='Sprite1'><xml><variables></variables><block id='qtb$%.eL1Vvm/X(3L~=9' type='event_whenflagclicked' x='148' y='149' ><next><block id='h8`{hP//8(Nq1#cDT`}?' type='data_setvariableto' ><value name='VALUE'><shadow id='j#EVK3vV#%_#45so0dRG' type='text' ><field name='TEXT'>5</field></shadow></value><field name='VARIABLE' id='`jEk@4|i[#Fk?(8x)AV.-my variable' variabletype=''>my variable</field><next><block id='166%iT,kyMaP1JNI{L[h' type='control_forever' ><value name='SUBSTACK'><block id='A#zuD8-1M(lV,i^Ge*c5' type='motion_movesteps' ><value name='STEPS'><block id='3R@M^jefLbTYB[9hoYo/' type='data_variable' ><field name='VARIABLE' id='`jEk@4|i[#Fk?(8x)AV.-my variable' variabletype=''>my variable</field></block><shadow id='6jtsJD)51tP4lx@0!M[@' type='math_number' ><field name='NUM'>10</field></shadow></value><next><block id='vTu{#U|||Whl*Aw/XKhm' type='motion_turnright' ><value name='DEGREES'><shadow id='7alRb+44~AcFcamDiGJ=' type='math_number' ><field name='NUM'>15</field></shadow></value></block></next></block></value></block></next></block></next></block>,<block id='z7a.W)`{t:$aS(6JE--[' type='event_whenflagclicked' x='600' y='148' ></block></xml></sprite><sprite name='Crab'><xml><variables></variables><block id='/v!,y+iu/pUZHh:,Df^q' type='event_whenflagclicked' x='500' y='-300' ></block></xml></sprite></program>";
		Program program = new Sb3XmlParser().parseProgram(xml);
		ProgramMetricsAnalyzer analyzer = new ProgramMetricsAnalyzer();
		MetricsKeyValue metrics = analyzer.computeMetrics(program).getProjectMetricsInfo();
		log.info(program.toDevString());
		assertThat(metrics.get(Metric.Size.num_blocks), equalTo(9));
		assertThat(metrics.get(Metric.Size.locs), equalTo(9));
		assertThat(metrics.get(Metric.Size.num_scripts), equalTo(4));

		assertThat(ProgramMetricsAnalyzer.numBlocks(program.getTargetByName("Sprite1").get().getScript(0)), equalTo(6));

		assertThat(metrics.get(Metric.Size.num_scriptables), equalTo(3));
		assertThat(metrics.get(Metric.Entity.num_vars), equalTo(1));
		assertThat((double) metrics.get(Metric.Entity.procedure_dens), closeTo(0.0, DELTA));
		assertThat((double) metrics.get(Metric.Quality.long_script_dens), closeTo(0, DELTA));
		assertThat((double) metrics.get(Metric.Quality.complex_script_dens), closeTo(0, DELTA));
	}

	@Test
	public void testLocs() {
		String xml = "<xml xmlns='http://www.w3.org/1999/xhtml'><variables></variables><block type='control_if' id='(8.k_Magm{7kL_$Cq}Jm' x='205' y='193'><value name='CONDITION'><block type='operator_lt' id='xor^WX6~jWW}|D`i8Md*'><value name='OPERAND1'><shadow type='text' id='UKbP;YZvNBsDVndo/92+'><field name='TEXT'>1</field></shadow></value><value name='OPERAND2'><shadow type='text' id='FodXWuE2:z!B1b7bTh1p'><field name='TEXT'>2</field></shadow></value></block></value><statement name='SUBSTACK'><block type='control_repeat' id='7K^NRmb@s~dcOS#vb:$3'><value name='TIMES'><shadow type='math_whole_number' id='JS3JbaQ_*,5$CqT;]uRP'><field name='NUM'>10</field></shadow></value><statement name='SUBSTACK'><block type='motion_movesteps' id='9x-@.z;mW4)4+G:D#oAJ'><value name='STEPS'><shadow type='math_number' id='Y4/U_:M}T!7Hcry5VFjn'><field name='NUM'>10</field></shadow></value><next><block type='control_if_else' id='mJTfMeIz(gH7e}X30dw^'><value name='CONDITION'><block type='operator_equals' id='XI1KB[|sl-zPSO(#;`U6'><value name='OPERAND1'><shadow type='text' id='-NRAw5bI%hGZDUX;Jm4f'><field name='TEXT'>2</field></shadow></value><value name='OPERAND2'><shadow type='text' id='~CLcYWxYmo+|ZN^$:%c^'><field name='TEXT'>2</field></shadow></value></block></value><statement name='SUBSTACK'><block type='motion_movesteps' id=';N.;3)q8=/q`}J{8(L#f'><value name='STEPS'><shadow type='math_number' id='~)6~hHaUBdDju_RJ18^d'><field name='NUM'>10</field></shadow></value></block></statement><statement name='SUBSTACK2'><block type='motion_turnright' id='0.$@uRfUAK(1Y;*=ziyB'><value name='DEGREES'><shadow type='math_number' id='L.?@K,8zy_KF7Q7lxlCP'><field name='NUM'>15</field></shadow></value></block></statement></block></next></block></statement><next><block type='motion_movesteps' id=']sj065:.RwM)XV4cQwW6'><value name='STEPS'><shadow type='math_number' id='QL*=@CBMck}HC*)hAJdb'><field name='NUM'>10</field></shadow></value></block></next></block></statement></block></xml>";
		Sprite sprite = new Sb3XmlParser().parseTarget(xml);
		Program program = ProgramFactory.programOf(sprite);
		ProgramMetricsAnalyzer analyzer = new ProgramMetricsAnalyzer();
		MetricsKeyValue metrics = analyzer.computeMetrics(program).getProjectMetricsInfo();
		assertThat(metrics.get(Metric.Size.locs), equalTo(11));
	}

	@Test
	public void testLiterals() {
		String xml = "<xml xmlns='http://www.w3.org/1999/xhtml'><variables></variables><block type='control_if' id='(8.k_Magm{7kL_$Cq}Jm' x='205' y='193'><value name='CONDITION'><block type='operator_lt' id='xor^WX6~jWW}|D`i8Md*'><value name='OPERAND1'><shadow type='text' id='UKbP;YZvNBsDVndo/92+'><field name='TEXT'>1</field></shadow></value><value name='OPERAND2'><shadow type='text' id='FodXWuE2:z!B1b7bTh1p'><field name='TEXT'>2</field></shadow></value></block></value><statement name='SUBSTACK'><block type='control_repeat' id='7K^NRmb@s~dcOS#vb:$3'><value name='TIMES'><shadow type='math_whole_number' id='JS3JbaQ_*,5$CqT;]uRP'><field name='NUM'>10</field></shadow></value><statement name='SUBSTACK'><block type='motion_movesteps' id='9x-@.z;mW4)4+G:D#oAJ'><value name='STEPS'><shadow type='math_number' id='Y4/U_:M}T!7Hcry5VFjn'><field name='NUM'>10</field></shadow></value><next><block type='control_if_else' id='mJTfMeIz(gH7e}X30dw^'><value name='CONDITION'><block type='operator_equals' id='XI1KB[|sl-zPSO(#;`U6'><value name='OPERAND1'><shadow type='text' id='-NRAw5bI%hGZDUX;Jm4f'><field name='TEXT'>2</field></shadow></value><value name='OPERAND2'><shadow type='text' id='~CLcYWxYmo+|ZN^$:%c^'><field name='TEXT'>2</field></shadow></value></block></value><statement name='SUBSTACK'><block type='motion_movesteps' id=';N.;3)q8=/q`}J{8(L#f'><value name='STEPS'><shadow type='math_number' id='~)6~hHaUBdDju_RJ18^d'><field name='NUM'>10</field></shadow></value></block></statement><statement name='SUBSTACK2'><block type='motion_turnright' id='0.$@uRfUAK(1Y;*=ziyB'><value name='DEGREES'><shadow type='math_number' id='L.?@K,8zy_KF7Q7lxlCP'><field name='NUM'>15</field></shadow></value></block></statement></block></next></block></statement><next><block type='motion_movesteps' id=']sj065:.RwM)XV4cQwW6'><value name='STEPS'><shadow type='math_number' id='QL*=@CBMck}HC*)hAJdb'><field name='NUM'>10</field></shadow></value></block></next></block></statement></block></xml>";
		Sprite sprite = new Sb3XmlParser().parseTarget(xml);
		Program program = ProgramFactory.programOf(sprite);
		ProgramMetricsAnalyzer analyzer = new ProgramMetricsAnalyzer();
		MetricsKeyValue metrics = analyzer.computeMetrics(program).getProjectMetricsInfo();
		assertThat(metrics.get(Metric.Entity.num_literals), equalTo(9));
		log.info(program.toDevString());
	}

	@Test
	public void testInformationHidingFactor() {
		String xml = "<xml xmlns='http://www.w3.org/1999/xhtml'><variables></variables><block type='control_if' id='(8.k_Magm{7kL_$Cq}Jm' x='205' y='193'><value name='CONDITION'><block type='operator_lt' id='xor^WX6~jWW}|D`i8Md*'><value name='OPERAND1'><shadow type='text' id='UKbP;YZvNBsDVndo/92+'><field name='TEXT'>1</field></shadow></value><value name='OPERAND2'><shadow type='text' id='FodXWuE2:z!B1b7bTh1p'><field name='TEXT'>2</field></shadow></value></block></value><statement name='SUBSTACK'><block type='control_repeat' id='7K^NRmb@s~dcOS#vb:$3'><value name='TIMES'><shadow type='math_whole_number' id='JS3JbaQ_*,5$CqT;]uRP'><field name='NUM'>10</field></shadow></value><statement name='SUBSTACK'><block type='motion_movesteps' id='9x-@.z;mW4)4+G:D#oAJ'><value name='STEPS'><shadow type='math_number' id='Y4/U_:M}T!7Hcry5VFjn'><field name='NUM'>10</field></shadow></value><next><block type='control_if_else' id='mJTfMeIz(gH7e}X30dw^'><value name='CONDITION'><block type='operator_equals' id='XI1KB[|sl-zPSO(#;`U6'><value name='OPERAND1'><shadow type='text' id='-NRAw5bI%hGZDUX;Jm4f'><field name='TEXT'>2</field></shadow></value><value name='OPERAND2'><shadow type='text' id='~CLcYWxYmo+|ZN^$:%c^'><field name='TEXT'>2</field></shadow></value></block></value><statement name='SUBSTACK'><block type='motion_movesteps' id=';N.;3)q8=/q`}J{8(L#f'><value name='STEPS'><shadow type='math_number' id='~)6~hHaUBdDju_RJ18^d'><field name='NUM'>10</field></shadow></value></block></statement><statement name='SUBSTACK2'><block type='motion_turnright' id='0.$@uRfUAK(1Y;*=ziyB'><value name='DEGREES'><shadow type='math_number' id='L.?@K,8zy_KF7Q7lxlCP'><field name='NUM'>15</field></shadow></value></block></statement></block></next></block></statement><next><block type='motion_movesteps' id=']sj065:.RwM)XV4cQwW6'><value name='STEPS'><shadow type='math_number' id='QL*=@CBMck}HC*)hAJdb'><field name='NUM'>10</field></shadow></value></block></next></block></statement></block></xml>";
		Sprite sprite = new Sb3XmlParser().parseTarget(xml);
		Program program = ProgramFactory.programOf(sprite);
		ProgramMetricsAnalyzer analyzer = new ProgramMetricsAnalyzer();
		MetricsKeyValue metrics = analyzer.computeMetrics(program).getProjectMetricsInfo();
		// assertThat(metrics.get(Metric.Size.locs), equalTo(11));
	}

}
