package anlys.refact;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

import org.junit.Test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Lists;

import anlys.metrics.Metric;
import anlys.refact.transform.action.ActionList;
import anlys.refact.transform.action.Create;
import anlys.refact.transform.action.Insert;
import anlys.refact.transform.action.Replace;
import anlys.refact.transform.action.VarDeclare;
import anlys.smell.DuplicateCode;
import anlys.smell.SmellListResult;
import ast.ExprStmt;

public class ActionSerializationTest {

	@Test
	public void shouldSerializeActionsToJSON() throws JsonProcessingException {
		ObjectMapper mapper = new ObjectMapper();

		VarDeclare varDecl = new VarDeclare();
		varDecl.setVarId("id");
		varDecl.setVarName("varName");

		Insert insert = new Insert();
		insert.setInsertedBlock("a");
		insert.setTargetBlock("t");

		Replace replace = new Replace();
		replace.setTargetBlock("beforeID");
		replace.setReplaceWith("afterID");

		ExprStmt stmt = new ExprStmt();
		stmt.setBlockId("block1");
		stmt.setOpcode("move");
		Create create = new Create(stmt);

		ActionList actions = new ActionList();
		actions.addAction(varDecl);
		actions.addAction(create);
		actions.addAction(insert);
		actions.addAction(replace);

		String jsonStr = mapper.writeValueAsString(actions);
		assertThat(jsonStr.contains("undefined"), equalTo(false));
		assertThat(jsonStr.contains("type"), equalTo(true));
	}

	@Test
	public void shouldSerializeSmellResultToJson() throws JsonProcessingException {
		SmellListResult<DuplicateCode> result = new SmellListResult<>();
		result.setMetadata(Metric.Size.num_blocks, 50);
		result.setInstances(Lists.newArrayList());
		String jsonStr = result.toJson();
		assertThat(jsonStr.contains("undefined"), equalTo(false));
		assertThat(jsonStr.contains("metadata"), equalTo(true));
	}

}
