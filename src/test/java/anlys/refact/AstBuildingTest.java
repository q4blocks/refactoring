package anlys.refact;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;

import anlys.refact.transform.ASTBuilder;
import anlys.refact.transform.ArgBuildSpec;
import anlys.refact.transform.ParamSpec;
import anlys.refact.transform.TransformerHelper;
import anlys.refact.transform.action.TransformRes;
import ast.AssignStmt;
import ast.BlockSeq;
import ast.Expr;
import ast.ExprStmt;
import ast.IfElseStmt;
import ast.IncrementStmt;
import ast.OperatorExpr;
import ast.ProcAccess;
import ast.ProcDecl;
import ast.ScratchBlock;
import ast.Stmt;
import ast.StrLiteral;
import ast.VarAccess;
import ast.VarDecl;
import lombok.extern.slf4j.Slf4j;
import sb3.parser.ProcDeclConvert;
import sb3.parser.ProcDefEl;
import sb3.parser.ScratchBlockAstConvert;
import sb3.parser.xml.Sb3XmlParser;

@Slf4j
public class AstBuildingTest {
	Sb3XmlParser parser = new Sb3XmlParser();
	TransformerHelper transformer = new TransformerHelper();
	private ASTBuilder astBuilder;

	@Before
	public void setup() {
		astBuilder = new ASTBuilder("", "");
	}

	@Test
	public void testAssignmentDefaultVal() {
		TransformRes<VarDecl> varDeclareRes = transformer.declareVar(null, "temp", "temp_id");

		TransformRes<AssignStmt> assignStmtCreateRes = astBuilder.createAssignment((VarDecl) varDeclareRes.getNode(),
				new StrLiteral("A"), "A");
		System.out.println(getAsXmlString((ScratchBlock) assignStmtCreateRes.getNode()));
	}

	@Test
	public void testAssignmentOpExpr() {
		TransformRes<VarDecl> varDeclareRes = transformer.declareVar(null, "temp", "temp_id");
		ScratchBlock opExpr = parser.parseBlock(
				"<block type=\"operator_add\" id=\"|{B4/#BlHWJ^x=[!yl4N\" x=\"227\" y=\"53\"><value name=\"NUM1\"><shadow type=\"math_number\" id=\".?}n}J;5P/OOI$54LBQ(\"><field name=\"NUM\">1</field></shadow></value><value name=\"NUM2\"><shadow type=\"math_number\" id=\"#DqugWjzgizr;L=@1W64\"><field name=\"NUM\">2</field></shadow></value></block>");
		TransformRes<AssignStmt> assignStmtCreateRes = astBuilder.createAssignment((VarDecl) varDeclareRes.getNode(),
				(Expr) opExpr, "0");
		System.out.println(getAsXmlString((ScratchBlock) assignStmtCreateRes.getNode()));
	}

	public String getAsXmlString(ScratchBlock block) {
		String xmlString = new ScratchBlockAstConvert(block).toXml().mkString();
		xmlString = xmlString.replaceAll("\"", "\'");
		return "<xml>" + xmlString;
	}

	@Test
	public void testCreateProcDecl() {
		String procCode = "test %s";
		List<ParamSpec> paramSpecs = Lists.newArrayList(new ParamSpec("a", "id0", false));
		BlockSeq body = new BlockSeq();
		ProcDecl procDecl = new ASTBuilder.ProcDeclBuilder(procCode, "procdefID").setParamDeclListFromSpecs(paramSpecs)
				.addBody(body).build();
		ProcDefEl procDefEl = new ProcDeclConvert(procDecl).toProcDefEl();
		String xml = procDefEl.toXml().mkString();
		System.out.println(xml);
	}

	@Test
	public void testCreateAssertProcDeclSig() {
		String procCode = "assertEquals %s %s";
		List<ParamSpec> paramSpecs = Lists.newArrayList(new ParamSpec("var", "varId", false),
				new ParamSpec("expr", "exprRootId", false));
		BlockSeq body = new BlockSeq();
		ProcDecl procDecl = new ASTBuilder.ProcDeclBuilder(procCode, "assertEqualID")
				.setParamDeclListFromSpecs(paramSpecs).addBody(body).build();
		ProcDefEl procDefEl = new ProcDeclConvert(procDecl).toProcDefEl();
		String xml = procDefEl.toXml().mkString();
		System.out.println(xml);
	}

	@Test
	public void testCreateParamAcc() {
		String xmlStr = "<block type='procedures_definition' id='procdefID' x='95' y='254'><statement name='custom_block'><shadow type='procedures_prototype' id='random'><mutation proccode='test %s' argumentids='[&quot;id0&quot;]' argumentnames='[&quot;a&quot;]' argumentdefaults='[&quot;&quot;]' warp='false'></mutation><value name='id0'><shadow type='argument_reporter_string_number' id='9$-GmJQ`LJ~{V0a%+y~D'><field name='VALUE'>a</field></shadow></value></shadow></statement><next><block type='motion_movesteps' id='BIC+nuQ9epcPUQ^BmxZ:'><value name='STEPS'><shadow type='math_number' id='3S}=RZL)BIwz$4,j|!Zo'><field name='NUM'>10</field></shadow><block type='argument_reporter_string_number' id=';[Ew/MUSs3GDfcbi^b|k'><field name='VALUE'>a</field></block></value></block></next></block>";
		ProcDefEl procDef = new Sb3XmlParser().parseProcDef(scala.xml.XML.loadString(xmlStr));
		VarAccess varAcc = astBuilder.createParamAccess(new ParamSpec("param_name", null, false));
		assertThat(varAcc.getType(), is("argument_reporter_string_number"));
		assertThat(getAsXmlString(varAcc), containsString("param_name"));
	}

	@Test
	public void testCreateStmtBlock() {
		List<ArgBuildSpec> argSpecs = Lists.newArrayList(new ArgBuildSpec(null, "5"));
		ExprStmt stmt = astBuilder.createStmt("motion_movesteps", null, argSpecs);
		System.out.println(getAsXmlString(stmt));
	}

	@Test
	public void testCreateExprBlock() {
		List<ArgBuildSpec> argSpecs = Lists.newArrayList(new ArgBuildSpec(null, "world"));
		OperatorExpr expr = astBuilder.createExpr("operator_length", null, argSpecs);
		System.out.println(getAsXmlString(expr));
	}

	@Test
	public void testCreateAttrReadExprBlock() {
		OperatorExpr res = (OperatorExpr) astBuilder.createAttrReadOpExpr("xpos", "sprite1").get();
		assertThat(res.getOpcode(), is("sensing_of"));
		assertThat(res.getArg(0).toDevString(), containsString("xpos"));
		assertThat(res.getArg(1).toDevString(), containsString("sprite1"));
	}

	@Test
	public void testCreateAssertionProcDecl() {
		String procCode = "assertEquals %s %s";
		List<ParamSpec> paramSpecs = Lists.newArrayList(new ParamSpec("var", "varId", false),
				new ParamSpec("expr", "exprRootId", false));
		BlockSeq body = new BlockSeq();

		VarAccess varAcc = astBuilder.createParamAccess(paramSpecs.get(0));
		List<ArgBuildSpec> argSpecs = Lists.newArrayList(new ArgBuildSpec(varAcc, "5"));
		ExprStmt stmt = astBuilder.createStmt("motion_movesteps", null, argSpecs);
		body.addStmt(stmt);

		ProcDecl procDecl = new ASTBuilder.ProcDeclBuilder(procCode, "assertEqualID")
				.setParamDeclListFromSpecs(paramSpecs).addBody(body).build();

		ProcDefEl procDefEl = new ProcDeclConvert(procDecl).toProcDefEl();
		String xml = procDefEl.toXml().mkString();
		System.out.println(xml);
	}

	public ProcDecl getTestProcDecl() {
		String xmlStr = "<block type='procedures_definition' id='procdefID' x='95' y='254'><statement name='custom_block'><shadow type='procedures_prototype' id='random'><mutation proccode='test %s' argumentids='[&quot;id0&quot;]' argumentnames='[&quot;a&quot;]' argumentdefaults='[&quot;&quot;]' warp='false'></mutation><value name='id0'><shadow type='argument_reporter_string_number' id='9$-GmJQ`LJ~{V0a%+y~D'><field name='VALUE'>a</field></shadow></value></shadow></statement><next><block type='motion_movesteps' id='BIC+nuQ9epcPUQ^BmxZ:'><value name='STEPS'><shadow type='math_number' id='3S}=RZL)BIwz$4,j|!Zo'><field name='NUM'>10</field></shadow><block type='argument_reporter_string_number' id=';[Ew/MUSs3GDfcbi^b|k'><field name='VALUE'>a</field></block></value></block></next></block>";
		ProcDefEl procDef = new Sb3XmlParser().parseProcDef(scala.xml.XML.loadString(xmlStr));
		return procDef.toProcDecl();
	}

	@Test
	public void testCreateProcAccess() {
		ProcDecl procDecl = getTestProcDecl();
		List<ArgBuildSpec> argSpecs = Lists.newArrayList(new ArgBuildSpec(null, "world"));
		OperatorExpr expr = astBuilder.createExpr("operator_length", null, argSpecs);

		Map<String, ArgBuildSpec> id2ArgSpec = ImmutableMap.<String, ArgBuildSpec>builder()
				.put(procDecl.getMeta().getArgIds().get(0), new ArgBuildSpec(expr, "5")).build();
		TransformRes<ProcAccess> result = new ASTBuilder.ProcAccessBuilder(procDecl, "callblock_id")
				.arguments(id2ArgSpec).build();
		System.out.println(getAsXmlString((ScratchBlock) result.getNode()));
	}

	@Test
	public void testCreateIfStmt() {
		List<ArgBuildSpec> argSpecs = Lists.newArrayList(new ArgBuildSpec(null, "1"), new ArgBuildSpec(null, "2"));
		OperatorExpr boolExpr = astBuilder.createExpr("operator_equals", null, argSpecs);

		TransformRes<VarDecl> varDeclareRes = transformer.declareVar(null, "temp", "temp_id");
		TransformRes<IncrementStmt> incrStmtCreateRes = astBuilder
				.createIncrementStmt((VarDecl) varDeclareRes.getNode(), "id");
		BlockSeq then = new BlockSeq();
		then.addStmt((Stmt) incrStmtCreateRes.getNode());
		IfElseStmt ifstmt = astBuilder.createIfStmt(boolExpr, then);
		System.out.println(getAsXmlString(ifstmt));
	}

	@Test
	public void testCreateInvariantCheckProcDecl() {
		String watchedId = "__inv_failure_counter_";
		TransformRes<VarDecl> varDeclareRes = new TransformerHelper().declareVar(null, "__inv_failure_count",
				"__inv_failure_count");
		TransformRes<ProcDecl> invariantProcDeclCreateRes = astBuilder
				.createInvariantCheckDef((VarDecl) varDeclareRes.getNode(), watchedId);
		ProcDefEl procDefEl = new ProcDeclConvert((ProcDecl) invariantProcDeclCreateRes.getNode()).toProcDefEl();
		String xml = "<xml>" + procDefEl.toXml().mkString();
		System.out.println(xml);
	}
}
