package anlys.refact;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

import org.junit.Before;
import org.junit.Test;

import com.fasterxml.jackson.core.JsonProcessingException;

import anlys.smell.DuplicateCode;
import anlys.smell.DuplicateCodeAnalyzer;
import anlys.smell.SmellListResult;
import ast.Program;
import lombok.extern.slf4j.Slf4j;
import sb3.parser.xml.Sb3XmlParser;
import util.UtilFunc;

@Slf4j
public class DuplicateCodeAnalyzerTest {

	private Program program;

	@Before
	public void setup() {
		String xmlStr = "<program><stage name='Stage'><xml><variables><variable type='' id='`jEk@4|i[#Fk?(8x)AV.-my variable' islocal='false'>my variable</variable></variables></xml></stage><sprite name='Sprite1'><xml><variables></variables><block id='7rvb18WS*GeHbR]cd)FM' type='event_whenflagclicked' x='522' y='-114' ><next><block id='_9hkX0PowA+7V^W8)E$9' type='control_forever' ><value name='SUBSTACK'><block id='mZSmzYKe+?j)pWls,0]#' type='control_repeat' ><value name='TIMES'><shadow id='1#iNOUv3+DeXvlY}Lnj2' type='math_whole_number' ><field name='NUM'>25</field></shadow></value><value name='SUBSTACK'><block id='5u`vxheM9NcLQlM?:B5?' type='motion_movesteps' ><value name='STEPS'><shadow id='%3QbhFGa+Zt)jM@/4{:O' type='math_number' ><field name='NUM'>15</field></shadow></value><next><block id='!mSOKxM`/3Sk%kt|p]u]' type='motion_turnright' ><value name='DEGREES'><shadow id='C6pjAhyQWGgk(5sDjhF*' type='math_number' ><field name='NUM'>15</field></shadow></value><next><block id='B5~yXc[D]3C7aWTO^@Cu' type='looks_nextcostume' ></block></next></block></next></block></value><next><block id='$El6Yl$G}n(0x]|+uND|' type='control_repeat' ><value name='TIMES'><shadow id='}oID`z{ASrs-{J0@|Ev6' type='math_whole_number' ><field name='NUM'>25</field></shadow></value><value name='SUBSTACK'><block id='l5-ZUZ4ZVI]Qtc+1rFA$' type='motion_movesteps' ><value name='STEPS'><shadow id='y=z/3md.K@+EkO8Bw|JW' type='math_number' ><field name='NUM'>15</field></shadow></value><next><block id='sy1Yow+VGadSJsh+,n3:' type='motion_turnright' ><value name='DEGREES'><shadow id='c|Z@ZiBvmp+0{Brg9Z!+' type='math_number' ><field name='NUM'>-15</field></shadow></value><next><block id='.kOE`wBs(Q:@Y)kt6@7]' type='looks_nextcostume' ></block></next></block></next></block></value><next><block id='fIT`Z8M;Ib~!6h4)-Ey.' type='motion_gotoxy' ><value name='X'><shadow id='E-iKB__FCus:/._)1BR#' type='math_number' ><field name='NUM'>0</field></shadow></value><value name='Y'><shadow id='Zq]U2Hfag%Z+%3V]LEA]' type='math_number' ><field name='NUM'>0</field></shadow></value></block></next></block></next></block></value></block></next></block></xml></sprite></program>";
		program = new Sb3XmlParser().parseProgram(xmlStr);
	}
	
	@Test
	public void testSmellAnalysis() throws JsonProcessingException {
		DuplicateCodeAnalyzer analyzer = new DuplicateCodeAnalyzer();
		SmellListResult<DuplicateCode> results = analyzer.analyze(program);
		assertThat(results.size(), equalTo(1));
		log.info(results.get(0).toPrettyJson());
	}

	@Test
	public void testSmellInstanceToAndFromJson() throws Exception {
		String jsonStr = UtilFunc.readStringFromResource("DuplicateCode-test.json");
		DuplicateCode instance = new DuplicateCode().fromJson(jsonStr, program);

		assertThat(instance.getTarget(), equalTo("Sprite1"));
		assertThat(instance.getFragments().size(), equalTo(2));
		assertThat(instance.getSmellId(), is(notNullValue()));
		assertThat(instance.getMetadata(), is(notNullValue()));
		assertThat(jsonStr, equalTo(instance.toPrettyJson()));
	}

	

}
