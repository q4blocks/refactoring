package anlys.refact;

import static anlys.ActionsMatcher.matchesActions;
import static org.junit.Assert.assertThat;
import static org.hamcrest.CoreMatchers.equalTo;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import anlys.TestUtil;
import anlys.refact.transform.action.ActionList;
import anlys.smell.DuplicateCode;
import anlys.smell.DuplicateCodeAnalyzer;
import anlys.smell.SmellListResult;
import ast.Program;
import ast.Sprite;
import lombok.extern.slf4j.Slf4j;
import sb3.parser.xml.Sb3XmlParser;
import util.CreatedEntityCounter;
import util.ProgramFactory;
import util.UtilFunc;

@Slf4j
public class ExtractProcedureTest {
	@Before
	public void setup() {
		CreatedEntityCounter.reset();
	}

	/**
	 * fix bug number of attributes '1' but was '2' (how come the shadow has id)
	 * <xml><block type='procedures_call' id='call_DoSomething1%s_1'><mutation proccode='DoSomething1 %s' argumentids='[&quot;param0_ID&quot;]' warp='null'/><value name='param0_ID'><shadow type='text'><field name='TEXT'>15</field></shadow></value></block></xml>
	 * <xml><block type='procedures_call' id='DoSomething1%s_Call1'><mutation proccode='DoSomething1 %s' argumentids='[&quot;param0_ID&quot;]' warp='null'/><value name='param0_ID'><shadow type='text' id='DoSomething1%s_Call1_param_0'><field name='TEXT'>15</field></shadow></value></block></xml>
	 */
	
	/**
	 * Expect 1 extract procedure request Proc(param1):
	 * repeat {movesteps,turnright(param1), nextcostume}
	 * 
	 * @throws Exception
	 */
	@Ignore
	@Test
	public void testExtractProc() throws Exception {
		String xmlStr = "<program><stage name='Stage'><xml><variables><variable type='' id='`jEk@4|i[#Fk?(8x)AV.-my variable' islocal='false'>my variable</variable></variables></xml></stage><sprite name='Sprite1'><xml><variables></variables><block id='7rvb18WS*GeHbR]cd)FM' type='event_whenflagclicked' x='522' y='-114' ><next><block id='_9hkX0PowA+7V^W8)E$9' type='control_forever' ><value name='SUBSTACK'><block id='mZSmzYKe+?j)pWls,0]#' type='control_repeat' ><value name='TIMES'><shadow id='1#iNOUv3+DeXvlY}Lnj2' type='math_whole_number' ><field name='NUM'>25</field></shadow></value><value name='SUBSTACK'><block id='5u`vxheM9NcLQlM?:B5?' type='motion_movesteps' ><value name='STEPS'><shadow id='%3QbhFGa+Zt)jM@/4{:O' type='math_number' ><field name='NUM'>15</field></shadow></value><next><block id='!mSOKxM`/3Sk%kt|p]u]' type='motion_turnright' ><value name='DEGREES'><shadow id='C6pjAhyQWGgk(5sDjhF*' type='math_number' ><field name='NUM'>15</field></shadow></value><next><block id='B5~yXc[D]3C7aWTO^@Cu' type='looks_nextcostume' ></block></next></block></next></block></value><next><block id='$El6Yl$G}n(0x]|+uND|' type='control_repeat' ><value name='TIMES'><shadow id='}oID`z{ASrs-{J0@|Ev6' type='math_whole_number' ><field name='NUM'>25</field></shadow></value><value name='SUBSTACK'><block id='l5-ZUZ4ZVI]Qtc+1rFA$' type='motion_movesteps' ><value name='STEPS'><shadow id='y=z/3md.K@+EkO8Bw|JW' type='math_number' ><field name='NUM'>15</field></shadow></value><next><block id='sy1Yow+VGadSJsh+,n3:' type='motion_turnright' ><value name='DEGREES'><shadow id='c|Z@ZiBvmp+0{Brg9Z!+' type='math_number' ><field name='NUM'>-15</field></shadow></value><next><block id='.kOE`wBs(Q:@Y)kt6@7]' type='looks_nextcostume' ></block></next></block></next></block></value><next><block id='fIT`Z8M;Ib~!6h4)-Ey.' type='motion_gotoxy' ><value name='X'><shadow id='E-iKB__FCus:/._)1BR#' type='math_number' ><field name='NUM'>0</field></shadow></value><value name='Y'><shadow id='Zq]U2Hfag%Z+%3V]LEA]' type='math_number' ><field name='NUM'>0</field></shadow></value></block></next></block></next></block></value></block></next></block></xml></sprite></program>";
		Program program = new Sb3XmlParser().parseProgram(xmlStr);
		String jsonStr = UtilFunc.readStringFromResource("DuplicateCode-test.json");
		log.info(jsonStr);
		DuplicateCode instance = new DuplicateCode().fromJson(jsonStr, program);
		ExtractProcedureReq req = ExtractProcedureReq.from(instance, program);
		String refactJson = UtilFunc.readStringFromResource("extract_procedure-RefactResult.json");
		RefactResult expected = RefactResult.fromJson(refactJson);
		RefactResult actual = new ExtractProcedure().analyze(program, req);
		System.out.println(actual.toPrettyJson());
		assertThat(actual.getActions(), matchesActions(expected.getActions()));
	}

	@Test
	public void shouldExhaustivelyApply() throws Exception {
		String xmlStr = "<xml xmlns='http://www.w3.org/1999/xhtml'><variables></variables><block type='event_whenflagclicked' id='O]+(mWYA%ab(PGl*,o{g' x='521' y='107'><next><block type='motion_movesteps' id='Df;@h_U92Y]4.PJ@27J0'><value name='STEPS'><shadow type='math_number' id='Hgk7#IhCf7sF71@?YR0t'><field name='NUM'>10</field></shadow></value><next><block type='motion_turnright' id='nJlf*jVhTMVrx?p^s{b('><value name='DEGREES'><shadow type='math_number' id='ld?kg,k#R]8ExVrUr,x`'><field name='NUM'>15</field></shadow></value><next><block type='motion_movesteps' id=')Jme*bPC_n[DHQsED56@'><value name='STEPS'><shadow type='math_number' id=']Ghg}Y^PSsPFI[MZtzaL'><field name='NUM'>10</field></shadow></value><next><block type='motion_turnright' id='N|#-wv|RniuZVzj?!Fm1'><value name='DEGREES'><shadow type='math_number' id='hjyS1t1![-Cz/zf,Mk_N'><field name='NUM'>15</field></shadow></value></block></next></block></next></block></next></block></next></block><block type='event_whenthisspriteclicked' id='[vkfh*!{W`y1/u:_zeOA' x='771' y='116'><next><block type='motion_movesteps' id='){8gd23_+BB6sLcvZ$v^'><value name='STEPS'><shadow type='math_number' id='0e)w.pK[IKb1Y]G{Mw~.'><field name='NUM'>10</field></shadow></value><next><block type='motion_turnright' id='WhzbUNmEjaH{LYTDROL~'><value name='DEGREES'><shadow type='math_number' id='SSY6lo0xB/fiL`2hLN?/'><field name='NUM'>15</field></shadow></value><next><block type='motion_movesteps' id='E|gx/P1,#mPzXZI@/6F$'><value name='STEPS'><shadow type='math_number' id='Xe2=(][x)fKg5v^fyaLE'><field name='NUM'>10</field></shadow></value><next><block type='motion_turnright' id='(3$G({x?M8:SJ@hHFZSH'><value name='DEGREES'><shadow type='math_number' id='EijWLCE_j#RR^$.#L`)z'><field name='NUM'>15</field></shadow></value></block></next></block></next></block></next></block></next></block></xml>";
		Sprite target = new Sb3XmlParser().parseTarget(xmlStr);
		Program program = ProgramFactory.programOf(target);
		DuplicateCodeAnalyzer smellAnalyzer = new DuplicateCodeAnalyzer();
		smellAnalyzer.setGroupSize(2);
		smellAnalyzer.setMinSubseqSize(3);

		new MultiPassRefactorer(program).run(smellAnalyzer, new ExtractProcedure());
		String control = UtilFunc.readStringFromResource("extract_procedure-repeated-application-after.xml");
		String test = UtilFunc.xmlFromProgram(program);
		TestUtil.assertSimilarBlockXml(control, test);
		TestUtil.assertUniqueId(control, test);
	}
	
	@Test
	public void shouldDecline() throws Exception {
		String xmlStr = "<xml xmlns='http://www.w3.org/1999/xhtml'><variables></variables><block type='event_whenflagclicked' id='WTkomJRw!r:#xuKpN4Vb' x='321' y='163'><next><block type='control_if' id=':g;Q7-}(_]uPbIEEmgV5'><value name='CONDITION'><block type='sensing_touchingcolor' id='od(3u~xfbYVx/WdR:LXO'><value name='COLOR'><shadow type='colour_picker' id='1m?2B%P**Zdl=gM1m0zZ'><field name='COLOUR'>#ae68f8</field></shadow></value></block></value><statement name='SUBSTACK'><block type='motion_movesteps' id='fnRY5c6FE}RHuoZ;b^+O'><value name='STEPS'><shadow type='math_number' id='VWcXtZ=_(HWlpni1fUBb'><field name='NUM'>10</field></shadow></value><next><block type='motion_turnright' id='rO**uDrGjE!/HVAH.K=b'><value name='DEGREES'><shadow type='math_number' id='$dT/C[u-gNbdjLRiDpzn'><field name='NUM'>15</field></shadow></value></block></next></block></statement><next><block type='control_if' id=')6g6/=HBx8Vhk{.pNG)8'><value name='CONDITION'><block type='sensing_touchingcolor' id='v0kI:1?$oV]coX:7#*au'><value name='COLOR'><shadow type='colour_picker' id='{11-pV|PW2-s?TdAr^4J'><field name='COLOUR'>#f86666</field></shadow></value></block></value><statement name='SUBSTACK'><block type='motion_movesteps' id='JRE(?(SG=4*9vUXCyDp8'><value name='STEPS'><shadow type='math_number' id='w1_H$[0bZG.doen?+Z}t'><field name='NUM'>10</field></shadow></value><next><block type='motion_turnright' id='VU:T0uSC}sPYd)Mfw,2N'><value name='DEGREES'><shadow type='math_number' id='_FfqsKQyn!a[*w3U9m%P'><field name='NUM'>15</field></shadow></value></block></next></block></statement></block></next></block></next></block></xml>";
		Sprite target = new Sb3XmlParser().parseTarget(xmlStr);
		Program program = ProgramFactory.programOf(target);
		DuplicateCodeAnalyzer smellAnalyzer = new DuplicateCodeAnalyzer();
		smellAnalyzer.setGroupSize(2);
		smellAnalyzer.setMinSubseqSize(3);
		SmellListResult<DuplicateCode> res = smellAnalyzer.analyze(program);
		assertThat(res.size(), equalTo(0));
	}
	

}
