package anlys.refact;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import org.junit.Before;
import org.junit.Test;

import com.google.common.collect.Lists;

import ast.CFGNode;
import ast.IfElseStmt;
import ast.LoopStmt;
import ast.Program;
import ast.SmallSet;
import ast.Sprite;
import sb3.dsl.SimXml2AstParser;
import util.ProgramFactory;
import util.UtilFunc;

public class GetNextStmtCfgTest {
	SimXml2AstParser parser;

	@Before
	public void setup() {
		parser = new SimXml2AstParser();
	}

	@Test
	public void nextStmtCFGForIfElseStmt() {
		String xml = "<sprite><vars><vardecl id='a'/></vars><script>"
				+ "<ifelse id='ifelse0'><cond><str>true</str></cond>" + "<then>"
				+ "<stmt id='0'><varacc id='Ra0' vid='a'/></stmt>" + "</then>" + "</ifelse>"
				+ "<stmt id='1'><varacc id='Ra2' vid='a'/></stmt>" + "</script></sprite>";
		Sprite sprite = (Sprite) parser.parse(xml);
		Program program = ProgramFactory.programOf(sprite);
		System.out.println(program.toDevString());
		IfElseStmt ifelse = (IfElseStmt) program.getBlock("ifelse0");
		System.out.println(ifelse.nextStmtCFG());
		SmallSet<CFGNode> expected = SmallSet.mutable();
		Lists.newArrayList(program.getBlock("0"), program.getBlock("1")).stream()
				.forEach(stmt -> expected.add((CFGNode) stmt));
		assertThat(ifelse.nextStmtCFG(), is(expected));
	}

	@Test
	public void nextStmtCFGForIfElseStmt2() {
		String xml = "<sprite><vars><vardecl id='a'/></vars><script>"
				+ "<ifelse id='ifelse0'><cond><str>true</str></cond>" + "<then>"
				+ "<stmt id='0'><varacc id='Ra0' vid='a'/></stmt>" + "</then>" + "<else>"
				+ "<stmt id='1'><varacc id='Ra0' vid='a'/></stmt>" + "</else>" + "</ifelse>"
				+ "<stmt id='2'><varacc id='Ra0' vid='a'/></stmt>" + "</script></sprite>";
		Sprite sprite = (Sprite) parser.parse(xml);
		Program program = ProgramFactory.programOf(sprite);
		System.out.println(program.toDevString());
		IfElseStmt ifelse = (IfElseStmt) program.getBlock("ifelse0");
		System.out.println(ifelse.nextStmtCFG());
		SmallSet<CFGNode> expected = SmallSet.mutable();
		Lists.newArrayList(program.getBlock("0"), program.getBlock("1"), program.getBlock("2")).stream()
				.forEach(stmt -> expected.add((CFGNode) stmt));
		assertThat(ifelse.nextStmtCFG(), is(expected));
	}

	@Test
	public void nextStmtCFGForLoopStmt() {
		String xml = "<sprite><vars><vardecl id='a'/></vars><script>" + "<loop id='loop0'><cond><str>true</str></cond>"
				+ "<body>" + "<stmt id='0'><varacc id='Ra0' vid='a'/></stmt>" + "</body>" + "</loop>"
				+ "<stmt id='1'><varacc id='Ra0' vid='a'/></stmt>" + "</script></sprite>";
		Sprite sprite = (Sprite) parser.parse(xml);
		Program program = ProgramFactory.programOf(sprite);
		System.out.println(program.toDevString());
		LoopStmt loopStmt = (LoopStmt) program.getBlock("loop0");
		System.out.println(loopStmt.nextStmtCFG());
		SmallSet<CFGNode> expected = SmallSet.mutable();
		Lists.newArrayList(program.getBlock("0"), program.getBlock("1")).stream()
				.forEach(stmt -> expected.add((CFGNode) stmt));
		assertThat(loopStmt.nextStmtCFG(), is(expected));
	}

}
