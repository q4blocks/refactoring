package anlys.refact;

import org.junit.Before;
import org.junit.Test;

import anlys.TestUtil;
import anlys.smell.DuplicateCodeAnalyzer;
import ast.Program;
import ast.Sprite;
import lombok.extern.slf4j.Slf4j;
import sb3.parser.xml.Sb3XmlParser;
import util.CreatedEntityCounter;
import util.UtilFunc;

@Slf4j
public class RefactManagerTest {
	@Before
	public void setup() {
		CreatedEntityCounter.reset();
	}

}
