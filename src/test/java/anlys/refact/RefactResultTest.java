package anlys.refact;

import static anlys.ActionMatcher.similarActionTo;
import static anlys.ActionsMatcher.matchesActions;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.junit.Assert.assertThat;

import org.junit.Ignore;
import org.junit.Test;

import com.google.common.collect.Lists;

import anlys.TestUtil;
import anlys.refact.transform.action.ActionList;
import anlys.refact.transform.action.Create;
import anlys.refact.transform.action.Insert;
import anlys.refact.transform.action.ReplaceSeq;
import anlys.refact.transform.action.VarDeclare;
import anlys.refact.transform.action.VarRename;

public class RefactResultTest {

	@Test
	public void blockXmlComparisonShouldIgnoreId() {
		String xml1 = "<block type='motion_movesteps' id='id1' x='526' y='496'><value name='STEPS'><shadow type='math_number' id='id3'><field name='NUM'>10</field></shadow></value></block>";
		String xml2 = "<block type='motion_movesteps' id='id2' x='525' y='496'><value name='STEPS'><shadow type='math_number' id='id4'><field name='NUM'>10</field></shadow></value></block>";
		TestUtil.assertSimilarBlockXml(xml1, xml2);
	}

	@Ignore
	@Test
	public void testActionMatching() {
		// TODO: use samePropertyValuesAs (import static
		// org.hamcrest.beans.SamePropertyValuesAs.*;)
		assertThat(new VarDeclare("varName", "id1", "sprite1"),
				is(similarActionTo(new VarDeclare("varName", "id1", "sprite1"))));
		assertThat(new VarDeclare("actual", "id1", "sprite1"),
				not(similarActionTo(new VarDeclare("expected", "id1", "sprite1"))));

		assertThat(new Insert("id1", "id2", true), is(similarActionTo(new Insert("id1", "id2", true))));
		// inserted block can be different id
		assertThat(new Insert("?", "id2", true), is(similarActionTo(new Insert("id1", "id2", true))));

		assertThat(new Insert("id1", "id2", false), not(similarActionTo(new Insert("id1", "id2", true))));

		assertThat(new VarRename("oldName", "newName", "id1", "target"),
				is(similarActionTo(new VarRename("oldName", "newName", "id1", "target"))));
		assertThat(new VarRename("oldName", "newName", "id1","target"),
				not(similarActionTo(new VarRename("oldName", "?", "id1","target"))));

		assertThat(new ReplaceSeq(Lists.newArrayList("stmt0", "stmt1"), "stmt2"),
				is(similarActionTo(new ReplaceSeq(Lists.newArrayList("stmt0", "stmt1"), "stmt2"))));
		assertThat(new ReplaceSeq(Lists.newArrayList("stmt0", "stmt3"), "stmt2"),
				not(similarActionTo(new ReplaceSeq(Lists.newArrayList("stmt0", "stmt1"), "stmt2"))));

		Create create1 = new Create(
				"<block type='motion_movesteps' id='id1' x='526' y='496'><value name='STEPS'><shadow type='math_number' id='id3'><field name='NUM'>10</field></shadow></value></block>");
		Create create2 = new Create(
				"<block type='motion_movesteps' id='id2' x='525' y='496'><value name='STEPS'><shadow type='math_number' id='id4'><field name='NUM'>10</field></shadow></value></block>");
		assertThat(create1, similarActionTo(create2));

		Create create3 = new Create(
				"<block type='turn' id='id1' x='526' y='496'><value name='STEPS'><shadow type='math_number' id='id3'><field name='NUM'>10</field></shadow></value></block>");
		Create create4 = new Create(
				"<block type='motion_movesteps' id='id2' x='525' y='496'><value name='STEPS'><shadow type='math_number' id='id4'><field name='NUM'>10</field></shadow></value></block>");
//		assertThat(create3, not(similarActionTo(create4)));
	}

	@Test
	public void createActionComparisonShouldIgnoreId() {
		RefactResult expected = new RefactResult();
		String xml1 = "<block type='motion_movestep' id='id1' x='526' y='496'><value name='STEPS'><shadow type='math_number' id='id3'><field name='NUM'>10</field></shadow></value></block>";
		expected.setActions(
				new ActionList().addAction(new Create(xml1)).addAction(new VarDeclare("var1", "id1", "_stage_")));
		RefactResult actual = new RefactResult();
		String xml2 = "<block type='motion_movestep' id='id2' x='525' y='496'><value name='STEPS'><shadow type='math_number' id='id4'><field name='NUM'>10</field></shadow></value></block>";
		actual.setActions(
				new ActionList().addAction(new Create(xml2)).addAction(new VarDeclare("var1", "id1", "_stage_")));
		assertThat(actual.getActions(), matchesActions(expected.getActions()));
	}

}
