package anlys.refact.precond;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

import java.util.List;
import java.util.stream.Collectors;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.google.common.collect.Lists;

import anlys.refact.transform.TransformerHelper;
import ast.AssignStmt;
import ast.Program;
import ast.Sprite;
import ast.Stmt;
import ast.VarAccess;
import sb3.dsl.SimXml2AstParser;
import util.ProgramFactory;
import util.UtilFunc;

public class ReachingDefTest {
	SimXml2AstParser parser;

	@Before
	public void setup() {
		parser = new SimXml2AstParser();

	}

	@Test
	public void testTopLevelStmt() {
		String inputXml = "<sprite><vars><vardecl id='a'/></vars><script>"
				+ "<ifelse id='ifelse0'><cond><str>true</str></cond>" + "<then>"
				+ "<stmt id='0'><varacc id='Ra0' vid='a'/></stmt>" + "<ifelse id='ifelse1'><cond><str>true</str></cond>"
				+ "<then>" + "<stmt id='1'><varacc id='Ra1' vid='a'/></stmt>" + "</then>" + "</ifelse>" + "</then>"
				+ "</ifelse>" + "<stmt id='2'><varacc id='Ra2' vid='a'/></stmt>" + "</script></sprite>";
		Sprite sprite = (Sprite) parser.parse(inputXml);
		Program program = ProgramFactory.programOf(sprite);
		assertThat(program.lookupStmt("0").topLevelStmt(), equalTo(program.lookupStmt("ifelse0")));
		assertThat(program.lookupStmt("1").topLevelStmt(), equalTo(program.lookupStmt("ifelse0")));
		assertThat(program.lookupStmt("2").topLevelStmt(), equalTo(program.lookupStmt("2")));

		// testGetStmtIndexOfCurSeq
		assertThat(program.lookupStmt("0").getIndexInCurSeq(), equalTo(0));
		assertThat(program.lookupStmt("1").getIndexInCurSeq(), equalTo(0));
		assertThat(program.lookupStmt("2").getIndexInCurSeq(), equalTo(1));

		assertThat(program.lookupStmt("ifelse0").getIndexInCurSeq(), equalTo(0));
		assertThat(program.lookupStmt("ifelse1").getIndexInCurSeq(), equalTo(1));

		String assignStmtXml = "<assign id='A1'><dest><varacc id='Wa1' vid='a'/></dest><src><num>1</num></src></assign>";
		AssignStmt assign = (AssignStmt) parser.parse(assignStmtXml);
		new TransformerHelper().insertBefore(assign, program.lookupStmt("0"));
		System.out.println(program.toDevString());
	}

	@Test
	public void testMoveUpToParentSeq() {
		String inputXml = "<sprite><vars><vardecl id='a'/></vars><script>"
				+ "<ifelse id='ifelse0'><cond><str>true</str></cond>" + "<then>"
				+ "<stmt id='0'><varacc id='Ra0' vid='a'/></stmt>" + "<ifelse id='ifelse1'><cond><str>true</str></cond>"
				+ "<then>" + "<stmt id='1'><varacc id='Ra1' vid='a'/></stmt>" + "</then>" + "</ifelse>" + "</then>"
				+ "</ifelse>" + "<stmt id='2'><varacc id='Ra2' vid='a'/></stmt>" + "</script></sprite>";
		Sprite sprite = (Sprite) parser.parse(inputXml);
		Program program = ProgramFactory.programOf(sprite);
		System.out.println("before\n" + program.toDevString());
		Stmt moveUpStmt = program.lookupStmt("1");
		Stmt moveUpTarget = new TransformerHelper().getMoveUpBeforeTarget(moveUpStmt);
		while (moveUpTarget != null) {
			new TransformerHelper().moveUp(moveUpStmt);
			System.out.println("after\n" + program.toDevString());
			moveUpTarget = new TransformerHelper().getMoveUpBeforeTarget(moveUpStmt);
		}
	}

	@Ignore
	@Test
	public void testDetermineAssignmentInsertLoc() {
		String inputXml = "<sprite><vars><vardecl id='a'/></vars><script>" + "<stmt id='a'></stmt>"
				+ "<ifelse id='ifelse0'><cond><str>true</str></cond>" + "<then>"
				+ "<stmt id='0'><varacc id='Ra0' vid='a'/></stmt>" + "<ifelse id='ifelse1'><cond><str>true</str></cond>"
				+ "<then>" + "<stmt id='1'><varacc id='Ra1' vid='a'/></stmt>" + "</then>" + "</ifelse>" + "</then>"
				+ "</ifelse>" + "<stmt id='2'><varacc id='Ra2' vid='a'/></stmt>" + "</script></sprite>";

		Sprite sprite = (Sprite) parser.parse(inputXml);
		Program program = ProgramFactory.programOf(sprite);
		String assignStmtXml = "<assign id='A1'><dest><varacc id='Wa1' vid='a'/></dest><src><num>1</num></src></assign>";
		AssignStmt assignStmt = (AssignStmt) parser.parse(assignStmtXml);
		Stmt initialTarget = program.lookupStmt("1");
		new TransformerHelper().insertBefore(assignStmt, initialTarget);

		DefPreservAnalysis analysis = new DefPreservAnalysis(program);

		List<VarAccess> varReadAccs = Lists.newArrayList("Ra0", "Ra1", "Ra2").stream()
				.map(vaccId -> program.lookupBlock(vaccId).get()).map(expr -> (VarAccess) expr)
				.collect(Collectors.toList());
		Stmt targetId = analysis.determineAssignmentInsertTarget(assignStmt, varReadAccs);
		System.out.println("insert before:" + targetId.toDevString());
	}
}
