package anlys.refact.transform;

import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;
import java.util.Map;

import org.junit.Test;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;

import anlys.refact.transform.ASTBuilder;
import anlys.refact.transform.ArgBuildSpec;
import anlys.refact.transform.ParamSpec;
import anlys.refact.transform.TransformException;
import anlys.refact.transform.action.TransformRes;
import anlys.refact.transform.strategy.CopyStrategy;
import ast.BlockSeq;
import ast.OperatorExpr;
import ast.ProcAccess;
import ast.ProcDecl;
import lombok.extern.slf4j.Slf4j;
import sb3.parser.ProcDeclConvert;
import sb3.parser.ProcDefEl;
import sb3.parser.xml.Sb3XmlParser;
import util.UtilFunc;

@Slf4j
public class AstBuilderProcDeclBuilderTest {

	@Test
	public void shouldMakeDeepCopyOfProcDecl() throws TransformException {
		String procCode = "test";
		List<ParamSpec> paramSpecs = Lists.newArrayList(new ParamSpec("a", "id0", false));
		BlockSeq body = new BlockSeq();
		ProcDecl procDecl = new ASTBuilder.ProcDeclBuilder(procCode, "procdefID").setParamDeclListFromSpecs(paramSpecs)
				.addBody(body).build();
		ProcDecl procDecl2 = new CopyStrategy<>(procDecl).makeCopy();
		assertTrue(procDecl2.getAttrs() != procDecl.getAttrs());
		assertTrue(procDecl2.getMeta() != procDecl.getMeta());
		assertTrue(procDecl.getParamDecl(0).getMeta() != procDecl2.getParamDecl(0).getMeta());
		log.debug(UtilFunc.xmlFromProcDecl(procDecl2));
	}

	public ProcDecl getTestProcDecl() {
		String xmlStr = "<block type='procedures_definition' id='procdefID' x='95' y='254'><statement name='custom_block'><shadow type='procedures_prototype' id='random'><mutation proccode='test %s' argumentids='[&quot;id0&quot;]' argumentnames='[&quot;a&quot;]' argumentdefaults='[&quot;&quot;]' warp='false'></mutation><value name='id0'><shadow type='argument_reporter_string_number' id='9$-GmJQ`LJ~{V0a%+y~D'><field name='VALUE'>a</field></shadow></value></shadow></statement><next><block type='motion_movesteps' id='BIC+nuQ9epcPUQ^BmxZ:'><value name='STEPS'><shadow type='math_number' id='3S}=RZL)BIwz$4,j|!Zo'><field name='NUM'>10</field></shadow><block type='argument_reporter_string_number' id=';[Ew/MUSs3GDfcbi^b|k'><field name='VALUE'>a</field></block></value></block></next></block>";
		ProcDefEl procDef = new Sb3XmlParser().parseProcDef(scala.xml.XML.loadString(xmlStr));
		return procDef.toProcDecl();
	}

	@Test
	public void shouldMakeDeepCopyOfProcAcc() throws TransformException {
		ProcDecl procDecl = getTestProcDecl();
		List<ArgBuildSpec> argSpecs = Lists.newArrayList(new ArgBuildSpec(null, "world"));
		OperatorExpr expr = new ASTBuilder("", "").createExpr("operator_length", null, argSpecs);
		Map<String, ArgBuildSpec> id2ArgSpec = ImmutableMap.<String, ArgBuildSpec>builder()
				.put(procDecl.getMeta().getArgIds().get(0), new ArgBuildSpec(expr, "5")).build();
		TransformRes<ProcAccess> result = new ASTBuilder.ProcAccessBuilder(procDecl, "callblock_id")
				.arguments(id2ArgSpec).build();

		ProcAccess procAcc = result.getNode();
		ProcAccess procAcc2 = new CopyStrategy<>(procAcc).makeCopy();
		assertNotEquals(procAcc, procAcc2);
		assertNotEquals(procAcc.getMeta(), procAcc2.getMeta());
	}

}
