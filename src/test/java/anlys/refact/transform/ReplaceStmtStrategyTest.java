package anlys.refact.transform;

import static org.junit.Assert.*;
import static org.hamcrest.Matchers.*;
import org.junit.Before;
import org.junit.Test;

import com.google.common.collect.Lists;

import anlys.refact.transform.ASTBuilder;
import anlys.refact.transform.ArgBuildSpec;
import anlys.refact.transform.Snippet;
import anlys.refact.transform.TransformException;
import anlys.refact.transform.TransformerHelper;
import ast.ExprStmt;
import ast.IfElseStmt;
import ast.Program;
import ast.ScratchBlock;
import sb3.parser.ScratchBlockAstConvert;
import sb3.parser.xml.Sb3XmlParser;

public class ReplaceStmtStrategyTest {
	private Snippet snippet;
	private ExprStmt stmt;

	@Before
	public void setup() {
		ASTBuilder astBuilder = new ASTBuilder("block#", "var#");
		snippet = new Snippet();

		ExprStmt target0 = astBuilder.createStmt("motion_movesteps", "id0",
				Lists.newArrayList(new ArgBuildSpec(null, "1")));
		snippet.addStmt(target0);

		ExprStmt target1 = astBuilder.createStmt("motion_turnright", "id1",
				Lists.newArrayList(new ArgBuildSpec(null, "2")));
		snippet.addStmt(target1);

		ExprStmt target2 = astBuilder.createStmt("motion_turnleft", "id2",
				Lists.newArrayList(new ArgBuildSpec(null, "3")));
		snippet.addStmt(target2);

		ExprStmt target3 = astBuilder.createStmt("motion_pointindirection", "id3",
				Lists.newArrayList(new ArgBuildSpec(null, "4")));
		snippet.addStmt(target3);

		stmt = astBuilder.createStmt("looks_show", "id4", Lists.newArrayList());

	}

	@Test 
	public void testReplaceTopStmts() throws TransformException {
		new TransformerHelper(snippet.get()).replaceStmt(Lists.newArrayList("id0", "id1"), stmt);
		assertThat(snippet.getBody().getIndexOfChild(stmt), equalTo(0));
	}

	@Test
	public void testReplaceMidStmts() throws TransformException {
		new TransformerHelper(snippet.get()).replaceStmt(Lists.newArrayList("id1", "id2"), stmt);
		assertThat(snippet.getBody().getIndexOfChild(stmt), equalTo(1));
		assertThat(snippet.getBody().getNumStmt(), equalTo(3));
	}

	@Test
	public void testReplaceLastStmts() throws TransformException {
		new TransformerHelper(snippet.get()).replaceStmt(Lists.newArrayList("id2", "id3"), stmt);
		assertThat(snippet.getBody().getIndexOfChild(stmt), equalTo(2));
		assertThat(snippet.getBody().getNumStmt(), equalTo(3));
	}

	@Test(expected = TransformException.class)
	public void testReplaceNonConnectedStmtsShouldFail() throws TransformException {
		new TransformerHelper(snippet.get()).replaceStmt(Lists.newArrayList("id1", "id3"), stmt);
		System.out.println(snippet.get().toDevString());
	}

	@Test
	public void testReplaceStmtsInADeeplyNestedConstruct() throws TransformException {
		// todo: parse a seq of blocks
		String xml = "<block type='control_if' id='~ta(.bV1XRC~W!9_~8R1' x='121' y='264'><value name='CONDITION'><block type='operator_lt' id='u5OsG|e6F=FO%yvUs4cH'><value name='OPERAND1'><shadow type='text' id='pYV0t$V%9%wmHGdJ!fA$'><field name='TEXT'>1</field></shadow></value><value name='OPERAND2'><shadow type='text' id='Hp;{g{4J)S7d,5UfV4rt'><field name='TEXT'>2</field></shadow></value></block></value><statement name='SUBSTACK'><block type='motion_movesteps' id='(?;u4Kx*78CIb33Nr*yS'><value name='STEPS'><shadow type='math_number' id='124[)e-Ki?c{^1rInE`u'><field name='NUM'>10</field></shadow></value><next><block type='control_if' id='Y#R?}?{C}5Jc}sP7-iBR'><value name='CONDITION'><block type='operator_lt' id='BJ*OpataFv4j)774!B.U'><value name='OPERAND1'><shadow type='text' id='Nd,-*X]6YD2H0bbz.it`'><field name='TEXT'>1</field></shadow></value><value name='OPERAND2'><shadow type='text' id='K08ZDh4;V=7CO5s#34-.'><field name='TEXT'>2</field></shadow></value></block></value><statement name='SUBSTACK'><block type='motion_movesteps' id='q~Gwy:Zf0Hod5OVq2J*Z'><value name='STEPS'><shadow type='math_number' id='0Hj[,cplYHfZUF%%K;kI'><field name='NUM'>10</field></shadow></value><next><block type='control_if' id='tK$9pQ8K@iL|Rto@kXxZ'><value name='CONDITION'><block type='operator_lt' id='G1!^GPaAU0aX0b)%n_Xp'><value name='OPERAND1'><shadow type='text' id='IAhuwB3%{te`kiLPnB$L'><field name='TEXT'>1</field></shadow></value><value name='OPERAND2'><shadow type='text' id='fiYFFYrJFG!K:FNB@`%,'><field name='TEXT'>2</field></shadow></value></block></value><statement name='SUBSTACK'><block type='motion_movesteps' id='G^OCYnQj=zkQr_Blod:K'><value name='STEPS'><shadow type='math_number' id='4A5;wSK7BM*SKx%6YFU,'><field name='NUM'>10</field></shadow></value><next><block type='motion_turnright' id='pjx(Tp(,cKE2Am5*L+oB'><value name='DEGREES'><shadow type='math_number' id='5hd5Hbwy@%;,c`mKEydb'><field name='NUM'>15</field></shadow></value><next><block type='motion_turnleft' id='_OSQ6g9:RE#Z~mt1`Gvg'><value name='DEGREES'><shadow type='math_number' id='|M+LYu#WxM^j`m-aSCfJ'><field name='NUM'>15</field></shadow></value><next><block type='control_if' id='T.X1dxDlGfZ]|4lvO9ha'><value name='CONDITION'><block type='operator_lt' id='yg@xbB]ti:~d{J!X+U|x'><value name='OPERAND1'><shadow type='text' id='.bHDSuS!5|C]PagW4JQ{'><field name='TEXT'>1</field></shadow></value><value name='OPERAND2'><shadow type='text' id='k5-=%.-as;4|qhlo$pz{'><field name='TEXT'>2</field></shadow></value></block></value><statement name='SUBSTACK'><block type='motion_movesteps' id='/S?RX@CG5YiHeZ+$F}0k'><value name='STEPS'><shadow type='math_number' id='#IsNlhy^8$Y/@Gu`VHO}'><field name='NUM'>10</field></shadow></value></block></statement></block></next></block></next></block></next></block></statement></block></next></block></statement></block></next></block></statement></block>";
		IfElseStmt ifelseStmt = (IfElseStmt) new Sb3XmlParser().parseBlock(xml);
		Snippet snippet = new Snippet();
		snippet.addStmt(ifelseStmt);
		System.out.println(snippet.get().toDevString());
		new TransformerHelper(snippet.get()).replaceStmt(Lists.newArrayList("pjx(Tp(,cKE2Am5*L+oB", "_OSQ6g9:RE#Z~mt1`Gvg"),
				stmt);
		System.out.println(snippet.get().toDevString());

		// String xml2 = new ScratchBlockAstConvert((ScratchBlock)
		// snippet.getBody().getStmt(0)).toXml().mkString();
		// System.out.println(xml2);
	}
	
	
	@Test
	public void testReplacedBlockShouldBeDeleted() throws TransformException {
		System.out.println(snippet.get().blockColl());
		new TransformerHelper(snippet.get()).replaceStmt(Lists.newArrayList("id0", "id1"), stmt);
		assertThat(snippet.getBody().getIndexOfChild(stmt), equalTo(0));
		snippet.get().flushAttrAndCollectionCache();
		Program program = snippet.get().deepCopy();
		System.out.println(program.blockColl());
		assert(!program.lookupBlock("id0").isPresent());
		assert(!program.lookupBlock("id1").isPresent());
		assert(program.lookupBlock("id2").isPresent());
		assert(program.lookupBlock("id3").isPresent());
		assert(program.lookupBlock("id4").isPresent());
	}


}
