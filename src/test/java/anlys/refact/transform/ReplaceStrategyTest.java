package anlys.refact.transform;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

import org.junit.Test;

import anlys.refact.transform.strategy.ReplaceExpr;
import ast.BlockSeq;
import ast.Expr;
import ast.ExprStmt;
import ast.OperatorExpr;
import ast.Program;
import ast.Sprite;
import lombok.extern.slf4j.Slf4j;
import sb3.parser.xml.Sb3XmlParser;
import util.ProgramFactory;

@Slf4j
public class ReplaceStrategyTest {

	@Test
	public void test() throws TransformException {

		String xml = "<block type='motion_movesteps' id='!#CEvj2lppWqCa;jD_%;' x='156' y='92'><value name='STEPS'><shadow type='math_number' id='u:X2q)iqj{^:Cy?DT`l0'><field name='NUM'>10</field></shadow></value></block>";
		ExprStmt stmt = (ExprStmt) new Sb3XmlParser().parseBlock(xml);

		BlockSeq seq = new BlockSeq();
		seq.addStmt(stmt);
		Snippet snippet = new Snippet(seq);

		String exprXml = "<block type='operator_round' id='PX8g}M8JZ8l``OFeI[mW' x='213' y='184'><value name='NUM'><shadow type='math_number' id='wzwAy}C.X;L_/kXop#xk'><field name='NUM'>3.3</field></shadow></value></block>";
		OperatorExpr opExpr = (OperatorExpr) new Sb3XmlParser().parseBlock(exprXml);

		Expr expr = snippet.get().lookupExpr("u:X2q)iqj{^:Cy?DT`l0").get();
		new ReplaceExpr<>(snippet.get()).replace(expr).with(opExpr).applyTransform();

		assertThat(stmt.getArg(0), equalTo(opExpr));
	}

	@Test
	public void testReplaceDropdownOption() {
		String xml = "<xml xmlns='http://www.w3.org/1999/xhtml'><variables><variable type='broadcast_msg' id='brVURUnozJFTQM@wVtU|' islocal='false' iscloud='false'>message1</variable></variables><block type='event_broadcast' id='zc-AcCD18Tn%kF`#?kj+' x='271' y='385'><value name='BROADCAST_INPUT'><shadow type='event_broadcast_menu' id='_kQhzm4=1CLJ#73BeBDG'><field name='BROADCAST_OPTION' id='brVURUnozJFTQM@wVtU|' variabletype='broadcast_msg'>message1</field></shadow></value></block></xml>";
		Sprite target = new Sb3XmlParser().parseTarget(xml);
		Program program = ProgramFactory.programOf(target);
		log.info(program.toDevString());
		assertThat(program.lookupBlock("_kQhzm4=1CLJ#73BeBDG").isPresent(), is(true));
	}
}
