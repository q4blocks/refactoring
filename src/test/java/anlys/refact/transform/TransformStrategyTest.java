package anlys.refact.transform;

import static anlys.TestUtil.assertUniqueId;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertThat;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;

import com.google.common.collect.Lists;

import anlys.refact.transform.strategy.CopyStrategy;
import anlys.refact.transform.strategy.TransformStrategy;
import ast.BlockSeq;
import ast.Expr;
import ast.ExprStmt;
import ast.Program;
import ast.Script;
import ast.Sprite;
import lombok.extern.slf4j.Slf4j;
import sb3.parser.xml.Sb3XmlParser;
import util.ProgramFactory;
import util.UtilFunc;

@Slf4j
public class TransformStrategyTest {

	@Test
	public void blockIdShouldBeUniqueForStatementBlockCopy() throws TransformException {
		String xmlStr = "<xml><variables></variables><block type='motion_movesteps' id='b1' x='525' y='496'><value name='STEPS'><shadow type='math_number' id='{H`(3cDLOp*BR%OM3t1j'><field name='NUM'>10</field></shadow></value></block></xml>";
		Sprite sprite = new Sb3XmlParser().parseTarget(xmlStr);
		Program program = ProgramFactory.programOf(sprite);
		ExprStmt original = (ExprStmt) program.lookupBlock("b1").get();
		ExprStmt copy = new CopyStrategy<>(original).makeCopy();
		assertThat(copy, is(not(equalTo(original))));

		new TransformStrategy(program).apply(() -> original.parentSeq().addStmt(copy));

		assertUniqueId(UtilFunc.xmlFromScratchBlock(original), UtilFunc.xmlFromScratchBlock(copy));

	}

	@Test
	public void blockIdShouldBeUniqueForExprSubtreeCopy() throws TransformException {
		String xmlStr = "<xml xmlns='http://www.w3.org/1999/xhtml'><variables></variables><block type='motion_gotoxy' id='stmtId' x='347' y='216'><value name='X'><shadow type='math_number' id='87.O,Kj!X90?M`W8Q#7R'><field name='NUM'>0</field></shadow><block type='operator_random' id='DmBLjb_7t_9Iv$fj%:Fc'><value name='FROM'><shadow type='math_number' id='v/?~aeV(QlzIqArv?vse'><field name='NUM'>1</field></shadow></value><value name='TO'><shadow type='math_number' id='*z!Yuw?(iwuAo34my45j'><field name='NUM'>10</field></shadow><block type='operator_add' id='hg$-,);,+E_@9.U@eYyO'><value name='NUM1'><shadow type='math_number' id='()tVKn[^TOT(W.nLY]fh'><field name='NUM'>2</field></shadow></value><value name='NUM2'><shadow type='math_number' id='[,`]UkX6NdESa6nkEWmF'><field name='NUM'>3</field></shadow></value></block></value></block></value><value name='Y'><shadow type='math_number' id='nFQs}ZA8Uh#B:o{skq(P'><field name='NUM'>0</field></shadow></value></block></xml>";
		Sprite sprite = new Sb3XmlParser().parseTarget(xmlStr);
		Program program = ProgramFactory.programOf(sprite);
		ExprStmt original = (ExprStmt) program.lookupBlock("stmtId").get();
		Expr copy = new CopyStrategy<>(original.getArg(0)).makeCopy();
		new TransformStrategy(program).apply(() -> original.setArg(copy, 1));

		assertThat(original, is(not(equalTo(copy))));
		log.info(UtilFunc.xmlFromScratchBlock(original));

	}

	@Test
	public void blockAndShadowIdsShouldUpdateWhenCopyStmtBlock() throws TransformException {
		String xml = "<block type='motion_movesteps' id='I4p%gV@dJinR/;S``WfM' x='151' y='127'><value name='STEPS'><shadow type='math_number' id='}7q3hc1Gy{YDyQaXbbXe'><field name='NUM'>10</field></shadow><block type='operator_random' id='#0OE;pd/l3=%#)8ZBX7X'><value name='FROM'><shadow type='math_number' id='h^Y;Z2*Bt[p]Z%Krl/7N'><field name='NUM'>1</field></shadow></value><value name='TO'><shadow type='math_number' id='CDBA~YyYX|`dnzAh*eZG'><field name='NUM'>10</field></shadow></value></block></value></block>";
		ExprStmt original = (ExprStmt) new Sb3XmlParser().parseBlock(xml);
		ExprStmt copy = new CopyStrategy<>(original).makeCopy();
		assertNotEquals(original.getDefaults(), copy.getDefaults());
		log.info(Arrays.toString(original.getDefaults()));
		log.info(Arrays.toString(copy.getDefaults()));
		assertUniqueId(UtilFunc.xmlFromScratchBlock(original), UtilFunc.xmlFromScratchBlock(copy));
	}

	// TODO convert to strategy
	@Test
	public void testBlockInsert() {
		ASTBuilder astBuilder = new ASTBuilder("block#", "var#");
		Script script = new Script();
		script.setBody(new BlockSeq());
		ExprStmt stmt1 = astBuilder.createStmt("motion_movesteps", "id1",
				Lists.newArrayList(new ArgBuildSpec(null, "5")));
		script.getBody().addStmt(stmt1);

		List<ArgBuildSpec> argSpecs = Lists.newArrayList(new ArgBuildSpec(null, "5"));
		ExprStmt stmt2 = astBuilder.createStmt("motion_turnright", "id1",
				Lists.newArrayList(new ArgBuildSpec(null, "7")));

		new TransformerHelper().insertBefore(stmt2, stmt1);
		System.out.println(script.toDevString());
		assertThat(stmt2.getNextSibling(), equalTo(stmt1));
	}

}
