package anlys.smell;

import static anlys.ActionsMatcher.matchesActions;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

import org.junit.Ignore;
import org.junit.Test;

import anlys.refact.RefactResult;
import anlys.refact.RescopeVar;
import anlys.refact.RescopeVarReq;
import ast.Program;
import lombok.extern.slf4j.Slf4j;
import sb3.parser.xml.Sb3XmlParser;
import util.DiffTextGenerator;
import util.ProjectFileWriter;
import util.UtilFunc;

@Slf4j
public class BroadScopeVarAnalyzerTest {

	@Ignore
	@Test
	public void test() throws Exception {
		String xmlStr = UtilFunc.readStringFromResource("broad-var-scope-test-project.xml");
		Program program = new Sb3XmlParser().parseProgram(xmlStr);
		String beforeDevString = program.toDevString();
		log.debug(beforeDevString);

		BroadScopeVarAnalyzer analyzer = new BroadScopeVarAnalyzer();
		SmellListResult<BroadScopeVar> results = analyzer.analyze(program);
		assertThat(results.size(), equalTo(2));

		RescopeVarReq request = results.get(0).buildRefactRequest(program);
		RefactResult result = new RescopeVar().analyze(program, request);
		log.debug(result.toPrettyJson());

		String refactJson = UtilFunc.readStringFromResource("rescope-var-refact-result.json");
		RefactResult expected = RefactResult.fromJson(refactJson);
		assertThat(result.getActions(), matchesActions(expected.getActions()));

		String afterDevString = program.toDevString();
		DiffTextGenerator diffTextGenerator = new DiffTextGenerator(new ProjectFileWriter());
		diffTextGenerator.compare(beforeDevString, afterDevString).toPrettyHtml("diff-rescope-var");
		log.debug(afterDevString);
	}

}
