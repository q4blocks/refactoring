package anlys.smell;

import static anlys.ActionsMatcher.matchesActions;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.util.List;

import org.junit.Test;

import anlys.refact.ExtractConst;
import anlys.refact.ExtractConstReq;
import anlys.refact.RefactResult;
import ast.ExprStmt;
import ast.Program;
import ast.Sprite;
import ast.StrLiteral;
import lombok.extern.slf4j.Slf4j;
import sb3.parser.xml.Sb3XmlParser;
import util.DiffTextGenerator;
import util.ProgramFactory;
import util.ProjectFileWriter;
import util.UtilFunc;

@Slf4j
public class DuplicateConstAnalysisTest extends SmellAnalyzerTest {
	@Test
	public void testSimpleCloneExprs() throws Exception {
		String xmlStr = "<xml xmlns='http://www.w3.org/1999/xhtml'><variables><variable type='' id='sprite1var1' islocal='false' iscloud='false'>var1</variable></variables><block type='motion_movesteps' id='(Q5ph7?vGm+:/6wC]44E' x='148' y='147'><value name='STEPS'><shadow type='math_number' id='tJu~}j$qp}d9;%q=5B5{'><field name='NUM'>10</field></shadow><block type='operator_add' id='n#_9ZX/bE.XSZ3O*]-34'><value name='NUM1'><shadow type='math_number' id='{k_al^[W*,Vkd-WR_;jP'><field name='NUM'></field></shadow><block type='operator_length' id='hVTJx0a)(@Z/X;+RpSva'><value name='STRING'><shadow type='text' id='#R6wGz%,HEb3DySSaR(Z'><field name='TEXT'>world</field></shadow></value></block></value><value name='NUM2'><shadow type='math_number' id='?jQ16Z{#7aM(_AJSjE(A'><field name='NUM'>1</field></shadow></value></block></value><next><block type='motion_turnright' id='JK|77ipA{}kp%5bJVb;d'><value name='DEGREES'><shadow type='math_number' id='as%)4njC~$l;7,du^`D{'><field name='NUM'>15</field></shadow><block type='operator_add' id='tM}WQ*RBw!oWL7*/3o(='><value name='NUM1'><shadow type='math_number' id='wGE2{!LDhr!UC,$]?Y%a'><field name='NUM'></field></shadow><block type='operator_length' id=',TClb}*}0E;Ao=4txi`G'><value name='STRING'><shadow type='text' id='_m!2/g+]WFu$6vLRC`W!'><field name='TEXT'>world</field></shadow></value></block></value><value name='NUM2'><shadow type='math_number' id='=Jqk;.j]h^DI7+4Kf)_y'><field name='NUM'>1</field></shadow></value></block></value></block></next></block></xml>";
		Sprite sprite = new Sb3XmlParser().parseTarget(xmlStr);
		sprite.setName("sprite1");
		Program program = ProgramFactory.programOf(sprite);
		String beforeDevString = program.toDevString();
		log.debug(beforeDevString);

		DuplicateConstAnalyzer analyzer = new DuplicateConstAnalyzer();
		analyzer.setMinCloneGroupSize(2);
		analyzer.setMinLiteralLength(2);

		SmellListResult<DuplicateConstant> res = analyzer.analyze(program);
		List<DuplicateConstant> groups = res.getInstances();
		assertThat(groups.size(), equalTo(1));
		assertThat(groups.get(0).getElements().size(), equalTo(2));

		// TODO also consider subexpression (not just whole expr)

		ExtractConstReq request = res.get(0).buildRefactRequest(program);

		RefactResult result = new ExtractConst().analyze(program, request);

		log.debug(result.toPrettyJson());

		String refactJson = UtilFunc.readStringFromResource("extract_const-RefactResult.json");
		RefactResult expected = RefactResult.fromJson(refactJson);

		assertThat(result.getActions(), matchesActions(expected.getActions()));

		String afterDevString = program.toDevString();
		DiffTextGenerator diffTextGenerator = new DiffTextGenerator(new ProjectFileWriter());
		diffTextGenerator.compare(beforeDevString, afterDevString).toPrettyHtml("test-extract-const");
	}

	@Test
	public void literalTypeShouldCorrespondToBlockShadowType() {
		String xmlStr = "<block type='sound_play' id='EdT3B;knz=ubor*A.n1M' x='181' y='71'><value name='SOUND_MENU'><shadow type='sound_sounds_menu' id='/L)kjtHEed]c6^Ll#*R]'><field name='SOUND_MENU'>0</field></shadow></value></block>";
		ExprStmt blk = (ExprStmt) new Sb3XmlParser().parseBlock(xmlStr);
		StrLiteral lit = (StrLiteral) blk.getArg(0);
		assertThat(lit.getAttrs().get("type"), is("sound_sounds_menu"));
	}
	
	@Test
	public void testUserStudyProgram() throws Exception {
		String xmlStr = "<xml xmlns=\"http://www.w3.org/1999/xhtml\"><variables><variable type=\"\" id=\"`jEk@4|i[#Fk?(8x)AV.-my variable\" islocal=\"false\" iscloud=\"false\">my variable</variable><variable type=\"\" id=\"}//DrmuK3Rn0C@[[9!6/\" islocal=\"true\" iscloud=\"false\">color</variable></variables><block type=\"event_whenflagclicked\" id=\"XCFw;Ub8W@DtOyVH|^I!\" x=\"102\" y=\"246\"><next><block type=\"looks_hide\" id=\"=Z`!/wXZ=AW~FF1@JX;E\"><next><block type=\"motion_gotoxy\" id=\"BkB:Wv%L[fwvgC8Z)55w\"><value name=\"X\"><shadow type=\"math_number\" id=\"|k0{i6mHd%W7.`ee8Zz)\"><field name=\"NUM\">0</field></shadow></value><value name=\"Y\"><shadow type=\"math_number\" id=\"*UFiN(kl!YMyWP1Pw{Sf\"><field name=\"NUM\">0</field></shadow></value><next><block type=\"motion_setrotationstyle\" id=\"~EhmU8j5?D8Bq1C@jaO@\"><field name=\"STYLE\">all around</field><next><block type=\"control_forever\" id=\"i_F9sM3#Z:OMVPAL-_pv\"><statement name=\"SUBSTACK\"><block type=\"control_repeat\" id=\"R-oD/;T{=CF#b-1,%;)}\"><value name=\"TIMES\"><shadow type=\"math_whole_number\" id=\"ez)C^Q}Te-8=?4j$UHF1\"><field name=\"NUM\">10</field></shadow></value><statement name=\"SUBSTACK\"><block type=\"motion_turnright\" id=\"q}_qLxBmNZPS4m_Ha(i(\"><value name=\"DEGREES\"><shadow type=\"math_number\" id=\"hA=pBhqdcN#|G2:{/#T+\"><field name=\"NUM\">36</field></shadow></value><next><block type=\"data_setvariableto\" id=\"DQa,po`0E$E?+{01M=%4\"><field name=\"VARIABLE\" id=\"}//DrmuK3Rn0C@[[9!6/\" variabletype=\"\">color</field><value name=\"VALUE\"><shadow type=\"text\" id=\"nvi0f^k`H3DoW%c2);tH\"><field name=\"TEXT\">185</field></shadow></value><next><block type=\"control_create_clone_of\" id=\"k[(!Y+GlYr*ZiwuTogY,\"><value name=\"CLONE_OPTION\"><shadow type=\"control_create_clone_of_menu\" id=\"p2nG:wr|y$61=E@8u~N:\"><field name=\"CLONE_OPTION\">_myself_</field></shadow></value></block></next></block></next></block></statement><next><block type=\"control_repeat\" id=\"f^`%C8pz~W*f5//e9@7n\"><value name=\"TIMES\"><shadow type=\"math_whole_number\" id=\"k?s.=x?0Rq6f$SwR%%um\"><field name=\"NUM\">10</field></shadow></value><statement name=\"SUBSTACK\"><block type=\"motion_turnright\" id=\"coO[[O%0%.NQ[OEVfi0t\"><value name=\"DEGREES\"><shadow type=\"math_number\" id=\"LkMdw$J_FPE^Q6xczQ$i\"><field name=\"NUM\">36</field></shadow></value><next><block type=\"data_setvariableto\" id=\"I;a%t1En+rQ4ctPh780j\"><field name=\"VARIABLE\" id=\"}//DrmuK3Rn0C@[[9!6/\" variabletype=\"\">color</field><value name=\"VALUE\"><shadow type=\"text\" id=\"zqDHp%cRfP%u5BSR~G8e\"><field name=\"TEXT\">80</field></shadow></value><next><block type=\"control_create_clone_of\" id=\"6-;V4Okl3/{h-i$Rs)}H\"><value name=\"CLONE_OPTION\"><shadow type=\"control_create_clone_of_menu\" id=\"Z_^!)|8MF8p^5GwHl63i\"><field name=\"CLONE_OPTION\">_myself_</field></shadow></value></block></next></block></next></block></statement></block></next></block></statement></block></next></block></next></block></next></block></next></block><block type=\"control_start_as_clone\" id=\"S!tvT_GzV;q|hbsNH|QU\" x=\"505\" y=\"245\"><next><block type=\"looks_show\" id=\"|!*`O/`kL(^OCuSgS:!E\"><next><block type=\"control_if\" id=\"mgjYYes+$y(E514f{g{4\"><value name=\"CONDITION\"><block type=\"operator_equals\" id=\"EuED]5(WPAO1`|533c7T\"><value name=\"OPERAND1\"><shadow id=\"H#})Rg:D=ED/=iJF/vnI\" type=\"text\" x=\"573\" y=\"301\"><field name=\"TEXT\"></field></shadow><block type=\"data_variable\" id=\"/GjiG*kSw7Vmb]KF%CU$\"><field name=\"VARIABLE\" id=\"}//DrmuK3Rn0C@[[9!6/\" variabletype=\"\">color</field></block></value><value name=\"OPERAND2\"><shadow type=\"text\" id=\"rb7A$jq8IQ_0LudCPx5!\"><field name=\"TEXT\">185</field></shadow></value></block></value><statement name=\"SUBSTACK\"><block type=\"looks_seteffectto\" id=\"As{l_wLC37GK((]6k8h7\"><field name=\"EFFECT\">COLOR</field><value name=\"VALUE\"><shadow id=\".HYR3S;C)N_(q~;aW.S5\" type=\"math_number\"><field name=\"NUM\">0</field></shadow><block type=\"data_variable\" id=\"5!)[gNt)sx$+,AKuDBU6\"><field name=\"VARIABLE\" id=\"}//DrmuK3Rn0C@[[9!6/\" variabletype=\"\">color</field></block></value><next><block type=\"control_repeat\" id=\"F(sS:?+I4h#h#~LyjX4H\"><value name=\"TIMES\"><shadow type=\"math_whole_number\" id=\"c/fW!)J/cZ~)#nMBp#[i\"><field name=\"NUM\">10</field></shadow></value><statement name=\"SUBSTACK\"><block type=\"motion_movesteps\" id=\"4X%g=*[VR)Z:C(dUAU_Y\"><value name=\"STEPS\"><shadow type=\"math_number\" id=\"1|9ppnXQLV27KSNC7e[6\"><field name=\"NUM\">10</field></shadow></value></block></statement><next><block type=\"control_delete_this_clone\" id=\"ECk5#lT+:)wk_|lGR!gE\"></block></next></block></next></block></statement><next><block type=\"control_if\" id=\"TDk0DGXsGEYl~y,-Kh%9\"><value name=\"CONDITION\"><block type=\"operator_equals\" id=\"9+Ka4Vmq5|Pc??I0?w8w\"><value name=\"OPERAND1\"><shadow id=\",/k#0%l5OF:lC1u.mYF=\" type=\"text\"><field name=\"TEXT\"></field></shadow><block type=\"data_variable\" id=\"eogseb!h}{cQx`~MY_z7\"><field name=\"VARIABLE\" id=\"}//DrmuK3Rn0C@[[9!6/\" variabletype=\"\">color</field></block></value><value name=\"OPERAND2\"><shadow type=\"text\" id=\"s42DGAUMa6#jiC1v*HLC\"><field name=\"TEXT\">80</field></shadow></value></block></value><statement name=\"SUBSTACK\"><block type=\"looks_seteffectto\" id=\"U:=`=o]GfyTJ6K;P(3Km\"><field name=\"EFFECT\">COLOR</field><value name=\"VALUE\"><shadow id=\";MyzC4M9NaL/T{DP$):b\" type=\"math_number\"><field name=\"NUM\">0</field></shadow><block type=\"data_variable\" id=\"X,#x~Bl0o$RH(KWm#Gy^\"><field name=\"VARIABLE\" id=\"}//DrmuK3Rn0C@[[9!6/\" variabletype=\"\">color</field></block></value><next><block type=\"control_repeat\" id=\"Ezzb9-f6Lv5FT8BrBpRh\"><value name=\"TIMES\"><shadow type=\"math_whole_number\" id=\"Ds(fsm-AXh_^*+79o)Jp\"><field name=\"NUM\">10</field></shadow></value><statement name=\"SUBSTACK\"><block type=\"motion_movesteps\" id=\"],e$S2im#{p3*|{6FSMy\"><value name=\"STEPS\"><shadow type=\"math_number\" id=\"+{1Bs:A.MMZnqc?y[q8q\"><field name=\"NUM\">10</field></shadow></value></block></statement><next><block type=\"control_delete_this_clone\" id=\"~hFv:]4yy(Yqm?cw$K==\"></block></next></block></next></block></statement></block></next></block></next></block></next></block></xml>";
		Sprite sprite = new Sb3XmlParser().parseTarget(xmlStr);
		sprite.setName("sprite1");
		Program program = ProgramFactory.programOf(sprite);

		DuplicateConstAnalyzer analyzer = new DuplicateConstAnalyzer();
		analyzer.setMinCloneGroupSize(2);
		analyzer.setMinLiteralLength(2);

		SmellListResult<DuplicateConstant> res = analyzer.analyze(program);
		List<DuplicateConstant> group = res.getInstances();
		
		assertThat(group.size(), equalTo(4));
		assertThat(group.get(0).getElements().size(), equalTo(2));
		
		ExtractConstReq request = res.get(0).buildRefactRequest(program);

		RefactResult result = new ExtractConst().analyze(program, request);

		log.debug(result.toPrettyJson());

		
	}

}
