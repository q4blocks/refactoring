package anlys.smell;

import static anlys.ActionsMatcher.matchesActions;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;

import java.io.IOException;
import java.text.NumberFormat;
import java.text.ParseException;

import org.junit.Test;

import com.google.common.collect.Sets;

import anlys.refact.ExtractParentSprite;
import anlys.refact.PrecondFailureType;
import anlys.refact.RefactResult;
import ast.Program;
import ast.Sprite;
import ast.Stage;
import clone.analyzer.SerializationConfig;
import lombok.extern.slf4j.Slf4j;
import sb3.parser.xml.Sb3XmlParser;
import util.DiffTextGenerator;
import util.ProjectFileWriter;
import util.UtilFunc;

@Slf4j
public class DuplicateSpriteAnalyzerTest {

	@Test
	public void testSerializeDeserialize() throws SmellJson2ObjException {
		Sprite sprite1 = new Sprite();
		sprite1.setName("sprite1");
		Program program = new Program();
		program.setStage(new Stage());
		program.getStage().addSprite(sprite1);

		DuplicateSprite instance = new DuplicateSprite(Sets.newHashSet(sprite1));
		DuplicateSprite deserializedInstance = new DuplicateSprite().fromJson(instance.toJson(), program);
		assertThat(deserializedInstance.getSprites().get(0).getSprite(), notNullValue());
	}

	@Test
	public void shouldDetectDuplicateSprite() throws IOException {
		String xmlStr = UtilFunc.readStringFromResource("duplicate-sprite-test-project.xml");
		Program program = new Sb3XmlParser().parseProgram(xmlStr);

		DuplicateSpriteAnalyzer analyzer = new DuplicateSpriteAnalyzer();
		SmellListResult<DuplicateSprite> results = analyzer.analyze(program);
		assertThat(results.size(), equalTo(1));
		assertThat(results.get(0).getSprites().size(), equalTo(2));
		log.debug(results.get(0).toPrettyJson());
	}

	@Test
	public void refactoringTest() throws Exception {
		String xmlStr = UtilFunc.readStringFromResource("duplicate-sprite-test-project.xml");
		Program program = new Sb3XmlParser().parseProgram(xmlStr);
		String beforeDevString = program.toDevString();
		// log.debug(beforeDevString);

		DuplicateSpriteAnalyzer analyzer = new DuplicateSpriteAnalyzer();
		SmellListResult<DuplicateSprite> results = analyzer.analyze(program);

		RefactResult refactResult = new ExtractParentSprite().analyze(program,
				results.get(0).buildRefactRequest(program));
		log.debug(refactResult.toPrettyJson());

		String refactJson = UtilFunc.readStringFromResource("extract-parent-sprite-refact-result.json");
		RefactResult expected = RefactResult.fromJson(refactJson);
		assertThat(refactResult.getActions(), matchesActions(expected.getActions()));

		String afterDevString = program.toDevString();
		DiffTextGenerator diffTextGenerator = new DiffTextGenerator(new ProjectFileWriter());
		diffTextGenerator.compare(beforeDevString,
				afterDevString).toPrettyHtml("diff-extract-parent-sprite");
		log.debug(afterDevString);
	}

	@Test
	public void shouldRejectSpriteDifferInOneOfScripts() throws Exception {
		String xmlStr = UtilFunc.readStringFromResource("duplicate-sprite-test-project-with-variation.xml");
		Program program = new Sb3XmlParser().parseProgram(xmlStr);
		String beforeDevString = program.toDevString();

		DuplicateSpriteAnalyzer analyzer = new DuplicateSpriteAnalyzer();
		SmellListResult<DuplicateSprite> results = analyzer.analyze(program);

		RefactResult refactResult = new ExtractParentSprite().analyze(program,
				results.get(0).buildRefactRequest(program));
		log.debug(refactResult.toPrettyJson());
		assertThat(refactResult.getMetadata("success"), is(false));
		assertThat(refactResult.getMetadata(PrecondFailureType.precond_invalid_variation_in_script), is(true));
	};

	/*
	 * should reject if event script differs in event arg (msg, key, etc.)
	 */

	@Test
	public void rejectInvalidVariants() throws Exception {
		String xmlStr = UtilFunc.readStringFromResource("duplicate-sprite-test-project-invalid-variants.xml");
		Program program = new Sb3XmlParser().parseProgram(xmlStr);
		String beforeDevString = program.toDevString();

		DuplicateSpriteAnalyzer analyzer = new DuplicateSpriteAnalyzer();
		SmellListResult<DuplicateSprite> results = analyzer.analyze(program);

		RefactResult refactResult = new ExtractParentSprite().analyze(program,
				results.get(0).buildRefactRequest(program));
		log.debug(refactResult.toPrettyJson());
		assertThat(refactResult.getMetadata("success"), is(false));
		assertThat(refactResult.getMetadata(PrecondFailureType.precond_invalid_variation_in_script), is(true));
	}

	/*
	 * should reject different event dropw down e.g. different keys pressed
	 * 
	 
	 */
	@Test
	public void rejectInvalidVariantsEventDropdown() throws Exception {
		String xmlStr = UtilFunc.readStringFromResource("duplicate-sprite-test-project-invalid-variants-2.xml");
		Program program = new Sb3XmlParser().parseProgram(xmlStr);
		String beforeDevString = program.toDevString();

		DuplicateSpriteAnalyzer analyzer = new DuplicateSpriteAnalyzer();
		SmellListResult<DuplicateSprite> results = analyzer.analyze(program);
		RefactResult refactResult = new ExtractParentSprite().analyze(program,
				results.get(0).buildRefactRequest(program));
		log.debug(refactResult.toPrettyJson());
		assertThat(refactResult.getMetadata("success"), is(false));
		assertThat(refactResult.getMetadata(PrecondFailureType.precond_invalid_variation_in_script), is(true));
	}

}
