package anlys.smell;

import ast.Program;
import sb3.dsl.SimXml2AstParser;

public abstract class SmellAnalyzerTest {

	public Program getProgramSimpXml(String xml) {
		return (Program) new SimXml2AstParser().parse(xml);
	}

	

}
